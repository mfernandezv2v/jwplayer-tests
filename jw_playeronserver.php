	<!--A Design by W3layouts 
	Author: W3layout
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<html lang="en">
	<head>
		<title>Should Sales Reps Promote Broadcast Programs?</title>
		<script type='text/javascript' src='jwplayer-7.10.4/jwplayer.js'></script>
		<script>jwplayer.key="gDfj3XHVRtVShp1ZTEqTuhGc0HDYC0IH8CzGMA==";</script>
		</script>
	</head>
	<body>
		<h2> Appending V2V logo on the top right. Duration: <div id="duration"> </div>
		<div id = 'video' name='video'>
		</div>	
		<button id="Back15s" onclick="volta15s()"> Get back 15s </button>
		<button id="Forward15s" onclick="adianta15s()"> Forward 15s </button>
		<button id="VolumePlus" onclick="riseVolume()"> Rise Volume </button>
		<button id="VolumeDown" onclick="downVolume()"> Down Volume </button>
		<script type="text/javascript">
			
			jwplayer('video').setup({
  			  file: "videos/5steps.mp4",
  			  image: "img/5steps.png",
			  height: "75%",
			  width: "75%",
			  tracks: [{ 
	            file: "assets/captions/english.vtt", 
	            label: "English",
	            kind: "captions",
	            "default": true} ],
				logo: {
					file: 'img/V2V_logo_transp_75.png',
					link: 'http://www.vision2voice.com'
				},
				abouttext: "Video Available at Vision2Voice.com",	            
        		
				aspectratio: "16:9",
				stretching: "fill"
			  //autostart: true,
			  
			});			
			function volta15s() {
				sharingPlugin.open();
				if (jwplayer('video').getPosition() > 15)
					jwplayer('video').seek (jwplayer('video').getPosition()-15);
				else 
					jwplayer('video').seek(0);
			}
			function adianta15s() {
					jwplayer('video').seek (jwplayer('video').getPosition()+15);
			}			
			function riseVolume() {
				jwplayer('video').setVolume(jwplayer('video').getVolume() + 10);
			}
			function downVolume () {
				jwplayer('video').setVolume(jwplayer('video').getVolume() - 10);
			}
			</script>
  </body>


	</html>
