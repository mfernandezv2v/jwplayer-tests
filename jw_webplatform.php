	<!--A Design by W3layouts 
	Author: W3layout
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<html lang="en">
	<head>
		<title>Should Sales Reps Promote Broadcast Programs?</title>
	</head>
	<body>
		<h2> Appending subtitles to remark important words on content </h2> 
		<p> Using only a javascript to embed the video - Turn on the CC button </p>
		<div id = "VideoScript">
			<script src="//content.jwplatform.com/players/gC8hhnUH-MH5L5mzx.js"></script>
		</div>
		<button id="Back15s" onclick="volta15s()"> Get back 15s </button>
		<button id="Forward15s" onclick="adianta15s()"> Forward 15s </button>
		<button id="VolumePlus" onclick="riseVolume()"> Rise Volume </button>
		<button id="VolumeDown" onclick="downVolume()"> Down Volume </button>		
	   <br/> 
    <br/>
    <br/>

		<script type="text/javascript">
			
			function volta15s(video) {
				if (jwplayer(video).getPosition() > 15)
					jwplayer(video).seek (jwplayer(video).getPosition()-15);
				else 
					jwplayer(video).seek(0);
			}
			function adianta15s(video) {
					jwplayer(video).seek (jwplayer(video).getPosition()+15);
			}			
			function riseVolume(video) {
				jwplayer(video).setVolume(jwplayer(video).getVolume() + 10);
			}
			function downVolume (video) {
				jwplayer(video).setVolume(jwplayer(video).getVolume() - 10);
			}
			</script>    

		<h2> Appending V2V logo on the top right </h2>
		<p> Using a div and defining its size, even though it can be responsive </p>
		<div id = "VideoDiv" style="position:relative; overflow:hidden; width: 50%;">
			<iframe src="//content.jwplatform.com/players/WCQZJGxg-MH5L5mzx.html" width="100%" height="100%" frameborder="0" scrolling="auto" allowfullscreen style="position:relative;">
			</iframe>
		</div>			
  </body>


	</html>
