<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	public function __construct()
		{
				parent::__construct();
				$this->load->library('form_validation');
				$this->load->model('Admin_model');
				$this->load->model('User_model');
				$this->load->model('Learning_model');
		}

	public function user_groups()
	{
		if($this->uri->segment(3)) {
			$user_group_id = $this->uri->segment(3);
			$groupData = $this->User_model->get_user_group($user_group_id);
		} else {
			$user_group_id = 0;
			$groupData = array (
			'user_group_id' => 0,
			'user_group_name' => '',
			'active' => 0
		);
		}
		$data['user_group_id'] = $user_group_id;
		$data['groupList'] = $this->User_model->get_user_groups('all'); // ('all', 'active', 'inactive')
		$data['groupData'] = $groupData;
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('admin/user_groups', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function user_groups_post()
	{
		if($this->input->post("user_group_id") !== NULL) {$user_group_id = $this->input->post("user_group_id");} else {$user_group_id = '';}
		if($this->input->post("user_group_name") !== NULL) {$user_group_name = $this->input->post("user_group_name");} else {$user_group_name = '';}
		if($this->input->post("active") !== NULL) {$active = $this->input->post("active");} else {$active = 0;}
		$modified_date = date('Y-m-d H:i:s');
		$modified_by = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
		$data = array(
			'user_group_id' => $user_group_id,
			'user_group_name' => $user_group_name,
			'active' => $active,
			'modified_date' => $modified_date,
			'modified_by' => $modified_by
		);
		$response = $this->User_model->update_user_groups($data);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
	redirect('Admin/user_groups/');
	}

	public function user_accounts()
	{
		if($this->uri->segment(3)) {
			$user_id = $this->uri->segment(3);
			$userData = $this->User_model->get_user_record($user_id);
			$password = $this->User_model->encrypt_decrypt('decrypt', $userData['password']);
		} else {
			$user_id = 0;
			$password = '';
			$userData = array (
			'user_id' => 0, 'email_address' => '', 'password' => '', 'user_type' => ''
			, 'title' => '', 'first_name' => '', 'mid_name' => '', 'last_name' => ''
			, 'degree' => '', 'display_name' => '', 'assignments' => '', 'active' => 0
		);
		}
		$data['user_id'] = $user_id;
		$data['user_password'] = $password;
		$data['userData'] = $userData;
		$data['userList'] = $this->User_model->get_all_users();
		$data['moduleList'] = $this->Learning_model->get_learning_modules('active');
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('admin/user_accounts', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function user_account_post()
	{
		if($this->input->post("user_id") !== NULL) {$user_id = $this->input->post("user_id");} else {$user_id = '';}
		if($this->input->post("email_address") !== NULL) {$email_address = $this->input->post("email_address");} else {$email_address = '';}
		if($this->input->post("password") !== NULL) {$password = $this->input->post("password");} else {$password = '';}
		if($this->input->post("user_type") !== NULL) {$user_type = $this->input->post("user_type");} else {$user_type = '';}
		if($this->input->post("display_name") !== NULL) {$display_name = $this->input->post("display_name");} else {$display_name = '';}
		if($this->input->post("title") !== NULL) {$title = $this->input->post("title");} else {$title = '';}
		if($this->input->post("first_name") !== NULL) {$first_name = $this->input->post("first_name");} else {$first_name = '';}
		if($this->input->post("mid_name") !== NULL) {$mid_name = $this->input->post("mid_name");} else {$mid_name = '';}
		if($this->input->post("last_name") !== NULL) {$last_name = $this->input->post("last_name");} else {$last_name = '';}
		if($this->input->post("degree") !== NULL) {$degree = $this->input->post("degree");} else {$degree = '';}
		if($this->input->post("active") !== NULL) {$active = $this->input->post("active");} else {$active = 0;}
		$assignment_list = '';
		if($this->input->post("assignments") !== NULL) {
			$assignments = $this->input->post("assignments");
			if(count($assignments) > 0) {
				foreach($assignments as $assign) {
					$assignment_list .= $assign.",";
				}
				$assignment_list = substr($assignment_list, 0, -1); // remove last comma
			} else {
			$assignment_list = 0;
			}
		} else {
			$assignment_list = 0;
		}
		$modified_date = date('Y-m-d H:i:s');
		$modified_by = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
		if($password != '') {
			$user_password = $this->User_model->encrypt_decrypt('encrypt', $password);
		} else {
			$user_password = '';
		}
		$data = array(
			'user_id' => $user_id,
			'email_address' => $email_address,
			'password' => $user_password,
			'user_type' => $user_type,
			'display_name' => $display_name,
			'title' => $title,
			'first_name' => $first_name,
			'mid_name' => $mid_name,
			'last_name' => $last_name,
			'degree' => $degree,
			'assignments' => $assignment_list,
			'active' => $active,
			'modified_date' => $modified_date,
			'modified_by' => $modified_by
		);
		$response = $this->User_model->update_user_account($data);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
	redirect('Admin/user_accounts/');
	}

	public function user_contact()
	{
		if($this->uri->segment(3)) {
			$user_id = $this->uri->segment(3);
			$userData = $this->User_model->get_user_contact($user_id);
		} else {
			$user_id = 0;
			$userData = array (
			'user_id' => 0, 'address_type' => '', 'address1' => '', 'address2' => ''
			, 'city' => '', 'state' => '', 'postal_code' => '', 'phone' => ''
			, 'phone_ext' => '', 'time_zone' => ''
			);
		}
		$data['user_id'] = $user_id;
		$data['userData'] = $userData;
		$data['addressTypes'] = $this->User_model->get_address_types();
		$data['stateCodes'] = $this->User_model->get_states();
		$data['timeZones'] = $this->User_model->get_time_zones();
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('admin/user_contact', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function user_contact_post()
	{
		if($this->input->post("user_id") !== NULL) {$user_id = $this->input->post("user_id");} else {$user_id = '';}
		if($this->input->post("address_type") !== NULL) {$address_type = $this->input->post("address_type");} else {$address_type = 0;}
		if($this->input->post("address1") !== NULL) {$address1 = $this->input->post("address1");} else {$address1 = '';}
		if($this->input->post("address2") !== NULL) {$address2 = $this->input->post("address2");} else {$address2 = '';}
		if($this->input->post("city") !== NULL) {$city = $this->input->post("city");} else {$city = '';}
		if($this->input->post("state") !== NULL) {$state = $this->input->post("state");} else {$state = '';}
		if($this->input->post("postal_code") !== NULL) {$postal_code = $this->input->post("postal_code");} else {$postal_code = '';}
		if($this->input->post("phone") !== NULL) {$phone = $this->input->post("phone");} else {$phone = '';}
		if($this->input->post("phone_ext") !== NULL) {$phone_ext = $this->input->post("phone_ext");} else {$phone_ext = '';}
		if($this->input->post("time_zone") !== NULL) {$time_zone = $this->input->post("time_zone");} else {$time_zone = '';}
		$modified_date = date('Y-m-d H:i:s');
		$modified_by = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
		$data = array(
			'user_id' => $user_id,
			'address_type' => $address_type,
			'address1' => $address1,
			'address2' => $address2,
			'city' => $city,
			'state' => $state,
			'postal_code' => $postal_code,
			'phone' => $phone,
			'phone_ext' => $phone_ext,
			'time_zone' => $time_zone,
			'modified_date' => $modified_date,
			'modified_by' => $modified_by
		);
		$response = $this->User_model->update_user_contact($data);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
	redirect('Admin/user_accounts/');
	}

	public function adm_time_zones()
	{
		if($this->uri->segment(3)) {
			$time_zone_id = $this->uri->segment(3);
			$tzData = $this->Admin_model->get_time_zone($time_zone_id);
		} else {
			$time_zone_id = 0;
			$tzData = array (
			'time_zone_id' => 0
			, 'time_zone_code' => ''
			, 'time_zone_name' => ''
			, 'active' => 0
			);
		}
		$tzList = $this->Admin_model->get_time_zones();
		$data['time_zone_id'] = $time_zone_id;
		$data['tzList'] = $tzList;
		$data['tzData'] = $tzData;
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('admin/adm_time_zones', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function adm_time_zones_post()
	{
		if($this->input->post("time_zone_id") !== NULL) {$time_zone_id = $this->input->post("time_zone_id");} else {$time_zone_post = '';}
		if($this->input->post("time_zone_code") !== NULL) {$time_zone_code = $this->input->post("time_zone_code");} else {$time_zone_code = 0;}
		if($this->input->post("time_zone_name") !== NULL) {$time_zone_name = $this->input->post("time_zone_name");} else {$time_zone_name = '';}
		if($this->input->post("active") !== NULL) {$active = $this->input->post("active");} else {$active = 0;}
		$data = array(
			'time_zone_id' => $time_zone_id,
			'time_zone_code' => $time_zone_code,
			'time_zone_name' => $time_zone_name,
			'active' => $active
		);
		$response = $this->Admin_model->update_time_zone($data);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
	redirect('Admin/adm_time_zones/');
	}

	public function adm_states()
	{
		if($this->uri->segment(3)) {
			$state_id = $this->uri->segment(3);
			$stData = $this->Admin_model->get_state($state_id);
		} else {
			$state_id = 0;
			$stData = array (
			'state_id' => 0
			, 'state_code' => ''
			, 'state_name' => ''
			, 'active' => 0
			);
		}
		$stList = $this->Admin_model->get_states();
		$data['state_id'] = $state_id;
		$data['stList'] = $stList;
		$data['stData'] = $stData;
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('admin/adm_states', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function adm_states_post()
	{
		if($this->input->post("state_id") !== NULL) {$state_id = $this->input->post("state_id");} else {$state_post = '';}
		if($this->input->post("state_code") !== NULL) {$state_code = $this->input->post("state_code");} else {$state_code = 0;}
		if($this->input->post("state_name") !== NULL) {$state_name = $this->input->post("state_name");} else {$state_name = '';}
		if($this->input->post("active") !== NULL) {$active = $this->input->post("active");} else {$active = 0;}
		$data = array(
			'state_id' => $state_id,
			'state_code' => $state_code,
			'state_name' => $state_name,
			'active' => $active
		);
		$response = $this->Admin_model->update_state($data);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
	redirect('Admin/adm_states/');
	}

	public function adm_address_types()
	{
		if($this->uri->segment(3)) {
			$address_type_id = $this->uri->segment(3);
			$adrData = $this->Admin_model->get_address_type($address_type_id);
		} else {
			$address_type_id = 0;
			$adrData = array (
			'address_type_id' => 0
			, 'address_type_name' => ''
			, 'active' => 0
			);
		}
		$adrList = $this->Admin_model->get_address_types();
		$data['address_type_id'] = $address_type_id;
		$data['adrList'] = $adrList;
		$data['adrData'] = $adrData;
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('admin/adm_address_types', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function adm_address_types_post()
	{
		if($this->input->post("address_type_id") !== NULL) {$address_type_id = $this->input->post("address_type_id");} else {$address_type_id = 0;}
		if($this->input->post("address_type_name") !== NULL) {$address_type_name = $this->input->post("address_type_name");} else {$address_type_name = '';}
		if($this->input->post("active") !== NULL) {$active = $this->input->post("active");} else {$active = 0;}
		$data = array(
			'address_type_id' => $address_type_id,
			'address_type_name' => $address_type_name,
			'active' => $active
		);
		$response = $this->Admin_model->update_address_type($data);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
	redirect('Admin/adm_address_types/');
	}

	public function sys_smtp()
	{
		if($this->uri->segment(3)) {
			$sys_smtp_id = $this->uri->segment(3);
			$smtpData = $this->Admin_model->get_smtp($sys_smtp_id);
		} else {
			$smtpData = $this->Admin_model->get_sys_smpt_default();
			$sys_smtp_id = $smtpData['sys_smtp_id'];
		}
		if(count($smtpData) < 1) {
			$sys_smtp_id = 0;
			$smtpData = array (
			'sys_smtp_id' => 0,
			'sys_smtp_server' => '',
			'sys_smtp_account_name' => '',
			'sys_smtp_account' => '',
			'sys_smtp_password' => '',
			'sys_smtp_port' => '',
			'sys_smtp_secure' => '',
			'sys_support_name' => '',
			'sys_support_account' => ''
			);
		}
		$data['sys_smtp_id'] = $sys_smtp_id;
		$data['smtpData'] = $smtpData;
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('Admin/sys_smtp', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function sys_smtp_post()
	{
		if($this->input->post("sys_smtp_id") !== null) {$sys_smtp_id = $this->input->post("sys_smtp_id");} else {$sys_smtp_id = 0;}
		if($this->input->post("sys_smtp_server") !== null) {$sys_smtp_server = $this->input->post("sys_smtp_server");} else {$sys_smtp_server= '';}
		if($this->input->post("sys_smtp_account_name") !== null) {$sys_smtp_account_name = $this->input->post("sys_smtp_account_name");} else {$sys_smtp_account_name = '';}
		if($this->input->post('sys_smtp_account') !== null) {$sys_smtp_account = $this->input->post('sys_smtp_account');} else {$sys_smtp_account = '';}
		if($this->input->post('sys_smtp_password') !== null) {$sys_smtp_password = $this->input->post('sys_smtp_password');} else {$sys_smtp_password = '';}
		if($this->input->post('sys_smtp_port') !== null) {$sys_smtp_port = $this->input->post('sys_smtp_port');} else {$sys_smtp_port = '';}
		if($this->input->post('sys_smtp_secure') !== null) {$sys_smtp_secure = $this->input->post('sys_smtp_secure');} else {$sys_smtp_secure = '';}
		if($this->input->post('sys_support_name') !== null) {$sys_support_name = $this->input->post('sys_support_name');} else {$sys_support_name = '';}
		if($this->input->post('sys_support_account') !== null) {$sys_support_account = $this->input->post('sys_support_account');} else {$sys_support_account = '';}
		$smtpPassword = $this->User_model->encrypt_decrypt('encrypt', $sys_smtp_password);
		$data = array (
			'sys_smtp_id' => $sys_smtp_id,
			'sys_smtp_server' => $sys_smtp_server,
			'sys_smtp_account_name' => $sys_smtp_account_name,
			'sys_smtp_account' => $sys_smtp_account,
			'sys_smtp_password' => $smtpPassword,
			'sys_smtp_port' => $sys_smtp_port,
			'sys_smtp_secure' => $sys_smtp_secure,
			'sys_support_name' => $sys_support_name,
			'sys_support_account' => $sys_support_account
		);
		if($sys_smtp_id == 0) {
			$response = $this->Admin_model->add_sys_smtp($data);
		} else {
			$response = $this->Admin_model->update_sys_smtp($data);
		}
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
		// go to main page
		redirect('Admin/sys_smtp/', 'refresh');
	}

	public function sys_email_template()
	{
		$emList = $this->Admin_model->get_sys_email_template_list();
		$data['emList'] = $emList;
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('Admin/sys_email_template', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function smtp_email_template_post()
	 {
		 $em_template_id = $this->input->post("em_template_id");
		 $data = array (
		 	 'em_template_id' => $this->input->post("em_template_id"),
			 'em_name' => $this->input->post("em_name"),
			 'em_subject' => $this->input->post("em_subject"),
			 'em_message' => $this->input->post("em_message")
		 );
		 if($em_template_id == '0') {
			 $response = $this->Admin_model->add_email_template($data);
		 } else {
			 $response = $this->Admin_model->update_email_template($data);
		 }
		 redirect('Admin/sys_email_template/', 'refresh');
	 }

	 public function smtp_template_edit()
 	{
		$data['em_template_id'] = $this->uri->segment(3);
 		$data['emData'] = $this->Admin_model->get_sys_email_template($this->uri->segment(3));
 		$this->load->view('common/pageHeader');
 		$this->load->view('common/pageTop');
 		$this->load->view('common/pageSideBar');
 		$this->load->view('admin/sys_email_template_edit', $data);
 		$this->load->view('common/pageFooter');
 		$this->load->view('common/pageScripts');
 	}

	 public function smtp_template_remove()
 	{
 		$em_template_id = $this->uri->segment(3);
 		$response = $this->Admin_model->remove_sys_email_template($em_template_id);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
 		redirect('Admin/sys_email_template/', 'refresh');
 	}

	public function learning_modules()
	{
		if($this->uri->segment(3)) {
			$module_id = $this->uri->segment(3);
			$moduleData = $this->Learning_model->get_learning_module($module_id);
		} else {
			$module_id = 0;
			$moduleData = array (
			'module_id' => 0
			, 'module_name' => ''
			, 'module_description' => ''
			, 'total_lessons' => 0
			, 'prerequisite' => 0
			, 'active' => 0
			);
		}
		$moduleList = $this->Learning_model->get_learning_modules('all');
		$data['module_id'] = $module_id;
		$data['moduleList'] = $moduleList;
		$data['moduleData'] = $moduleData;
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('Admin/adm_learning_modules', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function learning_modules_post()
	{
		if($this->input->post("module_id") !== null) {$module_id = $this->input->post("module_id");} else {$module_id = 0;}
		if($this->input->post("module_name") !== null) {$module_name = $this->input->post("module_name");} else {$module_name = '';}
		if($this->input->post("module_description") !== null) {$module_description = $this->input->post("module_description");} else {$module_description = '';}
		if($this->input->post("total_lessons") !== null) {$total_lessons = $this->input->post("total_lessons");} else {$total_sessions = '';}
		if($this->input->post("prerequisite") !== null) {$prerequisite = $this->input->post("prerequisite");} else {$prerequisite = 0;}
		if($this->input->post('active') !== null) {$active = $this->input->post('active');} else {$active = 0;}
		$modified_date = date('Y-m-d H:i:s');
		$modified_by = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
		$data = array (
			'module_id' => $module_id,
			'module_name' => $module_name,
			'module_description' => $module_description,
			'total_lessons' => $total_lessons,
			'prerequisite' => $prerequisite,
			'active' => $active,
			'modified_date' => $modified_date,
			'modified_by' => $modified_by
		);
		$response = $this->Learning_model->update_learning_module($data);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
		// go to main page
		redirect('Admin/learning_modules/', 'refresh');
	}

	public function lessons()
	{
		$moduleList = $this->Learning_model->get_learning_modules('all');
		$data['moduleList'] = $moduleList;
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('Admin/adm_lessons', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function lesson_edit()
	{
		if($this->uri->segment(3)) {
			$lesson_id = $this->uri->segment(3);
			$lessonData = $this->Learning_model->get_lesson($lesson_id);
		} else {
			$lesson_id = 0;
			$lessonData = array (
			'lesson_id' => 0
			, 'module_id' => 0
			, 'lesson_name' => ''
			, 'lesson_description' => ''
			, 'lesson_order' => 0
			, 'lesson_type' => 0
			, 'lesson_length_min' => 0
			, 'lesson_length_sec' => 0
			, 'lesson_file' => ''
			, 'lesson_image' => ''
			, 'active' => 0
			);
		}
		$moduleList = $this->Learning_model->get_learning_modules('all');
		$data['lesson_id'] = $lesson_id;
		$data['moduleList'] = $moduleList;
		$data['lessonData'] = $lessonData;
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('Admin/adm_lesson_edit', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function show_lesson_list()
	{
		$module_id = $_GET['module_id'];
    $lessonList = $this->Learning_model->get_lessons_by_module($module_id);
		$html = "<table class='table table-bordered table-striped table-hover table-condensed'>";
		$html .= "<thead><tr><th>Name</th><th width='30%'>Description</th><th>Sequence</th><th>Type</th><th>Length</th><th>Active</th></tr></thead>";
		$html .= "<tbody>";
			if(count($lessonList) > 0) {
				foreach($lessonList as $lesson) {
					$html .= "<tr>";
					$html .= "<td><a href='".base_url('Admin/lesson_edit/').$lesson['lesson_id']."'>".$lesson['lesson_name']."</a></td>";
					$html .= "<td>".$lesson['lesson_description']."</td>";
					$html .= "<td>".$lesson['lesson_order']."</td>";
					$html .= "<td>";
					if ($lesson['lesson_type'] == 1) $html .= "Video";
					else if($lesson['lesson_type'] == 2) $html .= "Audio";
					else if($lesson['lesson_type'] == 3) $html .= "PDF/Documet";
					else $html .= "undefined";
					$html .= "</td>";
					$time = "00:".$lesson['lesson_length_min'].":".$lesson['lesson_length_sec'];
					$media_time = date('i:s', strtotime($time));
					$html .= "<td>".$media_time."</td>";
					$html .= "<td>";
					if($lesson['active'] == 0) {$html .= 'No';}
					else if($lesson['active'] == 1) {$html .= "Yes";}
					else $html .= 'Undefined';
					$html .= "</td>";
					$html .= "</tr>";
				}
			} else {
				$html .= "<tr><td colspan='4'>There are no lessons for this module...</td></tr>";
			}
		$html .= "</tbody></table>";
		echo $html;
}

public function lesson_edit_post()
{
	if($this->input->post("lesson_id") !== null) {$lesson_id = $this->input->post("lesson_id");} else {$lesson_id = 0;}
	if($this->input->post("module_id") !== null) {$module_id = $this->input->post("module_id");} else {$module_id = 0;}
	if($this->input->post("lesson_name") !== null) {$lesson_name = $this->input->post("lesson_name");} else {$lesson_name = '';}
	if($this->input->post("lesson_description") !== null) {$lesson_description = $this->input->post("lesson_description");} else {$lesson_description = '';}
	if($this->input->post("lesson_order") !== null) {$lesson_order = $this->input->post("lesson_order");} else {$lesson_order = 0;}
	if($this->input->post("lesson_type") !== null) {$lesson_type = $this->input->post("lesson_type");} else {$lesson_type = 0;}
	if($this->input->post("lesson_length_min") !== null) {$lesson_length_min = $this->input->post("lesson_length_min");} else {$lesson_length_min = 0;}
	if($this->input->post("lesson_length_sec") !== null) {$lesson_length_sec = $this->input->post("lesson_length_sec");} else {$lesson_length_sec = 0;}
	if($this->input->post("lesson_file") !== null) {$lesson_file = $this->input->post("lesson_file");} else {$lesson_file = '';}
	if($this->input->post("lesson_image") !== null) {$lesson_image = $this->input->post("lesson_image");} else {$lesson_image = '';}
	if($this->input->post('active') !== null) {$active = $this->input->post('active');} else {$active = 0;}
	$modified_date = date('Y-m-d H:i:s');
	$modified_by = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
	$data = array (
		'lesson_id' => $lesson_id,
		'module_id' => $module_id,
		'lesson_name' => $lesson_name,
		'lesson_description' => $lesson_description,
		'lesson_order' => $lesson_order,
		'lesson_type' => $lesson_type,
		'lesson_length_min' => $lesson_length_min,
		'lesson_length_sec' => $lesson_length_sec,
		'lesson_file' => $lesson_file,
		'lesson_image' => $lesson_image,
		'active' => $active,
		'modified_date' => $modified_date,
		'modified_by' => $modified_by
	);
	$response = $this->Learning_model->update_lesson($data);
	if($response) {
		$this->session->set_flashdata("success", 'Update was successful!');
	} else {
		$this->session->set_flashdata("error", 'Update failed!');
	}
	// return
	if($lesson_id == 0) {
		redirect('Admin/lesson_edit/'.$response, 'refresh');
	} else {
		redirect('Admin/lesson_edit/'.$lesson_id, 'refresh');
	}
}


}
