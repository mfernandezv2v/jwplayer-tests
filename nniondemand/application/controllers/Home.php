<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
		{
				parent::__construct();
				$this->load->library('form_validation');
				$this->load->library('session');
				$this->load->model('Admin_model');
				$this->load->model('User_model');
				$this->load->model('Learning_model');
		}

	public function index()
	{
		$this->load->view('common/pageHeader');
		$this->load->view('login');
		$this->load->view('common/pageFooterLogin');
		$this->load->view('common/pageScripts');
	}

	public function login_validate()
	{
		echo "CU";
		if (empty($this->input->post('user_name')) OR empty($this->input->post('user_password'))) {
				$this->session->set_flashdata('login_error', 'Login attempt failed!<br />Please try again.');
				redirect("Home/index");
			}
			$password = $this->input->post('user_password');
			$email_address = strtolower($this->input->post('user_name'));
			$data = array (
			'password' => $password,
			'email_address' => $email_address
			);
			$login = $this->User_model->get_user_login($data);

			if (count($login) != 0) {
				//echo $login ['user_id'];
				$acct = $this->User_model->get_user_record($login['user_id']);
				echo $acct['user_id'];
				$this->session->set_userdata(array(
          'user_id'        => $acct['user_id'],
          'first_name'     => $acct['first_name'],
          'last_name'      => $acct['last_name'],
          'user_type'      => $acct['user_type'],
          'email_address'  => $acct['email_address'],
		  'display_name'	 => $acct['display_name'],
          'status'         => TRUE
          ));
				$data = array(
						  'user_id' => $this->session->userdata('user_id')
						, 'module_id' => 0
						, 'lesson_id' => 0
						, 'event_code' => LOG_SIGNIN
						, 'event_tag' => LOG_SIGNIN_TAG
						, 'log_date' => date('Y-m-d H:i:s')
						);
				$response = $this->Learning_model->log_lesson_activity($data);
				//
				if($password == PSWD_DEFAULT) {
					redirect ('Home/reset_password_initial');
				} else {
					redirect ('Home/main');
				}
			}
			else {
				$this->session->set_flashdata('login_error', 'Login attempt failed!<br />Please try again.');
				redirect("Home/index");
			}

	}

	public function main()
	{
		$user_id = $this->session->userdata['user_id'];
		$data['userData'] = $this->User_model->get_user_record($user_id);
		$data['number_modules'] = $this->Learning_model->count_available_modules_by_user($user_id);
		$data['modules_completed'] = $this->Learning_model->count_completed_modules_by_user($user_id);
		$data['lessons_completed'] = $this->Learning_model->count_completed_lessons_by_user($user_id);
		$this->session->set_userdata('activeMenu', 'm1');
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('main', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function logoff()
	{
	$data = array(
			  'user_id' => $this->session->userdata('user_id')
			, 'module_id' => 0
			, 'lesson_id' => 0
			, 'event_code' => LOG_SIGNOUT
			, 'event_tag' => LOG_SIGNOUT_TAG
			, 'log_date' => date('Y-m-d H:i:s')
			);
		$response = $this->Learning_model->log_lesson_activity($data);
		$this->session->sess_destroy();
		redirect ('Home/index');
	}

	public function show_error()
	{
		$this->load->view('common/pageHeader');
		$this->load->view('showerror');
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	/* session has timed out */
	public function timeout()
	{
		$this->load->view('common/pageHeader');
		$this->load->view('timeout');
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function my_account()
	{
		$this->session->set_userdata('activeMenu', 'm3');
		$user_id = $this->session->userdata['user_id'];
		$data['user_id'] = $user_id;
		$data['userData'] = $this->User_model->get_user_record($user_id);
		$data['addressTypes'] = $this->User_model->get_address_types();
		$data['stateCodes'] = $this->User_model->get_states();
		$data['timeZones'] = $this->User_model->get_time_zones();
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('user/my_account', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function my_account_post()
	{
		if($this->input->post("user_id") !== NULL) {$user_id = $this->input->post("user_id");} else {$user_id = '';}
		if($this->input->post("password") !== NULL) {$password = $this->input->post("password");} else {$password = '';}
		if($this->input->post("display_name") !== NULL) {$display_name = $this->input->post("display_name");} else {$display_name = '';}
		if($this->input->post("title") !== NULL) {$title = $this->input->post("title");} else {$title = '';}
		if($this->input->post("first_name") !== NULL) {$first_name = $this->input->post("first_name");} else {$first_name = '';}
		if($this->input->post("mid_name") !== NULL) {$mid_name = $this->input->post("mid_name");} else {$mid_name = '';}
		if($this->input->post("last_name") !== NULL) {$last_name = $this->input->post("last_name");} else {$last_name = '';}
		if($this->input->post("degree") !== NULL) {$degree = $this->input->post("degree");} else {$degree = '';}
		$modified_date = date('Y-m-d H:i:s');
		$modified_by = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
		if($password != '') {
			$user_password = $this->User_model->encrypt_decrypt('encrypt', $password);
		} else {
			$user_password = '';
		}
		$data = array(
			'user_id' => $user_id,
			'password' => $user_password,
			'display_name' => $display_name,
			'title' => $title,
			'first_name' => $first_name,
			'mid_name' => $mid_name,
			'last_name' => $last_name,
			'degree' => $degree,
			'modified_date' => $modified_date,
			'modified_by' => $modified_by
		);
		$response = $this->User_model->update_user_account($data);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
	redirect('Home/my_account/');
	}

	public function my_account_contact_post()
	{
		if($this->input->post("user_id") !== NULL) {$user_id = $this->input->post("user_id");} else {$user_id = '';}
		if($this->input->post("address_type") !== NULL) {$address_type = $this->input->post("address_type");} else {$address_type = 0;}
		if($this->input->post("address1") !== NULL) {$address1 = $this->input->post("address1");} else {$address1 = '';}
		if($this->input->post("address2") !== NULL) {$address2 = $this->input->post("address2");} else {$address2 = '';}
		if($this->input->post("city") !== NULL) {$city = $this->input->post("city");} else {$city = '';}
		if($this->input->post("state") !== NULL) {$state = $this->input->post("state");} else {$state = '';}
		if($this->input->post("postal_code") !== NULL) {$postal_code = $this->input->post("postal_code");} else {$postal_code = '';}
		if($this->input->post("phone") !== NULL) {$phone = $this->input->post("phone");} else {$phone = '';}
		if($this->input->post("phone_ext") !== NULL) {$phone_ext = $this->input->post("phone_ext");} else {$phone_ext = '';}
		if($this->input->post("time_zone") !== NULL) {$time_zone = $this->input->post("time_zone");} else {$time_zone = '';}
		$modified_date = date('Y-m-d H:i:s');
		$modified_by = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
		$data = array(
			'user_id' => $user_id,
			'address_type' => $address_type,
			'address1' => $address1,
			'address2' => $address2,
			'city' => $city,
			'state' => $state,
			'postal_code' => $postal_code,
			'phone' => $phone,
			'phone_ext' => $phone_ext,
			'time_zone' => $time_zone,
			'modified_date' => $modified_date,
			'modified_by' => $modified_by
		);
		$response = $this->User_model->update_user_contact($data);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
	redirect('Home/my_account/');
	}

	/* password reset request */
	public function resetpswd()
	{
		$this->load->view('common/pageHeader');
		$this->load->view('reset_request');
		$this->load->view('common/pageFooterLogin');
		$this->load->view('common/pageScripts');
	}

	/* validate the email address entered */
	public function reset_validate()
	{
		$email_address = $this->input->post("email_address");
		$userData = $this->User_model->get_user_by_email($email_address);
		if($userData) {
			$emStatus = $this->Admin_model->send_reset_request($userData['user_id']);
			if($emStatus[0] == 'success') {
				$data = array(
							'user_id' 		=> $this->session->userdata('user_id')
						, 'module_id' 	=> 0
						, 'lesson_id' 	=> 0
						, 'event_code' 	=> LOG_PASSWORD_REQUEST
						, 'event_tag' 	=> LOG_PASSWORD_REQUEST_TAG
						, 'log_date' 		=> date('Y-m-d H:i:s')
					);
				$response = $this->Learning_model->log_lesson_activity($data);
				redirect('Home/reset_ok/0');
			} else {
				redirect('Home/reset_error');
			}
		} else {
			$this->session->set_flashdata("reset_error", 'Request failed:<br />Email address invalid.');
			redirect('Home/resetpswd/');
		}
	}

	/* password reset request */
	public function reset_password()
	{
		$hashed_data = $this->uri->segment(3);
		$clear_data = $this->User_model->encrypt_decrypt('decrypt', $hashed_data);
		$userData = $this->User_model->get_user_by_email($clear_data);
		$data['clear_data'] = $clear_data;
		$data['userData'] = $userData;
		$data['userInitial'] = false;
		$this->load->view('common/pageHeader');
		$this->load->view('reset_password', $data);
		$this->load->view('common/pageFooterLogin');
		$this->load->view('common/pageScripts');
	}

	public function reset_password_post()
	{
		$user_id = $this->input->post("user_id");
		$email_address = $this->input->post("email_address");
		$password = $this->input->post("password");
		$hash_password = $this->User_model->encrypt_decrypt('encrypt', $password);
		$data = array (
			'user_id' => $user_id,
			'email_address' => $email_address,
			'password' => $hash_password
		);
		$response = $this->User_model->update_user_account($data);
		if($response) {
			$data = array(
						'user_id' 		=> $user_id
					, 'module_id' 	=> 0
					, 'lesson_id' 	=> 0
					, 'event_code' 	=> LOG_PASSWORD_RESET
					, 'event_tag' 	=> LOG_PASSWORD_RESET_TAG
					, 'log_date' 		=> date('Y-m-d H:i:s')
				);
			$response = $this->Learning_model->log_lesson_activity($data);
			$this->session->set_flashdata("success", 'Password reset successful!<br />Please sign in.');
			redirect('Home/index');
		} else {
			$this->session->set_flashdata("error", 'Password reset failed:<br />Please try again.');
			redirect('Home/resetpswd/');
		}
	}

	public function reset_ok()
	{
		$data['reset_complete'] = $this->uri->segment(3);
		$this->load->view('common/pageHeader');
		$this->load->view('reset_ok', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	/* password reset after first login */
	public function reset_password_initial()
	{
		$user_id = $this->session->userdata('user_id');
		$userData = $this->User_model->get_user_account($user_id);
		$data['userData'] = $userData;
		$data['userInitial'] = true;
		$this->load->view('common/pageHeader');
		$this->load->view('reset_password', $data);
		$this->load->view('common/pageFooterLogin');
		$this->load->view('common/pageScripts');
	}



}
