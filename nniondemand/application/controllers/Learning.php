<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Learning extends MY_Controller {

	public function __construct()
		{
				parent::__construct();
				$this->load->library('form_validation');
				$this->load->model('Admin_model');
				$this->load->model('User_model');
				$this->load->model('Learning_model');
		}

		public function modules()
		{
			if($this->uri->segment(3) != '') {
				$module_id = $this->uri->segment(3);
			} else {
				$module_id = 0;
			}
			$this->session->set_userdata('activeMenu', 'm2');
			$data['module_id'] = $module_id;
			$data['moduleList'] = $this->Learning_model->get_learning_modules('all');
			$data['userData'] = $this->User_model->get_user_record($this->session->userdata('user_id'));
			$this->load->view('common/pageHeader');
			$this->load->view('common/pageTop');
			$this->load->view('common/pageSideBar');
			$this->load->view('learning/my_modules', $data);
			$this->load->view('common/pageFooter');
			$this->load->view('common/pageScripts');
		}

	public function learning_modules_post()
	{
		if($this->input->post("module_id") !== null) {$module_id = $this->input->post("module_id");} else {$module_id = 0;}
		if($this->input->post("module_name") !== null) {$module_name = $this->input->post("module_name");} else {$module_name = '';}
		if($this->input->post("module_description") !== null) {$module_description = $this->input->post("module_description");} else {$module_description = '';}
		if($this->input->post("total_lessons") !== null) {$total_lessons = $this->input->post("total_lessons");} else {$total_sessions = '';}
		if($this->input->post('active') !== null) {$active = $this->input->post('active');} else {$active = 0;}
		$modified_date = date('Y-m-d H:i:s');
		$modified_by = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
		$data = array (
			'module_id' => $module_id,
			'module_name' => $module_name,
			'module_description' => $module_description,
			'total_lessons' => $total_lessons,
			'active' => $active,
			'modified_date' => $modified_date,
			'modified_by' => $modified_by
		);
		$response = $this->Learning_model->update_learning_module($data);
		if($response) {
			$this->session->set_flashdata("success", 'Update was successful!');
		} else {
			$this->session->set_flashdata("error", 'Update failed!');
		}
		// go to main page
		redirect('Admin/learning_modules/', 'refresh');
	}

	/* executes a lesson */
	public function play_lesson()
	{
		$lesson_id = $this->uri->segment(3);
		$lessonData = $this->Learning_model->get_lesson($lesson_id);
		$data['lesson_id'] = $lesson_id;
		$data['lessonData'] = $lessonData;
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('learning/play_lesson', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	/* logs lesson playback activity */
	public function log_lesson_activity()
	{
		$module_id = $_REQUEST['module_id'];
		$lesson_id = $_REQUEST['lesson_id'];
		$event_code = $_REQUEST['event_code'];
		$event_tag = $_REQUEST['event_tag'];
		$data = array(
			  'user_id' => $this->session->userdata('user_id')
			, 'module_id' => $module_id
			, 'lesson_id' => $lesson_id
			, 'event_code' => $event_code
			, 'event_tag' => $event_tag
			, 'log_date' => date('Y-m-d H:i:s')
		);
		//var_dump($data);
		$response = $this->Learning_model->log_lesson_activity($data);
	}

	/* logs lesson completed */
	public function log_lesson_complete()
	{
		$module_id = $_REQUEST['module_id'];
		$lesson_id = $_REQUEST['lesson_id'];
		$user_id = $this->session->userdata('user_id');
		$data = array(
			  'user_id' => $user_id
			, 'module_id' => $module_id
			, 'lesson_id' => $lesson_id
			, 'completed_date' => date('Y-m-d H:i:s')
		);
		$response = $this->Learning_model->update_lesson_log($data);
		// CHECK FOR COMPLETED MODULE
		$lessons_completed = $this->Learning_model->all_lessons_completed($user_id,$module_id);
		if($lessons_completed) {
			// MARK MODULE COMPLETED
			$data = array(
					'user_id' => $user_id
				, 'module_id' => $module_id
				, 'completed_date' => date('Y-m-d H:i:s')
			);
		$rtnCode = $this->Learning_model->set_module_complete($data);
		}
	}

	public function modules_completed()
	{
		$this->session->set_userdata('activeMenu', 'm2');
		$data['moduleList'] = $this->Learning_model->get_learning_modules('all');
		$data['userData'] = $this->User_model->get_user_record($this->session->userdata('user_id'));
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('learning/modules_completed', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function certificates()
	{
		$this->session->set_userdata('activeMenu', 'm2');
		$data['moduleList'] = $this->Learning_model->get_learning_modules('all');
		$data['userData'] = $this->User_model->get_user_record($this->session->userdata('user_id'));
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('learning/my_certificates', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}


}
