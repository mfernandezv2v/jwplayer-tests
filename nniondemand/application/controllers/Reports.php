<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {

	public function __construct()
		{
				parent::__construct();
				$this->load->library('session');
				$this->load->model('Admin_model');
				$this->load->model('User_model');
				$this->load->model('Learning_model');
		}


	public function user_progress()
	{
		$data['userList'] = $this->User_model->get_user_list('all');
		$this->session->set_userdata('activeMenu', 'm20');
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('reports/rpt_user_progress', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function show_user_progress()
	{
		$user_id = $_GET['user_id'];
		$userData = $this->User_model->get_user_record($user_id);
		$assignments = explode(",", $userData['assignments']);
		foreach($assignments as $assign) {
			$module = $this->Learning_model->get_learning_module($assign);
			$module_length = $this->Learning_model->calc_module_length($assign);
			$is_completed = $this->Learning_model->is_module_completed($user_id, $assign);
			$html = "<table class='table table-bordered table-striped table-hover table-condensed'>";
			$html .= "<thead><tr><th width='40%'>Module Name</th><th width='10%'>Lessons</th><th width='10%'>Length</th><th width='10%'>Completed</th><th>Date</th></tr></thead>";
			$html .= "<tbody>";
			$html .= "<tr>";
			$html .= "<td>".$module['module_name']."</td>";
			$html .= "<td>".$module['total_lessons']."</td>";
			$html .= "<td>".date('i:s',strtotime($module_length))."</td>";
			if($is_completed) {
				$html .= "<td><span class='label label-success'>&nbsp;Yes&nbsp;</span></td>";
				$html .= "<td>".date('m/d/Y',strtotime($is_completed['completed_date']))."</td>";
			}
			else {
				$html .= "<td><span class='label label-danger'>&nbsp;No&nbsp;</span></td>";
				$html .= "<td>TBD</td>";
			}
			$html .= "</tr>";
			$html .= "</tbody></table>";
			$html .= "<div class='table-responsive'>";
			$html .= "<table class='table table-bordered table-striped table-hover table-condensed' width='80%'>";
			$html .= "<thead><tr>";
			$html .= "<th class='font-custom-blue-mid' width='40%'>Lesson Name</th>";
			$html .= "<th class='font-custom-blue-mid' width='10%'>Total Time</th>";
			$html .= "<th class='font-custom-blue-mid' width='10%'>Status</th>";
			$html .= "<th class='font-custom-blue-mid' width='10%'>Date</th>";
			$html .= "</tr></thead>";
			$html .= "<tbody>";
			$lessons = $this->Learning_model->get_lessons_by_module($module['module_id']);
			foreach($lessons as $less) {
				$lesson_time = "00:".$less['lesson_length_min'].":".$less['lesson_length_sec'];
				$lesson_complete = $this->Learning_model->check_lesson_complete($less['lesson_id'], $user_id);
				$html .= "<tr>";
				$html .= "<td>".$less['lesson_name']."</td>";
				$html .= "<td>".date('i:s',strtotime($lesson_time))."</td>";
				$html .= "<td>";
				if($lesson_complete) {$html .= "<span class='label label-success'>&nbsp;&nbsp;Yes&nbsp;&nbsp;</span>";}
				else {$html .= "<span class='label label-danger'>&nbsp;&nbsp;No&nbsp;&nbsp;</span>";}
				$html .= "</td>";
				$html .= "<td>";
				if($lesson_complete['completed_date'] !== NULL) {
					$html .= date('m/d/Y', strtotime($lesson_complete['completed_date']));
					}
				$html .= "</td>";

				$html .= "</tr>";
			}
			$html .= "</tbody></table></div>";
			echo $html;
		}
}

	public function user_progress_summary()
	{
		$data['userList'] = $this->User_model->get_user_list_by_type(USER_TYPE_USER);
		$data['moduleList'] = $this->Learning_model->get_learning_modules_list('all');
		$this->session->set_userdata('activeMenu', 'm20');
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('reports/rpt_user_progress_summary', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function user_progress_summary_excel()
	{
		$data['userList'] = $this->User_model->get_user_list_by_type(USER_TYPE_USER);
		$data['moduleList'] = $this->Learning_model->get_learning_modules_list('all');
		$this->load->view('reports/rpt_user_progress_summary_excel', $data);
	}

	public function system_usage()
	{
		$data['userList'] = $this->User_model->get_user_list('all');
		$this->session->set_userdata('activeMenu', 'm20');
		$this->load->view('common/pageHeader');
		$this->load->view('common/pageTop');
		$this->load->view('common/pageSideBar');
		$this->load->view('reports/rpt_system_usage', $data);
		$this->load->view('common/pageFooter');
		$this->load->view('common/pageScripts');
	}

	public function show_system_usage()
	{
		$user_id = $_GET['user_id'];
		$userData = $this->User_model->get_user_record($user_id);
		$modules = $this->Learning_model->get_module_names();
		$lessons = $this->Learning_model->get_lesson_names();
		$activity = $this->Learning_model->get_user_activity($user_id);
		$html = "<div><a href='".base_url('Reports/system_usage_excel/').$user_id."' class='btn btn-md btn-info'> Export to Excel </a></div>";
		$html .= "<table class='table table-bordered table-striped table-hover table-condensed'>";
		$html .= "<caption><h4 class='bold'>System Usage Report for: ".$userData['last_name'].", ".$userData['first_name']."</h4></caption>";
		$html .= "<thead><tr><th>Module</th><th>Lesson</th><th>Code</th><th>Activity</th><th>Date</th></tr></thead>";
		$html .= "<tbody>";
		foreach($activity as $act) {
				$module_name = $lesson_name = '';
				foreach($modules as $idx => $mod) {
				if($act['module_id'] == $mod['module_id']) {
					$module_name = $mod['module_name'];
					break;
					}
				}
				foreach($lessons as $idx => $les) {
				if($act['lesson_id'] == $les['lesson_id']) {
					$lesson_name = $les['lesson_name'];
					break;
					}
				}
			$html .= "<tr>";
			$html .= "<td>".$module_name."</td>";
			$html .= "<td>".$lesson_name."</td>";
			$html .= "<td>".$act['event_code']."</td>";
			$html .= "<td>".$act['event_tag']."</td>";
			$html .= "<td>".date('m/d/Y H:i:s',strtotime($act['log_date']))."</td>";
			$html .= "</tr>";
			}
		$html .= "</tbody></table></div>";
		echo $html;
	}

	public function system_usage_excel()
	{
		$user_id = $this->uri->segment(3);
		$data['userData'] = $this->User_model->get_user_record($user_id);
		$data['modules'] = $this->Learning_model->get_module_names();
		$data['lessons'] = $this->Learning_model->get_lesson_names();
		$data['activity'] = $this->Learning_model->get_user_activity($user_id);
		$this->load->view('reports/rpt_system_usage_excel', $data);
	}


}
