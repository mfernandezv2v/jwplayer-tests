<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pagination_first_link'] = '&lsaquo; First';
$lang['pagination_next_link'] = '&gt;';
$lang['pagination_prev_link'] = '&lt;';
$lang['pagination_last_link'] = 'Last &rsaquo;';
/* APPLICATION TAGS  */
$lang['app_client_name'] = ' Novo Nordisk Inc ';
$lang['app_title'] = " Diabetes Speaker Bureau 2017 Training ";
/* FOOTER CONTENT */
$lang['footer_copyright'] = ' &copy; Novo Nordisk, Inc';
$lang['footer_contact_info'] = ' ';
$lang['footer_line1'] = 'Novo Nordisk is a registered trademark of Novo Nordisk A/S ';
$lang['footer_line2'] = ' ';
/* MAIN PAGE LABELS */
$lang['main_info_title'] = ' Welcome to Novo Nordisk Inc.<br />Diabetes Speaker Bureau 2017 Training ';
$lang['main_info_content'] = "This site provides access to the materials you will need to review as part of
the 2017 Diabetes Speaker Bureau Training requirements. We respectfully request you complete your review of
the assigned training requirements by Friday March 24, 2017.  If this is not possible, please provide notice by
sending an e-mail to <a href='mailto: NNISpeakerQuestions@novonordisk.com'>NNISpeakerQuestions@novonordisk.com.</a>
<br /><br />If you have questions regarding this site, you should contact Nancy Lauffenberger by email: <a href='mailto:nlauffenberger@vision2voice.com'>nlauffenberger@vision2voice.com</a>
or by calling 847-964-9696 ext. 2121. ";
/* CERTIFICATES LABELS */
$lang['certificate_not_available']  = "'Certificates of Completion' are not available until all required training modules have been completed";
$lang['certificate_info_1'] = "<h3 class='font-custom-blue-mid bold'>Congratulations!</h3>";
$lang['certificate_info_2'] = "Having completed your 2017 training requirements, we just need you to sign and return the 'Certificate of Completion.'";
$lang['certificate_info_3'] = "To do this, please download, open, print and return the 'Certificate of Completion' with
your signature, printed name and date by email to <a href='mailto: NNISpeakerQuestions@novonordisk.com' class='bold'>NNISpeakerQuestions@novonordisk.com</a>
or fax to <strong>609-419-3195.</strong>  Since this document is required to be kept on file to verify your completion of all training activity, we are not
able to activate your speaker status without it.";
$lang['certificate_info_4'] = "With the completed certificate received, your training will be confirmed as complete,
training payment will be issued, you will be selectable as a speaker in our system for programming and we will send
a 'Thank You' email containing some important information for your reference.  Our system is updated weekly to account
for the most current training status of our speakers so you may be selected to present programs you have been trained to deliver.";
$lang['certificate_info_5'] = "To download the 'Certificate of Completion' please click the download button below.";
/* INPUT PAGE LABELS  */
$lang['panel_registration_title'] = ' Registration Information ';
$lang['label_salutation'] = ' Salutation ';
$lang['label_firstname'] = ' First Name ';
$lang['label_midname'] = ' Middle Name ';
$lang['label_lastname'] = ' Last Name ';
$lang['label_degree'] = ' Degree ';
$lang['label_degree_other'] = ' If other, please specifiy ';
$lang['label_specialty'] = ' Specialty ';
$lang['label_practice'] = ' Practice Name or Affiliation ';
$lang['label_address1'] = ' Office Address ';
$lang['label_address2'] = ' Office Address 2 ';
$lang['label_city'] = ' City ';
$lang['label_state'] = ' State ';
$lang['label_postalcode'] = ' Postal Code ';
$lang['label_businessphone'] = ' Best Phone Number ';
$lang['label_mobilephone'] = ' Mobile Phone Number ';
$lang['label_businessfax'] = ' Office Fax Number ';
$lang['label_email'] = ' Email Address ';
$lang['label_timezone'] = ' Time Zone ';
$lang['label_terrid'] = ' Territory ID ';
$lang['label_menum'] = ' ME Number ';
$lang['label_npi'] = ' NPI Number ';
$lang['label_statelicense'] = ' State License Number ';
$lang['label_attendtype'] = ' Attendee Type ';
$lang['label_attendlocation'] = ' Attend at Location ';
$lang['label_message_block'] = ' Please provide a phone number where you can be reached after hours should we need to contact you regarding your scheduled event.&nbsp;&nbsp;<small>(Phone number and Email address needed for registration confirmation and any other relevant communications)</small>';
/* BUTTON TAGS */
$lang['button_save_continue'] = ' Save and Continue ';
$lang['button_submit_survey'] = ' Submit Survey ';
/* THANK YOU PAGE */
$lang['thankyou_title'] = ' Your Event Registration Is Complete ';
$lang['thankyou_paragraph_1'] = ' Thank you!  Your registration is now complete.<br />You will receive an email confirmation shortly. ';
$lang['thankyou_paragraph_2'] = ' You have been registered for the following web event:<br /><br /> ';
$lang['thankyou_footer'] = ' Please print this page for your reference. ';
/* MENU AND BANNER TAGS */
$lang['topmenu_title'] = ' <span>Registration </span>&nbsp;&nbsp;ADMIN</span> ';
/* POST EVENT SURVEY TAGS */
/* adjust config.inputconfig to include/exclude survey questions */
$lang['survey_title'] = ' Vision2Voice Annual Meeting - Post Event Review ';
$lang['survey_greeting'] = ' Thank you for completing the post event review.  We appreciate your time and commitment.<br /><br /> ';
$lang['survey_greeting_alt'] = ' ';
$lang['survey_q1_label'] = ' 1.  Where We\'ve Been and Where We Are Going? (Dan Rehal)';
$lang['survey_q1_comment_label'] = '';
$lang['survey_q2_label'] = ' 2.  Impact of Peer-to-Peer (Howard Drazner)';
$lang['survey_q2_comment_label'] = ' ';
$lang['survey_q3_label'] = ' 3.  Employee Handbook Updates (Scott Atkins) ';
$lang['survey_q3_comment_label'] = ' ';
$lang['survey_q4_label'] = ' 4.  Raving Fans (Nancy Lauffenberger and Lisa Dougherty) ';
$lang['survey_q4_comment_label'] = ' ';
$lang['survey_q5_label'] = ' 5.  Vision and Mission (Dan Rehal) ';
$lang['survey_q5_comment_label'] = ' ';
$lang['survey_q6_label'] = '6.  Advancing Our IT IQ (Ben Jurevicius)';
$lang['survey_q6_comment_label'] = '';
$lang['survey_q7_label'] = '7.  Professional Presence (Jennifer Pearsall) ';
$lang['survey_q7_comment_label'] = '';
$lang['survey_q8_label'] = '8.  How effectively did we meet our goals for this meeting? ';
$lang['survey_q8_comment_label'] = '';
$lang['survey_q9_label'] = '9. Which portions were most valuable to you from this meeting?';
$lang['survey_q9_comment_label'] = '';
$lang['survey_q10_label'] = '10.  Please comment on Loews Hotel and Conference room.';
$lang['survey_q10_comment_label'] = '';
$lang['survey_q11_label'] = '11.  Please comment on President Plaza.';
$lang['survey_q11_comment_label'] = '';
$lang['survey_q12_label'] = '12.  Please comment on the meals at McCormick and Schmicks, Capital Grille and/or Ashburn Restaurant.  ';
$lang['survey_q12_comment_label'] = '';
$lang['survey_q13_label'] = '13.  Please share your thoughts on the Vision2Voice core values/hand print banner exercise. ';
$lang['survey_q13_comment_label'] = '';
$lang['survey_q14_label'] = '14.  What would you like to see done differently to improve the Annual Meeting for next year? ';
$lang['survey_q14_comment_label'] = '';
$lang['survey_q15_label'] = '';
$lang['survey_q15_comment_label'] = '';
$lang['survey_q16_label'] = '';
$lang['survey_q16_comment_label'] = '';
$lang['survey_q17_label'] = '';
$lang['survey_q17_comment_label'] = '';
$lang['survey_q18_label'] = '';
$lang['survey_q18_comment_label'] = '';
$lang['survey_q19_label'] = '';
$lang['survey_q19_comment_label'] = '';
$lang['survey_q20_label'] = '';
$lang['survey_q20_comment_label'] = '';
$lang['survey_feedback_label'] = ' Comments/Feedback ';
/* POST EVENT */
$lang['postevent_banner'] = "<?php echo base_url('assets/img/sustain_page_banner.png');?> ";
$lang['postevent_message'] = " Thank you for completing this online survey.<br /><br />We appreciate your time and commitment. ";
