<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');

class Admin_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  /*
   * Get address types
   */
  function get_address_types()
  {
      $this->db->select('*');
      $this->db->from('ref_address_types');
      $query = $this->db->get();
      return $query->result_array();
  }

  /*
   * Get address type record
   */
  function get_address_type($address_type_id)
  {
      $this->db->select('*');
      $this->db->from('ref_address_types');
      $this->db->where('address_type_id', $address_type_id);
      $query = $this->db->get();
      return $query->row_array();
  }

  function update_address_type($data) {
    $response = false;
    $this->db->select('address_type_id');
    $this->db->from('ref_address_types');
    $this->db->where('address_type_id', $data['address_type_id']);
    $rowCount = $this->db->count_all_results();
    if($rowCount < 1) {
      $response = $this->db->insert('ref_address_types',$data);
    } else {
      $this->db->where('address_type_id',$data['address_type_id']);
      $response = $this->db->update('ref_address_types',$data);
    }
    return $response;
  }

  /*
   * Get states list
   */
  function get_states()
  {
      $this->db->select('*');
      $this->db->from('ref_states');
      $query = $this->db->get();
      return $query->result_array();
  }

  /*
   * Get one state types
   */
  function get_state($state_id)
  {
      $this->db->select('*');
      $this->db->from('ref_states');
      $this->db->where('state_id', $state_id);
      $query = $this->db->get();
      return $query->row_array();
  }

  function update_state($data) {
    $response = false;
    $this->db->select('state_id');
    $this->db->from('ref_states');
    $this->db->where('state_id', $data['state_id']);
    $rowCount = $this->db->count_all_results();
    if($rowCount < 1) {
      $response = $this->db->insert('ref_states',$data);
    } else {
      $this->db->where('state_id',$data['state_id']);
      $response = $this->db->update('ref_states',$data);
    }
    return $response;
  }

  /*
   * Get time zones
   */
  function get_time_zones()
  {
      $this->db->select('*');
      $this->db->from('ref_time_zones');
      $query = $this->db->get();
      return $query->result_array();
  }

  /*
   * Get one time zone
   */
  function get_time_zone($time_zone_id)
  {
      $this->db->select('*');
      $this->db->from('ref_time_zones');
      $this->db->where('time_zone_id', $time_zone_id);
      $query = $this->db->get();
      return $query->row_array();
  }

  function update_time_zone($data) {
    $response = false;
    $this->db->select('time_zone_id');
    $this->db->from('ref_time_zones');
    $this->db->where('time_zone_id', $data['time_zone_id']);
    $rowCount = $this->db->count_all_results();
    if($rowCount < 1) {
      $response = $this->db->insert('ref_time_zones',$data);
    } else {
      $this->db->where('time_zone_id',$data['time_zone_id']);
      $response = $this->db->update('ref_time_zones',$data);
    }
    return $response;
  }

  /*
   * Get smtp data
   */
  function get_smtp($sys_smtp_id)
  {
      $this->db->select('*');
      $this->db->from('sys_smtp');
      $this->db->where('sys_smtp_id', $sys_smtp_id);
      $query = $this->db->get();
      return $query->row_array();
  }

  /*
   * Get SMTP params
   */
  function get_sys_smtp()
  {
    $this->db->select('*');
    $this->db->from('sys_smtp');
    $this->db->limit(1);
    $query = $this->db->get();
    return $query->row_array();
  }

  /*
   * Get default SMTP mail info
   */
  function get_sys_smpt_default()
  {
      $this->db->select('*');
      $this->db->from('sys_smtp');
      $this->db->where('sys_smtp_id', SYS_SMTP_DEFAULT);
      $query = $this->db->get();
      $type = $query->row_array();
      return $type;
  }

  /*
   * Update smtp params
   */
  function update_sys_smtp($data)
  {
      $this->db->where('sys_smtp_id',$data['sys_smtp_id']);
      $response = $this->db->update('sys_smtp',$data);
      if($response) {
          return "SMTP params updated successfully";
      } else {
          return "SMTP params update failed";
      }
  }

  /*
   * Add new sys_smtp row
   */
  function add_sys_smtp($data)
  {
      //return $this->db->insert_id();
      $response = $this->db->insert('sys_smtp',$data);
      if($response) {
          return "SMTP params inserted successfully";
      } else {
          return "SMTP params insert failed";
      }
  }

  /*
   * Get Email Template
   */
  function get_sys_email_template($em_template_id)
  {
      $query = $this->db->query("SELECT * FROM sys_email_template WHERE em_template_id = '".$em_template_id."' ");
      return $query->result();
  }

  /*
   * Get Email Template by name
   */
  function get_sys_email_template_by_name($em_name)
  {
      $query = $this->db->query("SELECT * FROM sys_email_template WHERE em_name = '".$em_name."' ");
      return $query->result();
  }

  /*
   * Get Email Template
   */
  function get_sys_email_template_list()
  {
      $query = $this->db->query('select em_template_id, em_name, em_to, em_from, em_subject from sys_email_template ORDER BY em_name ');
      return $query->result();
  }

  /*
   * Add new email tempalte row
   */
  function add_email_template($data)
  {
      //return $this->db->insert_id();
      $response = $this->db->insert('sys_email_template',$data);
      if($response) {
          return "Email template inserted successfully";
      } else {
          return "Email template insert failed";
      }
  }

  /*
   * Update Email Template params
   */
  function update_email_template($data)
  {
      $this->db->where('em_template_id',$data['em_template_id']);
      $response = $this->db->update('sys_email_template',$data);
      if($response) {
          return "Email template updated successfully";
      } else {
          return "Email template update failed";
      }
  }

  /*
  * delete an email tempalte
  */
  function remove_sys_email_template($em_template_id)
 {
    $this->db->where('em_template_id',$em_template_id);
    $this->db->delete('sys_email_template');
 }

 /*
  * Get Reminder Email Template
  */
 function get_reminder_email_template_list()
 {
    $SQL = "SELECT em_template_id, em_name, em_to, em_from, em_subject ";
    $SQL .= "FROM sys_email_template ";
    $SQL .= "WHERE em_name LIKE '%_reminder%' ";
    $SQL .= "ORDER BY em_name ";
    $query = $this->db->query($SQL);
    return $query->result();
 }

 /*
  * Get No SHow Email Templates
  */
 function get_noshow_email_template_list()
 {
    $SQL = "SELECT em_template_id, em_name, em_to, em_from, em_subject ";
    $SQL .= "FROM sys_email_template ";
    $SQL .= "WHERE em_name LIKE '%_noshow%' ";
    $SQL .= "ORDER BY em_name ";
    $query = $this->db->query($SQL);
    return $query->result();
 }

 /*
  * Send password reset request
  */
 function send_reset_request($user_id)
 {
    $smtp = $this->get_sys_smtp();
    $smtp_password = $this->User_model->encrypt_decrypt('decrypt', $smtp['sys_smtp_password']);
    $userData = $this->User_model->get_user_record($user_id);
    $emailTemplate = $this->get_sys_email_template_by_name(TEMPLATE_PASSWORD_RESET);
    require_once 'PHPMailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    // Enable verbose debug output
    $mail->SMTPDebug = 0;
    // Set mailer to use SMTP
    $mail->isSMTP();
    // Specify main SMTP server
    $mail->Host = $smtp['sys_smtp_server'];
    // Enable SMTP authentication
    $mail->SMTPAuth = true;
    // SMTP username
    $mail->Username = $smtp['sys_smtp_account'];
     // SMTP password
    $mail->Password = $smtp_password;
    // Enable TLS encryption, `ssl` also accepted
    if($smtp['sys_smtp_secure'] == 'none') {
      $mail->SMTPSecure = NULL;
    } else {
      $mail->SMTPSecure =$smtp['sys_smtp_secure'];
    }
    $mail->Port = 25; //$smtp->sys_smtp_port;
    // Hack to resolve SSL conneciton issues
    $mail->SMTPOptions = array(
     'ssl' => array(
     'verify_peer' => false,
     'verify_peer_name' => false,
     'allow_self_signed' => true
       )
     );
     // Set email format to HTML
     $mail->isHTML(true);
     // clear addresses
     $mail->ClearAddresses();  // each AddAddress add to list
     $mail->ClearCCs();
     $mail->ClearBCCs();
     // Set recipients
     $mail->From = $smtp['sys_smtp_account'];
     $mail->FromName = $smtp['sys_smtp_account_name'];
     $mail->addReplyTo($smtp['sys_smtp_account']);
     $mail->addAddress($userData['email_address']);
     // $mail->addCC($cc);
     $mail->addBCC($smtp['sys_support_account']);
    // parse the subject and email body for merge tags before sending
    $emailSubject = $this->merge_email_tags($user_id, $emailTemplate[0]->em_subject);
    $mail->Subject = $emailSubject;
    $emailMessage = $this->merge_email_tags($user_id, $emailTemplate[0]->em_message);
    $mail->Body = $emailMessage;
    // send te message
    $returnStatus = array();
    if(!$mail->send()) {
       $returnStatus[0] = 'error';
       $returnStatus[1] = 'Mailer Error: '.$mail->ErrorInfo;
     } else {
      $returnStatus[0] = 'success';
      $returnStatus[1] = 'Message has been sent';
     }
    return $returnStatus;
}

/*
* Replace any email tags with valid data
*/
function merge_email_tags($user_id, $text) {
   $userData = $this->User_model->get_user_record($user_id);
   $smtp = $this->get_sys_smtp();
   //
   if(strpos($text, "{FULL-NAME}")) {
     $full_name = '';
     if($userData['title'] != '') $full_name .= $userData['title'];
     $full_name = $userData['first_name']." ".$userData['last_name'];
     $text = str_replace("{FULL-NAME}", $full_name, $text);
   }
   if(strpos($text, "{FIRST-NAME}")) {
     $text = str_replace("{FIRST-NAME}", $userData['first_name'], $text);
   }
   if(strpos($text, "{LAST-NAME}")) {
     $text = str_replace("{LAST-NAME}", $userData['last_name'], $text);
   }
   if(strpos($text, "{EMAIL}")) {
     $text = str_replace("{EMAIL}", $userData['email_address'], $text);
   }
   if(strpos($text, "{HASH-EMAIL}")) {
     $hash_email = $this->User_model->encrypt_decrypt('encrypt', $userData['email_address']);
     $text = str_replace("{HASH-EMAIL}", $hash_email, $text);
   }
   if(strpos($text, "{SUPPORT-TEAM}")) {
     $text = str_replace("{SUPPORT-TEAM}", $smtp['sys_support_name'], $text);
   }
   if(strpos($text, "{SUPPORT-EMAIL}")) {
     $text = str_replace("{SUPPORT-EMAIL}", $smtp['sys_support_account'], $text);
   }
   return $text;
 }



} // end class
