<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');

class Learning_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  /*
   * Get modules
   */
  function get_learning_modules($type)
  {
    $this->db->select('*');
    $this->db->from('modules');
    if($type == 'active') {
      $this->db->where('active', 1);
    } else if($type == 'inactive') {
      $this->db->where('active', 0);
    }
    $this->db->order_by('module_name asc');
    $query = $this->db->get();
    return $query->result_array();
  }

  /*
   * Get modules in ID order
   */
  function get_learning_modules_list($type)
  {
    $this->db->select('*');
    $this->db->from('modules');
    if($type == 'active') {
      $this->db->where('active', 1);
    } else if($type == 'inactive') {
      $this->db->where('active', 0);
    }
    $this->db->order_by('module_id asc');
    $query = $this->db->get();
    return $query->result_array();
  }


  /*
   * Get one module
   */
  function get_learning_module($module_id)
  {
    $this->db->select('*');
    $this->db->from('modules');
    $this->db->where('module_id', $module_id);
    $query = $this->db->get();
    return $query->row_array();
  }

  function update_learning_module($data) {
    $response = false;
    $this->db->select('module_id');
    $this->db->from('modules');
    $this->db->where('module_id', $data['module_id']);
    $rowCount = $this->db->count_all_results();
    if($rowCount < 1) {
      $response = $this->db->insert('modules',$data);
    } else {
      $this->db->where('module_id',$data['module_id']);
      $response = $this->db->update('modules',$data);
    }
    return $response;
  }

  /*
   * Get module total time
   */
  function get_module_total_time($module_id)
  {
    $this->db->select('sum(lesson_length_min) as total_minutes, sum(lesson_length_sec) as total_seconds');
    $this->db->from('lessons');
    $this->db->where('module_id', $module_id);
    $query = $this->db->get();
    return $query->row_array();
  }

  /*
   * Get lessons
   */
  function get_lessons($type)
  {
    $this->db->select('*');
    $this->db->from('lessons');
    if($type == 'active') {
      $this->db->where('active', 1);
    } else if($type == 'inactive') {
      $this->db->where('active', 0);
    }
    $this->db->order_by('module_id');
    $this->db->order_by('lesson_order');
    $query = $this->db->get();
    return $query->result_array();
  }

  /*
   * Get lessons by module
   */
  function get_lessons_by_module($module_id)
  {
    $this->db->select('*');
    $this->db->from('lessons');
    $this->db->where('module_id', $module_id);
    $this->db->order_by('lesson_order');
    $query = $this->db->get();
    return $query->result_array();
  }

  /*
   * Get one module
   */
  function get_lesson($lesson_id)
  {
    $this->db->select('*');
    $this->db->from('lessons');
    $this->db->where('lesson_id', $lesson_id);
    $query = $this->db->get();
    return $query->row_array();
  }

  function update_lesson($data) {
    $response = false;
    $this->db->select('lesson_id');
    $this->db->from('lessons');
    $this->db->where('lesson_id', $data['lesson_id']);
    $rowCount = $this->db->count_all_results();
    if($rowCount < 1) {
      $this->db->insert('lessons',$data);
      $response = $this->db->insert_id();
    } else {
      $this->db->where('lesson_id',$data['lesson_id']);
      $response = $this->db->update('lessons',$data);
    }
    return $response;
  }

  function check_lesson_complete($lesson_id, $user_id)
  {
    $this->db->select('*');
    $this->db->from('lesson_log');
    $this->db->where('lesson_id', $lesson_id);
    $this->db->where('user_id', $user_id);
    $query = $this->db->get();
    return $query->row_array();
  }

  function log_lesson_activity($data)
  {
    $this->db->insert('lesson_activity',$data);
    $response = $this->db->insert_id();
    return $response;
  }

  /* marks a lesson complete */
  function update_lesson_log($data)
  {
    $this->db->select('lesson_id');
    $this->db->from('lesson_log');
    $this->db->where('lesson_id', $data['lesson_id']);
    $this->db->where('user_id', $data['user_id']);
    $rowCount = $this->db->count_all_results();
    if($rowCount < 1) {
      $this->db->insert('lesson_log',$data);
      $response = $this->db->insert_id();
    } else {
      $response = true;
    }
    return $response;
  }

  /* count available modules by user */
  function count_available_modules_by_user($user_id)
  {
    $this->db->select('assignments');
    $this->db->from('user_account');
    $this->db->where('user_id', $user_id);
    $result = $this->db->get()->row()->assignments;
    $num_modules = explode(',', $result);
    return count($num_modules);
  }

  /* count completed modules by user */
  function count_completed_modules_by_user($user_id)
  {
    $this->db->select('COUNT(module_id) AS module_count');
    $this->db->from('module_log');
    $this->db->where('user_id', $user_id);
    $this->db->where('completed_date IS NOT NULL');
    return $this->db->get()->row()->module_count;
  }

  /* get completed modules by user */
  function get_completed_modules_by_user($user_id)
  {
    $this->db->select('*');
    $this->db->from('module_log');
    $this->db->where('user_id', $user_id);
    $query = $this->db->get();
    return $query->row_array();
  }

  /* see if module has been completed by user */
  function is_module_completed($user_id, $module_id)
  {
    $this->db->select('*');
    $this->db->from('module_log');
    $this->db->where('user_id', $user_id);
    $this->db->where('module_id', $module_id);
    $query = $this->db->get();
    return $query->row_array();
  }

  /* count completed lessons by user */
  function count_completed_lessons_by_user($user_id)
  {
    $this->db->select('COUNT(lesson_id) AS lesson_count');
    $this->db->from('lesson_log');
    $this->db->where('user_id', $user_id);
    $this->db->where('completed_date IS NOT NULL');
    return $this->db->get()->row()->lesson_count;
  }


  /* see if all module lessons have been completed by user */
  function all_lessons_completed($user_id, $module_id)
  {
    $lesson_count = 0;
    $this->db->select('COUNT(lesson_log_id) AS lesson_count');
    $this->db->from('lesson_log');
    $this->db->where('user_id', $user_id);
    $this->db->where('module_id', $module_id);
    $lesson_count = $this->db->get()->row()->lesson_count;
    // get module record
    $this->db->select('*');
    $this->db->from('modules');
    $this->db->where('module_id', $module_id);
    $query = $this->db->get();
    $module = $query->row_array();
    if($lesson_count == $module['total_lessons']) {
      return true;
    } else {
      return false;
    }
  }

  /* see if prerequisite is completed */
  function is_prerequisite_completed($user_id, $module_id)
  {
    $this->db->select('*');
    $this->db->from('module_log');
    $this->db->where('user_id', $user_id);
    $this->db->where('module_id', $module_id);
    $query = $this->db->get();
    return $query->row_array();
  }

  // CALC TOTAL TIME
  function calc_module_length($module_id) {
    $mod_length = $this->get_module_total_time($module_id);
    $total_hours = $total_minutes = $total_seconds = 0;
    if($mod_length['total_seconds'] >= '60') {
      $total_minutes = intval($mod_length['total_seconds'] / 60);
      $total_seconds = $mod_length['total_seconds'] % 60;
    } else {
      $total_seconds = $mod_length['total_seconds'];
    }
    if($mod_length['total_minutes'] >= '60') {
      $total_hours = intval($mod_length['total_minutes'] / 60);
      $total_minutes += intval($mod_length['total_minutes'] / 60);
    } else {
      $total_minutes += $mod_length['total_minutes'];
    }
    $module_length = $total_hours.":".$total_minutes.":".$total_seconds;
    return $module_length;
  }

  function set_module_complete($data) {
    $this->db->select('module_id');
    $this->db->from('module_log');
    $this->db->where('user_id', $data['user_id']);
    $this->db->where('module_id', $data['module_id']);
    $rowCount = $this->db->count_all_results();
    if($rowCount < 1) {
      $this->db->insert('module_log',$data);
      $response = $this->db->insert_id();
    } else {
      $response = true;
    }
    return $response;
  }

  /*
   * Get list of module names only
   */
  function get_module_names()
  {
    $this->db->select('module_id, module_name');
    $this->db->from('modules');
    $this->db->order_by('module_id ASC');
    $query = $this->db->get();
    return $query->result_array();
  }

  /*
   * Get list of lesson names only
   */
  function get_lesson_names()
  {
    $this->db->select('lesson_id, lesson_name');
    $this->db->from('lessons');
    $this->db->order_by('lesson_id ASC');
    $query = $this->db->get();
    return $query->result_array();
  }

  function get_user_activity($user_id)
  {
    $this->db->select('*');
    $this->db->from('lesson_activity');
    $this->db->where('user_id', $user_id);
    $this->db->order_by('log_date ASC');
    $query = $this->db->get();
    return $query->result_array();
  }

} // end class
