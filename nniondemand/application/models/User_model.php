<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');

class User_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  /*
   * Get events for the desiganted event group
   */
  function get_user_login($data)
  {

      $password = $this->encrypt_decrypt('encrypt', $data['password']);
      $this->db->select('*');
      $this->db->from('user_account');
      $this->db->where('email_address', $data['email_address']);
      $this->db->where('password', $password);
      $this->db->where('active', 1);
      $query = $this->db->get();
      return $query->row_array();

  }

  /*
   * Get events for a participant
   */
  function get_user_record($user_id)
  {
    $query = "SELECT a.user_id, a.user_type, a.email_address, a.password, a.active ";
    $query .= ", c.address_type, c.address1, c.address2, c.city, c.state, c.postal_code, c.phone, c.phone_ext, c.time_zone ";
    $query .= ", a.display_name, a.title, a.first_name, a.mid_name, a.last_name, a.degree, a.assignments ";
    $query .= "FROM user_account a, user_contact c ";
    $query .= "WHERE a.user_id = '".$user_id."' ";
    $query .= "AND a.user_id = c.user_id ";
    $result = $this->db->query($query);
    return $result->row_array(); 
  }

  function encrypt_decrypt($action, $string) {
       $output = false;
       $encrypt_method = "AES-256-CBC";
       $key = 'Cl3arV1si0N2020';
       $ivKey = 'v2v60015';

       // hash
       $key = hash('sha256', $key);
       // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
       $iv = substr(hash('sha256', $ivKey), 0, 16);
       if( $action == 'encrypt' ) {
           $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
           $output = base64_encode($output);
       }
       else if( $action == 'decrypt' ){
           $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
       }
       return $output;
   }

   /*
    * Get system user
    */
   function get_user_account($user_id)
   {
       $this->db->select('*');
       $this->db->from('user_account');
       $this->db->where('user_id', $user_id);
       $query = $this->db->get();
       return $query->row_array();
   }

   /*
    * Get user account by email_address
    */
   function get_user_by_email($email_address)
   {
       $this->db->select('*');
       $this->db->from('user_account');
       $this->db->where('email_address', $email_address);
       $this->db->limit(1);
       $query = $this->db->get();
       return $query->row_array();
   }

   /*
    * Get list of users
    */
   function get_user_list($status_type)
   {
     if($status_type == 'active') {
       $status = 1;
     } else if($status_type == 'inactive') {
       $status = 0;
     } else $status = NULL;
     $this->db->select('*');
     $this->db->from('user_account');
     if($status != NULL) $this->db->where('active', $status);
     $this->db->order_by('last_name','first_name');
     $query = $this->db->get();
     return $query->result_array();
   }

   /*
    * Get list of users
    */
   function get_user_list_by_type($user_type)
   {
     $this->db->select('*');
     $this->db->from('user_account');
     $this->db->where('user_type', $user_type);
     $this->db->order_by('last_name','first_name');
     $query = $this->db->get();
     return $query->result_array();
   }


   /*
    * Get all system users
    */
   function get_all_users()
   {
     $query = "SELECT a.user_id, a.user_type, a.email_address, a.password, a.active ";
     $query .= ", c.address_type, c.address1, c.address2, c.city, c.state, c.postal_code, c.phone, c.phone_ext, c.time_zone ";
     $query .= ", a.display_name, a.title, a.first_name, a.mid_name, a.last_name, a.degree ";
     $query .= "FROM user_account a, user_contact c ";
     $query .= "WHERE a.user_id = c.user_id ";
     $query .= "ORDER BY a.last_name, a.first_name ";
     $result = $this->db->query($query);
     return $result->result_array();
   }

   function update_user_account($data) {
     $response = false;
     $this->db->select('user_id');
     $this->db->from('user_account');
     $this->db->where('user_id', $data['user_id']);
     $rowCount = $this->db->count_all_results();
     if($rowCount < 1) {
       $response = $this->db->insert('user_account',$data);
     } else {
       $this->db->where('user_id',$data['user_id']);
       $response = $this->db->update('user_account',$data);
     }
     return $rowCount;
   }

   /*
   * delete a user
   */
  function remove_user($user_id)
  {
     $this->db->where('user_id',$user_id);
     $this->db->delete('user_account');

  }

  /*
   * Get user groups (status_type = 'all', 'active', 'inactive')
   */
  function get_user_groups($status_type)
  {
    if($status_type == 'active') {
      $status = 1;
    } else if($status_type == 'inactive') {
      $status = 0;
    } else $status = NULL;
      $this->db->select('*');
      $this->db->from('user_groups');
      if($status != NULL) $this->db->where('active', $status);
      $query = $this->db->get();
      return $query->result_array();
  }

  /*
   * Get user groups (status_type = 'all', 'active', 'inactive')
   */
  function get_user_group($user_group_id)
  {
      $this->db->select('*');
      $this->db->from('user_groups');
      $this->db->where('user_group_id', $user_group_id);
      $query = $this->db->get();
      return $query->row_array();
  }

  function update_user_groups($data) {
    $response = false;
    $this->db->select('user_group_id');
    $this->db->from('user_groups');
    $this->db->where('user_group_id', $data['user_group_id']);
    $rowCount = $this->db->count_all_results();
    if($rowCount < 1) {
      $response = $this->db->insert('user_groups',$data);
    } else {
      $this->db->where('user_group_id',$data['user_group_id']);
      $response = $this->db->update('user_groups',$data);
    }
    return $response;
  }

  /*
   * Get user conatct info
   */
  function get_user_contact($user_id)
  {
      $this->db->select('*');
      $this->db->from('user_contact');
      $this->db->where('user_id', $user_id);
      $query = $this->db->get();
      return $query->row_array();
  }

  /*
   * Get address types
   */
  function get_address_types()
  {
      $this->db->select('*');
      $this->db->from('ref_address_types');
      $query = $this->db->get();
      return $query->result_array();
  }

  /*
   * Get address types
   */
  function get_states()
  {
      $this->db->select('*');
      $this->db->from('ref_states');
      $query = $this->db->get();
      return $query->result_array();
  }

  /*
   * Get time zones
   */
  function get_time_zones()
  {
      $this->db->select('*');
      $this->db->from('ref_time_zones');
      $query = $this->db->get();
      return $query->result_array();
  }

  /*
   * Get one time zone
   */
  function get_time_zone($time_zone_id)
  {
      $this->db->select('*');
      $this->db->from('ref_time_zones');
      $this->db->where('time_zone_id', $time_zone_id);
      $query = $this->db->get();
      return $query->row_array();
  }

  function update_time_zone($data) {
    $response = false;
    $this->db->select('time_zone_id');
    $this->db->from('ref_time_zones');
    $this->db->where('time_zone_id', $data['time_zone_id']);
    $rowCount = $this->db->count_all_results();
    if($rowCount < 1) {
      $response = $this->db->insert('ref_time_zones',$data);
    } else {
      $this->db->where('time_zone_id',$data['time_zone_id']);
      $response = $this->db->update('ref_time_zones',$data);
    }
    return $response;
  }


  function update_user_contact($data) {
    $response = false;
    $this->db->select('user_id');
    $this->db->from('user_contact');
    $this->db->where('user_id', $data['user_id']);
    $rowCount = $this->db->count_all_results();
    if($rowCount < 1) {
      $response = $this->db->insert('user_contact',$data);
    } else {
      $this->db->where('user_id',$data['user_id']);
      $response = $this->db->update('user_contact',$data);
    }
    return $response;
  }


} // end class
