  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3>Table Maintenance&nbsp;-&nbsp;<small> Address Types </small>&nbsp;<i class="fa fa-list"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Add/Edit Address Types </span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body form">
                <form class="form" role="form" action='<?php echo base_url('Admin/adm_address_types_post');?>' method="POST" onsubmit='return finalCheck();'>
                  <div class="form-body">
                      <div class="form-group">
                        <label class="control-label">Address Type Name
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <input type="hidden" name="address_type_id" value="<?php echo $address_type_id;?>">
                          <input type="text" class="form-control required" name="address_type_name" id="address_type_name" size="30" maxlength="50" value='<?php echo $adrData['address_type_name'];?>' />
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Status
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <select class="form-control required" name="active" id="active" />
                          <option value=''>-- select --</option>
                          <option value='1' <?php if($adrData['active']=='1') echo ' selected';?>>Active</option>
                          <option value='0' <?php if($adrData['active']=='0') echo ' selected';?>>Inactive</option>
                          </select>
                        </div>
                      </div>
                      <!-- -->
                    </div>
                    <!-- -->
                    <div class="form-actions">
                      <?php if($address_type_id == 0 || $address_type_id == '') : ?>
                          <input type="submit" class="btn btn-info button-submit" value=" Add New ">&nbsp;&nbsp;&nbsp;
                      <?php else : ?>
                          <input type="submit" class="btn btn-success button-submit" value="Submit">&nbsp;&nbsp;&nbsp;
                      <?php endif; ?>
                        <a href='<?php echo base_url('Home/main');?>' class='btn btn-md btn-default'> Cancel </a>
                    </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Current Address Types </span><br />
                  <span class="helper" style="padding-left:18px;"><small>Click an address type name to edit</small></span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed">
                    <thead><tr><th width='10%'>ID</th><th>Name</th><th>Active</th></tr></thead>
                    <tbody>
                      <?php
                      if(count($adrList) > 0) {
                        foreach($adrList as $aList) {
                          echo "<tr>";
                          echo "<td>".$aList['address_type_id']."</td>";
                          echo "<td><a href='".base_url('Admin/adm_address_types/').$aList['address_type_id']."'>".$aList['address_type_name']."</a></td>";
                          echo "<td>";
                          if($aList['active'] == 0) {echo 'No';}
                          else if($aList['active'] == 1) {echo "Yes";}
                          else echo 'Undefined';
                          echo "</td>";
                          echo "</tr>";
                        }
                      } else {
                        echo "<tr><td colspan='3'>There are no address types defined...</td></tr>";
                      }
                      ?>
                    </tbody>
                  </table>
                    <!-- -->
                    <div class="row"><div class="col-md-12">&nbsp;</div></div>
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!--
    ** show last operation status
    -->
    <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
    <?php
    if($this->session->flashdata('success')) {
      echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
    } else if($this->session->flashdata('success')) {
      echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
    } ?>

    <!-- END CONTENT -->
    <script>
      function finalCheck() {
    	var err = false;
    	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
    	if($('#address_type_name').val() == "") {err=true;errMsg=errMsg+"- Please enter an address type name.\n";}
      if($('#status').val() == "") {err=true;errMsg=errMsg+"- Please indicate the address type status.\n";}
      if(err == true) {alert(errMsg); return false;}
      return true;
    }
    </script>
