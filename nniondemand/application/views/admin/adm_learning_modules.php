  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3> Learning Modules <i class="fa fa-book"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Add Learning Module </span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body form">
                <form class="form" role="form" action='<?php echo base_url('Admin/learning_modules_post');?>' method="POST" onsubmit='return finalCheck();'>
                  <div class="form-body">
                      <div class="form-group">
                        <label class="control-label">Module Name
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <input type="hidden" name="module_id" value="<?php echo $module_id;?>">
                          <input type="text" class="form-control required" name="module_name" id="module_name" size="50" maxlength="100" value='<?php echo $moduleData['module_name'];?>' />
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Module Description
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <textarea class="form-control required" name="module_description" id="module_description" rows="3" cols="100"><?php echo $moduleData['module_description'];?></textarea>
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Number of Lessons
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <input type="text" class="form-control required" name="total_lessons" id="total_lessons" size="5" maxlength="10" value='<?php echo $moduleData['total_lessons'];?>' />
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Prerequisite Module
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <select class="form-control required" name="prerequisite" id="active" />
                            <option value='0'> None </option>
                            <?php foreach($moduleList as $mList) {
                              echo "<option value='".$mList['module_id']."' ";
                              if($moduleData['prerequisite'] == $mList['module_id']) echo " selected";
                              echo ">".$mList['module_name']."</option>";
                            }
                              ?>
                          </select>
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Status
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <select class="form-control required" name="active" id="active" />
                            <option value=''>-- select --</option>
                            <option value='1' <?php if($moduleData['active']=='1') echo ' selected';?>>Active</option>
                            <option value='0' <?php if($moduleData['active']=='0') echo ' selected';?>>Inactive</option>
                          </select>
                        </div>
                      </div>
                      <!-- -->
                    </div>
                    <!-- -->
                    <div class="form-actions">
                      <?php if($module_id == 0 || $module_id == '') : ?>
                          <input type="submit" class="btn btn-info button-submit" value=" Add New ">&nbsp;&nbsp;&nbsp;
                      <?php else : ?>
                          <input type="submit" class="btn btn-success button-submit" value="Submit">&nbsp;&nbsp;&nbsp;
                      <?php endif; ?>
                        <a href='<?php echo base_url('Home/main');?>' class='btn btn-md btn-default'> Cancel </a>
                    </div>
                </form>
              </div>
            </div>
          </div>
          </div>
      <div class="row">
        <div class="col-md-9">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Current Learning Modules </span><br />
                  <span class="helper" style="padding-left:18px;"><small>Click a module name to edit</small></span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body">
                <table class="table table-bordered table-striped table-hover table-condensed">
                    <thead><tr><th>Name</th><th width="40%">Description</th><th>Lessons</th><th>Active</th></tr></thead>
                    <tbody>
                      <?php
                      if(count($moduleList) > 0) {
                        foreach($moduleList as $mList) {
                          echo "<tr>";
                          echo "<td><a href='".base_url('Admin/learning_modules/').$mList['module_id']."'>".$mList['module_name']."</a></td>";
                          echo "<td>".$mList['module_description']."</td>";
                          echo "<td>".$mList['total_lessons']."</td>";
                          echo "<td>";
                          if($mList['active'] == 0) {echo 'No';}
                          else if($mList['active'] == 1) {echo "Yes";}
                          else echo 'Undefined';
                          echo "</td>";
                          echo "</tr>";
                        }
                      } else {
                        echo "<tr><td colspan='3'>There are no learning modules defined...</td></tr>";
                      }
                      ?>
                    </tbody>
                  </table>
                    <!-- -->
                  <div class="row"><div class="col-md-12">&nbsp;</div></div>
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!--
    ** show last operation status
    -->
    <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
    <?php
    if($this->session->flashdata('success')) {
      echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
    } else if($this->session->flashdata('success')) {
      echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
    } ?>

    <!-- END CONTENT -->
    <script>
      function finalCheck() {
    	var err = false;
    	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
    	if($('#time_zone_code').val() == "") {err=true;errMsg=errMsg+"- Please enter a time zone code.\n";}
    	if($('#time_zone_name').val() == "") {err=true;errMsg=errMsg+"- Please enter the time zone full name.\n";}
      if($('#status').val() == "") {err=true;errMsg=errMsg+"- Please indicate the time zone status.\n";}
      if(err == true) {alert(errMsg); return false;}
      return true;
    }
    </script>
