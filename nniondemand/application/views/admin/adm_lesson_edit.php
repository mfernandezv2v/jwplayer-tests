  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
              <h3> Lesson Maintenance <i class="fa fa-book"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <?php if($lesson_id == 0 || $lesson_id == '') : ?>
                    <span class="caption-subject font-white"> Add New Lesson </span>
                  <?php else : ?>
                    <span class="caption-subject font-white"> Edit Lesson </span>
                  <?php endif; ?>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body form">
                <form class="form" role="form" class="form-horizontal" action='<?php echo base_url('Admin/lesson_edit_post');?>' method="POST" onsubmit='return finalCheck();'>
                  <input type="hidden" name="lesson_id" value="<?php echo $lesson_id;?>">
                  <div class="form-body">
                      <div class="form-group">
                        <label class="control-label">Module Name
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <select name="module_id" id="module_id" class="form-control required">
                          <option value="">-- select --</option>
                          <?php foreach($moduleList as $mList) {
                            echo "<option value='".$mList['module_id']."' ";
                            if($mList['module_id'] == $lessonData['module_id']) echo " selected";
                            echo ">".$mList['module_name']."</option>";
                          } ?>
                        </select>
                        </div>
                      </div>
                      <!--- -->
                      <div class="form-group">
                        <label class="control-label">Lesson Name
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <input type="text" class="form-control required" name="lesson_name" id="lesson_name" size="50" maxlength="100" value='<?php echo $lessonData['lesson_name'];?>' />
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Lesson Description
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <textarea class="form-control required" name="lesson_description" id="lesson_description" rows="3" cols="100"><?php echo $lessonData['lesson_description'];?></textarea>
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Lesson Sequence Number<br /><small>(for multiple lesson modules, this value determines the ordered sequence of the lesson)</small>
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <input type="text" class="form-control required" name="lesson_order" id="lesson_order" size="5" maxlength="10" value='<?php echo $lessonData['lesson_order'];?>' />
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Lesson Type
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <select class="form-control required" name="lesson_type" id="lesson_type" />
                            <option value=''>-- select --</option>
                            <option value='1' <?php if($lessonData['lesson_type']=='1') echo ' selected';?>>Video</option>
                            <option value='2' <?php if($lessonData['lesson_type']=='2') echo ' selected';?>>Audio</option>
                            <option value='3' <?php if($lessonData['lesson_type']=='3') echo ' selected';?>>PDF/Document</option>
                          </select>
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Lesson Length Minutes:Seconds<br /><small>(for video/audio lesson, total time length; for PDF/Document enter zero)</small>
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <div class="col-md-3"><label>Minutes</label><input type="text" class="form-control required" name="lesson_length_min" id="lesson_length_min" size="5" maxlength="10" value='<?php echo $lessonData['lesson_length_min'];?>' /></div>
                          <div class="col-md-3"><label>Seconds</label><input type="text" class="form-control required" name="lesson_length_sec" id="lesson_length_sec" size="5" maxlength="10" value='<?php echo $lessonData['lesson_length_sec'];?>' /></div>
                          <div class="col-md-3">&nbsp;</div>
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Lesson File Name<br /><small>(Full name of lesson file)</small>
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <input type="text" class="form-control required" name="lesson_file" id="lesson_file" size="50" maxlength="100" value='<?php echo $lessonData['lesson_file'];?>' />
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Lesson Image Name<br /><small>(icon image of lesson)</small>
                        </label>
                        <div class="input-group">
                          <input type="text" class="form-control" name="lesson_image" id="lesson_image" size="50" maxlength="100" value='<?php echo $lessonData['lesson_image'];?>' />
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Status
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <select class="form-control required" name="active" id="active" />
                            <option value=''>-- select --</option>
                            <option value='1' <?php if($lessonData['active']=='1') echo ' selected';?>>Active</option>
                            <option value='0' <?php if($lessonData['active']=='0') echo ' selected';?>>Inactive</option>
                          </select>
                        </div>
                      </div>
                      <!-- -->
                    </div>
                    <!-- -->
                    <div class="form-actions">
                      <?php if($lessonData['module_id'] == 0 || $lessonData['module_id'] == '') : ?>
                          <input type="submit" class="btn btn-info button-submit" value=" Add New ">&nbsp;&nbsp;&nbsp;
                      <?php else : ?>
                          <input type="submit" class="btn btn-success button-submit" value="Submit">&nbsp;&nbsp;&nbsp;
                      <?php endif; ?>
                        <a href='<?php echo base_url('Admin/lessons');?>' class='btn btn-md btn-default'> Cancel </a>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!--
    ** show last operation status
    -->
    <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
    <?php
    if($this->session->flashdata('success')) {
      echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
    } else if($this->session->flashdata('success')) {
      echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
    } ?>

    <!-- END CONTENT -->
    <script>
      function finalCheck() {
    	var err = false;
    	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
    	if($('#module_id').val() == "") {err=true;errMsg=errMsg+"- Please seelct a learning module.\n";}
    	if($('#lesson_name').val() == "") {err=true;errMsg=errMsg+"- Please enter a name for the lesson.\n";}
      if($('#lesson_description').val() == "") {err=true;errMsg=errMsg+"- Please enter a lesson description.\n";}
      if($('#lesson_order').val() == "") {err=true;errMsg=errMsg+"- Please enter a lesson sequence number.\n";}
      if(!$.isNumeric('#lesson_order').val()) {err=true;errMsg=errMsg+"- Lesson sequence value must be numeric.\n";}
      if($('#lesson_type').val() == "") {err=true;errMsg=errMsg+"- Please assign a lesson type.\n";}
      if(!$.isNumeric('#lesson_length_min').val()) {err=true;errMsg=errMsg+"- Lesson length minutes value must be numeric.\n";}
      if(!$.isNumeric('#lesson_length_sec').val()) {err=true;errMsg=errMsg+"- Lesson length seconds value must be numeric.\n";}
      if($('#lesson_file').val() == "") {err=true;errMsg=errMsg+"- Please enter the full name of the lesson file.\n";}
      if($('#status').val() == "") {err=true;errMsg=errMsg+"- Please indicate wheter this record is active or inactive.\n";}
      if(err == true) {alert(errMsg); return false;}
      return true;
    }
    </script>
