  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3> Lessons <i class="fa fa-book"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Module and Lessons </span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body form">
                <form class="form" role="form" action="" method="">
                  <div class="form-body">
                      <div class="form-group">
                        <label class="control-label">Select a Learning Module
                        </label>
                        <div class="input-group">
                          <select name="module_id" id="module_id" class="form-control" onchange="showLessons(this);">
                            <option value="">-- select -- </option>
                            <?php foreach($moduleList as $mList) : ?>
                              <option value='<?php echo $mList['module_id'];?>'><?php echo $mList['module_name'];?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>
                </form>
              </div>
            </div>
          </div>
          </div>
      <div class="row">
        <div class="col-md-12">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Lessons </span><br />
                  <span class="helper" style="padding-left:18px;"><small>Click a lesson name to edit</small></span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body">
                <div class="form-body">
                    <div class="form-group">
                      <a href="<?php echo base_url('Admin/lesson_edit/0');?>" class="btn btn-md btn-info">Add New Lesson</a>
                    </div>
                </div>
                <div id="lesson_section" >
                  <!-- selected lesson list goes here -->

                  </div>
                  <!-- -->
                <div class="row"><div class="col-md-12">&nbsp;</div></div>
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
  <script>
    function showLessons() {
      var mod_id = $('#module_id').val();
      $.ajax({
          type: "GET",//or POST
          url: '<?php echo site_url("Admin/show_lesson_list");?>',
          data: {module_id: mod_id},
          success: function (data) {
              $('#lesson_section').html(data);
              console.log(data);//it will show the error log if any [optional]
          }
      });
    }
  </script>
