  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3>Table Maintenance&nbsp;-&nbsp;<small> Time Zones </small>&nbsp;<i class="fa fa-clock-o"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Add Time Zone </span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body form">
                <form class="form" role="form" action='<?php echo base_url('Admin/adm_time_zones_post');?>' method="POST" onsubmit='return finalCheck();'>
                  <div class="form-body">
                      <div class="form-group">
                        <label class="control-label">Time Zone Code
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <input type="hidden" name="time_zone_id" value="<?php echo $time_zone_id;?>">
                          <input type="text" class="form-control required" name="time_zone_code" id="time_zone_code" size="10" maxlength="5" value='<?php echo $tzData['time_zone_code'];?>' />
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Time Zone Name
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <input type="text" class="form-control required" name="time_zone_name" id="time_zone_name" size="50" maxlength="50" value='<?php echo $tzData['time_zone_name'];?>' />
                        </div>
                      </div>
                      <!-- -->
                      <div class="form-group">
                        <label class="control-label">Status
                          <span class="required"> * </span>
                        </label>
                        <div class="input-group">
                          <select class="form-control required" name="active" id="active" />
                          <option value=''>-- select --</option>
                          <option value='1' <?php if($tzData['active']=='1') echo ' selected';?>>Active</option>
                          <option value='0' <?php if($tzData['active']=='0') echo ' selected';?>>Inactive</option>
                          </select>
                        </div>
                      </div>
                      <!-- -->
                    </div>
                    <!-- -->
                    <div class="form-actions">
                      <?php if($time_zone_id == 0 || $time_zone_id == '') : ?>
                          <input type="submit" class="btn btn-info button-submit" value=" Add New ">&nbsp;&nbsp;&nbsp;
                      <?php else : ?>
                          <input type="submit" class="btn btn-success button-submit" value="Submit">&nbsp;&nbsp;&nbsp;
                      <?php endif; ?>
                        <a href='<?php echo base_url('Home/main');?>' class='btn btn-md btn-default'> Cancel </a>
                    </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Current Time Zones </span><br />
                  <span class="helper" style="padding-left:18px;"><small>Click a time zone name to edit</small></span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed">
                    <thead><tr><th width='10%'>Code</th><th>Name</th><th>Active</th></tr></thead>
                    <tbody>
                      <?php
                      if(count($tzList) > 0) {
                        foreach($tzList as $tList) {
                          echo "<tr>";
                          echo "<td>".$tList['time_zone_code']."</td>";
                          echo "<td><a href='".base_url('Admin/adm_time_zones/').$tList['time_zone_id']."'>".$tList['time_zone_name']."</a></td>";
                          echo "<td>";
                          if($tList['active'] == 0) {echo 'No';}
                          else if($tList['active'] == 1) {echo "Yes";}
                          else echo 'Undefined';
                          echo "</td>";
                          echo "</tr>";
                        }
                      } else {
                        echo "<tr><td colspan='3'>There are no time zones defined...</td></tr>";
                      }
                      ?>
                    </tbody>
                  </table>
                    <!-- -->
                    <div class="row"><div class="col-md-12">&nbsp;</div></div>
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!--
    ** show last operation status
    -->
    <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
    <?php
    if($this->session->flashdata('success')) {
      echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
    } else if($this->session->flashdata('success')) {
      echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
    } ?>

    <!-- END CONTENT -->
    <script>
      function finalCheck() {
    	var err = false;
    	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
    	if($('#time_zone_code').val() == "") {err=true;errMsg=errMsg+"- Please enter a time zone code.\n";}
    	if($('#time_zone_name').val() == "") {err=true;errMsg=errMsg+"- Please enter the time zone full name.\n";}
      if($('#status').val() == "") {err=true;errMsg=errMsg+"- Please indicate the time zone status.\n";}
      if(err == true) {alert(errMsg); return false;}
      return true;
    }
    </script>
