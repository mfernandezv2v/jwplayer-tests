<!-- -->
<div class="page-content">
  <div class="container-fluid-md">
      <div class="row">
        <div class="col-md-10">
          <?php if($this->session->flashdata('reminder_sent')) {
            echo "<span class='alert alert-success'>".$this->session->flashdata('reminder_sent')."</span>";
          } ?>
          <div class="panel panel-custom-blue" style="min-height:100px;">
            <div class="panel-heading"><h3 class="panel-title"> Send Event Reminders </h3></div>
            <div class="panel-body">
              <form name="reminder" action="<?php echo base_url('Participant/send_reminder');?>" method="POST" onsubmit="return finalCheck();">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label"> Select Event
                      <select name="eventID" id="eventID" class="form-control" onchange="getRegistered()">
                      <option value="">-- Select --</option>
                      <?php foreach( $eventList as $evt) : ?>
                        <option value="<?php echo $evt['eventID'];?>"> <?php echo $evt['eventTitle'];?> - <?php echo date('m/d/Y', strtotime($evt['eventDate']))." ".$evt['eventStartTime'].$evt['eventMeridian']." ".$evt['eventTimeZone'];?> </option>
                      <?php endforeach; ?>
                      </select>
                    </label>
                  </div>
                  <!-- -->
                  <div class="form-group">
                    <label class="control-label"> Select Email Template
                      <select name="em_template_id" id="em_template_id" class="form-control" onchange="getTemplate()">
                      <option value="">-- Select --</option>
                      <?php foreach( $tempList as $tmp) : ?>
                        <option value="<?php echo $tmp->em_template_id;?>"> <?php echo $tmp->em_name;?> </option>
                      <?php endforeach; ?>
                      </select>
                    </label>
                  </div>
                  <!-- -->
                  <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-md btn-info" value=" Send Reminders">
                  </div>
                  </form>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" id="registeredList">
                  <!-- list of registered participants goes here -->
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" id="templateList">
                  <!-- show selected template here -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script>
function getRegistered() {
	var evtID = $('#eventID').val();
  $.ajax({
   type: "GET",//or POST
   url: '<?php echo base_url("Participant/show_registered");?>',
   data: {eventID: evtID},
   success: function(data){
     $('#registeredList').html(data);
     console.log(data);//it will show the error log if any [optional]
   }
 });
}

function getTemplate() {
	var templateID = $('#em_template_id').val();
  $.ajax({
   type: "GET",//or POST
   url: '<?php echo base_url("Admin/show_em_template");?>',
   data: {em_template_id: templateID},
   success: function(data){
     $('#templateList').html(data);
     console.log(data);//it will show the error log if any [optional]
   }
 });
}


function finalCheck() {
  var err = false;
  var errMsg = 'NOTE: Some required information has not been provided:\n\n';
  if($('#eventID').val() == "") {err=true;errMsg=errMsg+"- Please select an event ID.\n";}
  if($('#em_template_id').val() == "") {err=true;errMsg=errMsg+"- Please select an email template.\n";}
  if(err == true) {alert(errMsg); return false;}
  return true;
}

</script>
