<div class="page-content-wrapper">
<div class="page-content">
  <div class="container-fluid-md">
      <div class="row">
        <div class="col-md-12">
          <div class="portlet box grey-cascade">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-edit font-white"></i>
                <span class="caption-subject font-white"> Edit System Email Templates </span>
              </div>
            </div>
            <div class="portlet-body">
              <a href="<?php echo base_url('Admin/smtp_template_edit');?>/0" class="btn btn-md btn-primary"><i class="fa fa-add"></i> Add New </a>
              <div class="table table-responsive">
                <table class="table table-striped table-hover">
                  <thead><tr><th width="15%"></th><th> Name </th><th> Subject </th></tr></thead>
                  <tbody>
                  <?php
                  if(count($emList) > 0) : ?>
                    <?php foreach($emList as $el) : ?>
                      <tr>
                        <td>
                          <a href="<?php echo base_url('Admin/smtp_template_remove');?>/<?php echo $el->em_template_id;?>" class="btn btn-xs btn-warning" onclick="return verifyRem();"><i class="fa fa-trash"></i> Del </a>
                          <a href="<?php echo base_url('Admin/smtp_template_edit');?>/<?php echo $el->em_template_id;?>" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i> Edit </a>
                        </td>
                        <td><?php echo $el->em_name;?></td>
                        <td><?php echo $el->em_subject;?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else : ?>
                    <tr><td colspan='3'>No system email templates are available...</td></tr>
                  <?php endif; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script>
function verifyRem() {
  if (confirm('The selected record will be permanently deleted!\n\nDo you wish to continue?')) {
    return true; // do things if OK
}
  return false;
}
</script>
