<div class="page-content-wrapper">
<div class="page-content">
  <div class="container-fluid-md">
    <div class="row">
      <div class="col-md-10">
        <div class="portlet box grey-cascade">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-edit font-white"></i>
              <span class="caption-subject font-white"> Edit System Email Template </span>
            </div>
          </div>
          <div class="portlet-body">
            <form class="form" action='<?php echo base_url('Admin/smtp_email_template_post'); ?>' method="POST" onsubmit='return finalCheck();'>
              <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label">Email Template Name
                        <span class="font-red"> * </span>
                        <input type="hidden" name="em_template_id" id="em_template_id" value='<?php echo $emData[0]->em_template_id;?>'/>
                        <input type="text" class="form-control required" name="em_name" id="em_name" value='<?php echo $emData[0]->em_name;?>' size="50" maxlength="255"/>
                      </label>
                    </div>

                    <div class="form-group">
                      <label class="control-label">Email Subject Line
                        <span class="font-red"> * </span>
                        <input type="text" class="form-control required" name="em_subject" id="em_subject" value='<?php echo $emData[0]->em_subject;?>' size="50" maxlength="255"/>
                      </label>
                    </div>

                    <script src="//cdn.ckeditor.com/4.5.6/full/ckeditor.js"></script>
                    <div class="form-group">
                      <label class="control-label">Email Text
                        <span class="font-red"> * </span>
                      		<textarea class="form-control" name="em_message" id="em_message" cols="200" rows="20"><?php echo $emData[0]->em_message;?></textarea>
                        	<script type="text/javascript">	CKEDITOR.replace('em_message');  </script>
                     </label>
                     <span class="help-block"><small> (Use merge tags to personalize the email message) </small></span>
                    </div>

                    <div class="form-group">
                      <div class="input-group">
                        <input type="submit" class="btn btn-md btn-primary" value="Save Changes">&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo base_url('Admin/sys_email_template'); ?>" class="btn brn-sm btn-default"> Cancel </a>
                      </div>
                    </div>

                    <div class="list-group col-md-6">
                      <a href="javascript:;" class="list-group-item"><span class="help-block">{FULL-NAME} <small>Recipient full name</small> </a>
                      <a href="javascript:;" class="list-group-item"><span class="help-block">{FIRST-NAME} <small>Recipient first name</small> </a>
                      <a href="javascript:;" class="list-group-item"><span class="help-block">{LAST-NAME} <small>Recipient last name</small></a>
                      <a href="javascript:;" class="list-group-item"><span class="help-block">{SUPPORT-TEAM} <small>Support team name</small></a>
                      <a href="javascript:;" class="list-group-item"><span class="help-block">{SUPPORT-EMAIL} <small>Support team email address</small></a>
                    </div>
                    <div class="list-group col-md-6">
                      <a href="javascript:;" class="list-group-item"><span class="help-block">{EMAIL} <small>Recipient email address</small> </a>
                      <a href="javascript:;" class="list-group-item"><span class="help-block">{HASH-EMAIL} <small>Recipient Email (hashed)</small> </a>
                  </div>
                </div>
              </div>
            </form>
            </div>
          </div>
        </div>
      </div>
      <!-- END CONTENT -->
    </div>
  </div>
<script>
  function finalCheck() {
	var err = false;
	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
	if($('#em_name').val() == "" && $('#emailAccount').val() == "") {err=true;errMsg=errMsg+"- Email Template Name is Required.\n";}
  if($('#em_subject').val() == "") {err=true;errMsg=errMsg+"- Email Subject Line is Required.\n";}
	//if($('#em_message').val() == "") {err=true;errMsg=errMsg+"- Email Mesage Text is Required.\n";}
  if(err == true) {alert(errMsg); return false;}
  return true;
}
</script>
