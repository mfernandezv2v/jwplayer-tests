  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="container-fluid-md">
        <div class="row">
          <div class="col-md-8">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> SMTP Server Settings </span>
                </div>
              </div>
              <div class="portlet-body">
              <?php $smtpPassword = $this->User_model->encrypt_decrypt('decrypt', $smtpData['sys_smtp_password']); ?>
                <form class="form" action='<?php echo base_url('Admin/sys_smtp_post');?>' method="POST" onsubmit='return finalCheck();'>
                  <div class="row"><div class="col-md-12">&nbsp;</div></div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="control-label">SMTP Server
                          <span class="font-red"> * </span>
                          <input type="hidden" name="sys_smtp_id" id="sys_smtp_id" value='<?php echo $smtpData['sys_smtp_id'];?>'/>
                          <input type="text" class="form-control required" name="sys_smtp_server" id="sys_smtp_server" value='<?php echo $smtpData['sys_smtp_server'];?>' size="50" maxlength="50"/>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Account Name
                          <span class="font-red"> * </span>
                          <input type="text" class="form-control required" name="sys_smtp_account_name" id="sys_smtp_account_name" value='<?php echo $smtpData['sys_smtp_account_name'];?>' size="50" maxlength="100"/>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Account Email Address
                          <span class="font-red"> * </span>
                          <input type="text" class="form-control required" name="sys_smtp_account" id="sys_smtp_account" value='<?php echo $smtpData['sys_smtp_account'];?>' size="50" maxlength="50"/>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Password
                          <span class="font-red"> * </span>
                          <input type="password" class="form-control required" name="sys_smtp_password" id="sys_smtp_password" value='<?php echo $smtpPassword;?>' size="30" maxlength="50"/>
                          <span class="help-block"><small> (min length 8 chars, numbers & letters, 1 capital) </small></span>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Verify Password
                          <span class="font-red"> * </span>
                          <input type="password" class="form-control required" name="sys_smtp_password2" id="sys_smtp_password2" value='<?php echo $smtpPassword;?>' size="30"maxlength="50"/>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label">SMTP Port
                          <span class="font-red"> * </span>
                          <input type="text" class="form-control required" name="sys_smtp_port" id="sys_smtp_port" value='<?php echo $smtpData['sys_smtp_port'];?>' size="5" maxlength="5"/>
                        </label>
                      </div>

                      <div class="form-group">
                        <label class="control-label">SMTP Security
                          <span class="font-red"> * </span>
                          <select class="form-control required" name="sys_smtp_secure" id="sys_smtp_secure">
                            <option value=''>-- select --</option>
                            <option value='none' <?php if($smtpData['sys_smtp_secure'] == 'none') echo ' selected ';?>>none</option>
                            <option value='tls' <?php if($smtpData['sys_smtp_secure'] == 'TLS') echo ' selected ';?>>TLS</option>
                            <option value='ssl' <?php if($smtpData['sys_smtp_secure'] == 'SSL') echo ' selected ';?>>SSL</option>
                          </select>
                        </label>
                      </div>

                    <div class="form-group">
                        <label class="control-label">System Support Name<br /><small>System support team name</small>
                          <span class="font-red"> * </span>
                          <input type="text" class="form-control required" name="sys_support_name" id="sys_support_name" value='<?php echo $smtpData['sys_support_name'];?>' size="50" maxlength="255" />
                          <span class="help-block"><small> (255 character max.) </small></span>
                        </label>
                      </div>

                    <div class="form-group">
                      <label class="control-label">System Support Account(s)<br /><small>System support team email address(es)</small>
                        <span class="font-red"> * </span>
                        <input type="text" class="form-control required" name="sys_support_account" id="sys_support_account" value='<?php echo $smtpData['sys_support_account'];?>' size="50" maxlength="255" />
                        <span class="help-block"><small> (comma separate multiple email addresses) </small></span>
                      </label>
                    </div>

                  <div class="row"><div class="col-md-12">&nbsp;</div></div>
                  <div class="row">
                      <div class="col-md-offset-2 col-md-10">
                          <input type="submit" class="btn btn-md btn-primary button-submit" value="Save Changes">&nbsp;&nbsp;&nbsp;
                          <a href='<?php echo base_url('Home/main') ?>' class='btn btn-md btn-default'> Cancel </a>
                      </div>
                  </div>
                  <div class="row"><div class="col-md-12">&nbsp;</div></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--
** show last operation status
-->
<script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
<?php
if($this->session->flashdata('success')) {
  echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
} else if($this->session->flashdata('success')) {
  echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
} ?>

<script>
  function finalCheck() {
	var err = false;
	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
	if($('#sys_smtp_server').val() == "") {err=true;errMsg=errMsg+"- SMTP server name is required.\n";}
  if($('#sys_smtp_account_name').val() == "") {err=true;errMsg=errMsg+"- SMTP account name is required.\n";}
	if($('#sys_smtp_account').val() == "") {err=true;errMsg=errMsg+"- SMTP account email address is required.\n";}
	if($('#sys_smtp_password').val() == "") {err=true;errMsg=errMsg+"- SMTP password is required.\n";}
  if($('#sys_smtp_port').val() == "") {err=true;errMsg=errMsg+"- SMTP port number is required.\n";}
  if($('#sys_smtp_secure').val() == "") {err=true;errMsg=errMsg+"- Select SMTP security type.\n";}
  var pass1 = $('#sys_smtp_password').val();
  var pass2 = $('#sys_smtp_password2').val();
  if(pass1 != pass2) {err=true;errMsg=errMsg+"- SMTP passwords don't match!.\n";}
  if($('#sys_smtp_password').val().length < 8) {err=true;errMsg=errMsg+'- Password must be at least 8 characters long.\n';}
  if((!$('#sys_smtp_password').val().match(/[A-Z]/)) || !$('#sys_smtp_password').val().match(/([0-9])/)) { err=true;errMsg=errMsg+'- Password must contain an uppercase letter or a number.\n';}
  if(err == true) {alert(errMsg); return false;}
  return true;
}
</script>
