
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3>User Accounts &nbsp;-&nbsp;<small> Add/Edit </small>&nbsp;<i class="fa fa-edit"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Add/Edit User Account </span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body form">
                <form class="form" role="form" action='<?php echo base_url('Admin/user_account_post');?>' method="POST" onsubmit='return finalCheck();'>
                  <div class="form-body">
                    <div class="form-group">
                        <label class="control-label">Email Address
                            <span class="required"> * </span>
                        </label>
                            <input type="hidden" name="user_id" id="user_id" value='<?php echo $user_id;?>'/>
                            <input type="email" class="form-control required" name="email_address" id="email_address" value='<?php echo $userData['email_address'];?>'/>
                            <span class="help-block"> Email address will be the user's login ID</span>
                    </div>
                    <!-- -->
                    <div class="form-group">
                        <label class="control-label">Password
                            <span class="required"> * </span>
                        </label>
                        <input type="password" class="form-control required" name="password" id="password" size="25" maxlength="20" value='<?php echo $user_password;?>'/>
                        <span class="help-block"> Account password. </span>
                    </div>
                    <!-- -->
                    <div class="form-group">
                        <label class="control-label">Confirm Password
                            <span class="required"> * </span>
                        </label>
                        <input type="password" class="form-control required" name="rpassword" id="rpassword" size="25" maxlength="20" value='<?php echo $user_password;?>'/>
                        <span class="help-block"> Confirm your password </span>
                    </div>
                    <!-- -->
                    <div class="form-group">
                        <label class="control-label">Account Type
                            <span class="required"> * </span>
                        </label>
                        <select class="form-control required" name="user_type" id="user_type">
                          <option value="">-- select --</option>
                          <option value="1" <?php if($userData['user_type'] == '1') echo ' selected';?>>System Administrator</option>
                          <option value="2" <?php if($userData['user_type'] == '2') echo ' selected';?>>Client Administrator</option>
                          <option value="3" <?php if($userData['user_type'] == '3') echo ' selected';?>>User</option>
                      </select>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label">Display Name
                      </label>
                      <input type="text" class="form-control" name="display_name" id="display_name" value="<?php echo $userData['display_name'];?>" maxlength="50"/>
                      <span class="help-block"> (Optional) </span>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label">Title</label>
                      <select name="title" id="title" class="form-control" />
                      <option value=''>none</option>
                      <option value='Dr.'>Dr.</option>
                      <option value='Mr.'>Mr.</option>
                      <option value='Mrs.'>Mrs.</option>
                      <option value='Ms.'>Ms.</option>
                      <option value='Prof.'>Prof.</option>
                      </select>
                      <span class="help-block"> Select a title </span>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label">First Name
                          <span class="required"> * </span>
                      </label>
                      <input type="text" class="form-control required" name="first_name" id="first_name" value="<?php echo $userData['first_name'];?>" maxlength="50"/>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label">Middle Name
                      </label>
                      <input type="text" class="form-control" name="mid_name" id="mid_name" valu="<?php echo $userData['mid_name'];?>" maxlength="50"/>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label">Last Name
                          <span class="required"> * </span>
                      </label>
                      <input type="text" class="form-control required" name="last_name" id="last_name" value="<?php echo $userData['last_name'];?>" maxlength="50"/>
                    </div>
                    <!-- -->
                    <div class="form-group">
                        <label class="control-label">Degree(s)
                        </label>
                          <input type="text" class="form-control" name="degree" id="degree" value="<?php echo $userData['degree'];?>" maxlength="50"/>
                          <span class="help-block"></span>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label">Module Assignments
                        <span class="required"> * </span>
                      </label>
                      <div class="input-group">
                        <select class="form-control required" name="assignments[]" id="assignments" multiple="multiple" />
                        <option value='0'>None</option>
                        <?php foreach($moduleList as $mList) : ?>
                          <option value='<?php echo $mList['module_id'];?>'
                          <?php if(strpos($userData['assignments'], $mList['module_id']) !== false) echo ' selected';?>>
                          <?php echo $mList['module_name'];?>
                          </option>
                        <?php endforeach; ?>
                        </select>
                        <span class="helper-block"><small>(Hold Ctrl key to select multiple)</small>
                      </div>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label">Status
                        <span class="required"> * </span>
                      </label>
                      <div class="input-group">
                        <select class="form-control required" name="active" id="active" />
                        <option value=''>-- select --</option>
                        <option value='1' <?php if($userData['active']=='1') echo ' selected';?>>Active</option>
                        <option value='0' <?php if($userData['active']=='0') echo ' selected';?>>Inactive</option>
                        </select>
                      </div>
                    </div>
                    <!-- -->
                    <div class="form-actions">
                      <?php if($user_id == 0 || $user_id == NULL) :?>
                        <input type="submit" class="btn btn-info button-submit" value=" Add New ">&nbsp;&nbsp;&nbsp;
                      <?php else : ?>
                        <input type="submit" class="btn btn-success button-submit" value=" Submit ">&nbsp;&nbsp;&nbsp;
                      <?php endif; ?>
                      <a href='<?php echo base_url('Home/main');?>' class='btn btn-md btn-default'> Cancel </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-6">&nbsp;</div>
        </div>
        <!-- -->
        <div class="row">
        <div class="col-md-10">
          <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Current Users </span><br />
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body">
                <div class="table table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr><th>ID</th><th>Name</th><th>Email</th><th>Type</th><th>Active</th><th>Account</th><th>Contact</th></tr>
                    </thead>
                    <tbody>
                      <?php
                      if(count($userList) > 0) {
                        foreach($userList as $mList) {
                          $fullName = '';
                          if($mList['title'] != '') {$fullName .= $mList['title']." ";}
                          if($mList['first_name'] != '') {$fullName .= $mList['first_name']." ";}
                          if($mList['last_name'] != '') {$fullName .= $mList['last_name'];}
                          if($mList['degree'] != '') {$fullName .= ", ".$mList['degree'];}
                          echo "<tr>";
                          echo "<td>".$mList['user_id']."</td>";
                          echo "<td>".$fullName."</td>";
                          echo "<td>".$mList['email_address']."</td>";
                          echo "<td>";
                          if($mList['user_type'] == '1') echo "System Administrator";
                          if($mList['user_type'] == '2') echo "Client Administrator";
                          if($mList['user_type'] == '3') echo "User";
                          echo "</td>";
                          echo "<td>";
                          if($mList['active'] == 0) {echo 'No';}
                          else if($mList['active'] == 1) {echo "Yes";}
                          else echo 'Undefined';
                          echo "</td>";
                          echo "<td><a href='".base_url('Admin/user_accounts')."/".$mList['user_id']."'><button class='btn btn-sm'>Update</button></a></td>";
                          echo "<td><a href='".base_url('Admin/user_contact')."/".$mList['user_id']."'><button class='btn btn-sm'>Update</button></a></td>";
                          echo "</tr>";
                        }
                      } else {
                        echo "<tr><td colspan='8'>There are no users defined...</td></tr>";
                      }
                      ?>
                    </tbody>
                  </table>
                  </div>
                <!-- -->
                <div class="row"><div class="col-md-12">&nbsp;</div></div>
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

  <!--
  ** show last operation status
  -->
  <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
  <?php
  if($this->session->flashdata('success')) {
    echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
  } else if($this->session->flashdata('success')) {
    echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
  } ?>
  <script>
    function finalCheck() {
  	var err = false;
  	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
  	if($('#email_address').val() == "") {err=true;errMsg=errMsg+"- Please enter a valid email address.\n";}
    if($('#password').val() == "") {err=true;errMsg=errMsg+"- Please enter a password.\n";}
    if($('#password').val().length < 8) {err=true;errMsg=errMsg+"- Passwords should be at least 8 characers in length.\n";}
    var pswd1 = $('#password').val();
    var pswd2 = $('#rpassword').val();
    if(pswd1 != pswd2) {err=true;errMsg=errMsg+"- Passwords do not match.\n";}
    if($('#user_type').val() == "") {err=true;errMsg=errMsg+"- Please select an account type.\n";}
    if($('#first_name').val() == "") {err=true;errMsg=errMsg+"- Please enter the user's first name.\n";}
    if($('#last_name').val() == "") {err=true;errMsg=errMsg+"- Please enter the user's last name.\n";}
    if($('#assignments').val() == "") {err=true;errMsg=errMsg+"- Please select modules assignments for this user.\n";}
    if($('#active').val() == "") {err=true;errMsg=errMsg+"- Please indicate the user's status.\n";}
    if(err == true) {alert(errMsg); return false;}
    return true;
  }
  </script>
