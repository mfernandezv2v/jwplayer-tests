
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3>User Contact Information &nbsp;-&nbsp;<small> Add/Edit </small>&nbsp;<i class="fa fa-edit"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Edit Contact Information </span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body form">
                <form class="form" role="form" action='<?php echo base_url('Admin/user_contact_post');?>' method="POST" onsubmit='return finalCheck();'>
                  <div class="form-body">
                    <div class="form-group">
                        <label class="control-label"> Address Type
                            <span class="required"> * </span>
                        </label>
                            <input type="hidden" name="user_id" id="user_id" value='<?php echo $user_id;?>'/>
                            <select name="address_type" class="form-control">
                              <option value="">-- select --</option>
                              <?php foreach($addressTypes as $aType) {
                                echo "<option value='".$aType['address_type_id']."' ";
                                if($userData['address_type'] == $aType['address_type_id']) echo " selected";
                                echo ">".$aType['address_type_name']."</option>";
                              }
                              ?>
                            </select>
                    </div>
                    <!-- -->
                    <div class="form-group">
                        <label class="control-label"> Address </label>
                        <input type="text" class="form-control" name="address1" id="address1" size="25" maxlength="50" value='<?php echo $userData['address1'];?>'/>
                    </div>
                    <!-- -->
                    <div class="form-group">
                        <label class="control-label"> <small>(line 2)</small></label>
                        <input type="text" class="form-control" name="address2" id="address2" size="25" maxlength="50" value='<?php echo $userData['address2'];?>'/>
                    </div>
                    <!-- -->
                    <div class="form-group">
                        <label class="control-label"> City </label>
                        <input type="text" class="form-control" name="city" id="city" size="25" maxlength="50" value='<?php echo $userData['city'];?>'/>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label"> State </label>
                      <select name="state" id="state" class="form-control">
                        <option value="">-- select --</option>
                        <?php foreach($stateCodes as $sCode) {
                          echo "<option value='".$sCode['state_code']."' ";
                          if($userData['state'] == $sCode['state_code']) echo " selected";
                          echo ">".$sCode['state_code']." - ".$sCode['state_name']."</option>";
                        }
                        ?>
                      </select>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label"> Postal Code </label>
                      <input type="text" class="form-control" name="postal_code" id="postal_code" value="<?php echo $userData['postal_code'];?>" maxlength="20"/>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label"> Phone </label>
                      <input type="text" class="form-control" name="phone" id="phone" value="<?php echo $userData['phone'];?>" maxlength="20"/ onkeyup="formatAsPhone(this);">
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label"> Phone Ext </label>
                      <input type="text" class="form-control" name="phone_ext" id="phone_ext" valu="<?php echo $userData['phone_ext'];?>" maxlength="20"/>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label"> Time Zone </label>
                      <select name="time_zone" id="time_zone" class="form-control">
                        <option value="">-- select --</option>
                        <?php foreach($timeZones as $tzCode) {
                          echo "<option value='".$tzCode['time_zone_code']."' ";
                          if($userData['time_zone'] == $tzCode['time_zone_code']) echo " selected";
                          echo ">".$tzCode['time_zone_code']." - ".$tzCode['time_zone_name']."</option>";
                        }
                        ?>
                      </select>
                    </div>
                    <!-- -->
                    <div class="form-actions">
                      <?php if($user_id == 0 || $user_id == NULL) :?>
                        <input type="submit" class="btn btn-info button-submit" value=" Add New ">&nbsp;&nbsp;&nbsp;
                      <?php else : ?>
                        <input type="submit" class="btn btn-success button-submit" value=" Submit ">&nbsp;&nbsp;&nbsp;
                      <?php endif; ?>
                      <a href='<?php echo base_url('Admin/user_accounts');?>' class='btn btn-md btn-default'> Cancel </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-6">&nbsp;</div>
        </div>
        <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

  <!--
  ** show last operation status
  -->
  <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
  <?php
  if($this->session->flashdata('success')) {
    echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
  } else if($this->session->flashdata('success')) {
    echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
  }
  ?>
  <script>
    function finalCheck() {
  	var err = false;
  	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
  	if($('#address_type').val() == "") {err=true;errMsg=errMsg+"- Please enter a valid email address.\n";}
    if(err == true) {alert(errMsg); return false;}
    return true;
  }
  //
  function formatAsPhone(fld) {
    var numbers = fld.value.replace(/\D/g, ''),
        char = { 0: '', 3: '-', 6: '-' };
            fld.value = '';
            for (var i = 0; i < numbers.length; i++) {
                fld.value += (char[i] || '') + numbers[i];
            }
    }
  </script>
