    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3>User Groups &nbsp;-&nbsp;<small> Add/Edit </small>&nbsp;<i class="fa fa-edit"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Add/Edit User Groups </span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body form">
                <form class="form" role="form" action='<?php echo base_url('Admin/user_groups_post');?>' method="POST" onsubmit='return finalCheck();'>
                  <div class="form-body">
                    <div class="form-group">
                        <label class="control-label">Group Name
                            <span class="required"> * </span>
                        </label>
                            <input type="hidden" name="user_group_id" id="user_group_id" value='<?php echo $user_group_id;?>'/>
                            <input type="text" class="form-control required" name="user_group_name" id="user_group_name" value='<?php echo $groupData['user_group_name'];?>'/>
                    </div>
                    <!-- -->
                    <div class="form-group">
                      <label class="control-label">Status
                        <span class="required"> * </span>
                      </label>
                      <div class="input-group">
                        <select class="form-control required" name="active" id="active" />
                        <option value=''>-- select --</option>
                        <option value='1' <?php if($groupData['active']=='1') echo ' selected';?>>Active</option>
                        <option value='0' <?php if($groupData['active']=='0') echo ' selected';?>>Inactive</option>
                        </select>
                      </div>
                    </div>
                    <!-- -->
                    <div class="form-actions">

                      <?php if($groupData['user_group_id'] != '0') :?>
                        <input type="submit" class="btn green button-submit" value="Submit Changes">&nbsp;&nbsp;&nbsp;
                      <?php else :?>
                        <input type="submit" class="btn btn-info button-submit" value="Add New ">&nbsp;&nbsp;&nbsp;
                      <?php endif; ?>
                      <a href='<?php echo base_url('Home/main');?>' class='btn btn-md btn-default'> Cancel </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        <div class="col-md-6">
          <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Current User Groups </span><br />
                  <span class="helper" style="padding-left:18px;"><small>Click an group name to edit</small></span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed">
                    <thead>
                        <tr><th>ID</th><th>Name</th><th>Active</th></tr>
                    </thead>
                    <tbody>
                      <?php
                      if(count($groupList) > 0) {
                        foreach($groupList as $gList) {
                          echo "<tr>";
                          echo "<td>".$gList['user_group_id']."</td>";
                          echo "<td><a href='".base_url('Admin/user_groups/').$gList['user_group_id']."'>".$gList['user_group_name']."</a></td>";
                          echo "<td>";
                          if($gList['active'] == 0) {echo 'No';}
                          else if($gList['active'] == 1) {echo "Yes";}
                          else echo 'Undefined';
                          echo "</td>";
                          echo "</tr>";
                        }
                      } else {
                        echo "<tr><td colspan='3'>There are no user groups defined...</td></tr>";
                      }
                      ?>
                    </tbody>
                  </table>
                    <!-- -->
                    <div class="row"><div class="col-md-12">&nbsp;</div></div>
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
  <script>
    function finalCheck() {
  	var err = false;
  	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
  	if($('#user_group_name').val() == "") {err=true;errMsg=errMsg+"- Please enter a user group name.\n";}
    if($('#active').val() == "") {err=true;errMsg=errMsg+"- Please indicate the user group status.\n";}
    if(err == true) {alert(errMsg); return false;}
    return true;
  }
  </script>
