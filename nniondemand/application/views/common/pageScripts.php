<!--[if lt IE 9]>
<script src="<?php echo base_url('assets/theme/global/plugins/respond.min.js');?>"></script>
<script src="<?php echo base_url('assets/theme/global/plugins/excanvas.min.js');?>"></script>
<script src="<?php echo base_url('assets/theme/global/plugins/ie8.fix.min.js');?>"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url('assets/theme/global/plugins/jquery.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/global/plugins/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/global/plugins/js.cookie.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/global/plugins/jquery.blockui.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/global/plugins/bootstrap-growl/jquery.bootstrap-growl.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/global/plugins/select2/js/select2.full.min.js');?>" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url('assets/theme/global/plugins/jquery-validation/js/jquery.validate.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/global/plugins/jquery-validation/js/additional-methods.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/global/scripts/app.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/layouts/layout/scripts/layout.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/layouts/layout/scripts/demo.min.js');?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/theme/layouts/global/scripts/quick-sidebar.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/theme/layouts/global/scripts/quick-nav.min.js');?>" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
</body>

</html>
