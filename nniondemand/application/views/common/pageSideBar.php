<!-- check for active session, if not show timeout -->
<?php if(!$this->session->userdata('status')) { redirect('Home/timeout/', 'refresh'); }?>

<div class="clearfix"> </div>
   <!-- END HEADER & CONTENT DIVIDER -->
   <!-- BEGIN CONTAINER -->
<div class="page-container">
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu " data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200" >
          <li class="sidebar-toggler-wrapper ">
              <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
              <div class="sidebar-toggler hide"> </div>
              <!-- END SIDEBAR TOGGLER BUTTON -->
          </li>
            <?php if(!isset($this->session->userdata['activeMenu'])) : ?>
              <li class='nav-item active ' id='m1'>
                <?php $this->session->set_userdata('activeMenu','m1'); ?>
            <?php else : ?>
              <?php if($this->session->userdata['activeMenu'] == 'm1') : ?>
                <li class='nav-item active ' id='m1'>
              <?php else : ?>
                <li class='nav-item ' id='m1'>
              <?php endif; ?>
            <?php endif; ?>
                <a href="<?php echo base_url('Home/main');?>" class="nav-link nav-toggle " onclick='activeMenu("m1");'>
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            <!-- -->
            <?php if($this->session->userdata['activeMenu'] == 'm2') : ?>
              <li class='nav-item active' id='m2'>
            <?php else : ?>
              <li class='nav-item' id='m2'>
            <?php endif; ?>
                <a href="" class="nav-link nav-toggle " onclick='activeMenu("m2");'>
                    <i class="fa fa-book"></i>
                    <span class="title"> Learning Center </span>
                    <span class="arrow"></span>
                </a>
                  <ul class="sub-menu">
                    <li class="nav-item ">
                        <a href="<?php echo base_url('Learning/modules');?>" class="nav-link ">
                            <i class="fa fa-book"></i>
                            <span class="title"> Modules Available </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="<?php echo base_url('Learning/modules_completed');?>" class="nav-link ">
                            <i class="fa fa-check"></i>
                          <span class="title"> Modules Completed </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="<?php echo base_url('Learning/certificates');?>" class="nav-link ">
                            <i class="fa fa-graduation-cap"></i>
                            <span class="title"> Certificates </span>
                        </a>
                    </li>
                </ul>
              </li>
              <!-- -->
              <?php if($this->session->userdata['activeMenu'] == 'm3') : ?>
                <li class='nav-item active' id='m3'>
              <?php else : ?>
                <li class='nav-item ' id='m3'>
              <?php endif; ?>
                  <a href="<?php echo base_url('Home/my_account');?>" class="nav-link " onclick='activeMenu("m3");'>
                      <i class="fa fa-user"></i>
                      <span class="title">My Account</span>
                  </a>
              </li>
              <?php if($this->session->userdata['activeMenu'] == 'm4') : ?>
                <li class='nav-item active' id='m4'>
              <?php else : ?>
                <li class='nav-item ' id='m4'>
              <?php endif; ?>
                  <a href="<?php echo base_url('Home/logoff');?>" class="nav-link " onclick='activeMenu("m4");'>
                      <i class="fa fa-sign-out"></i>
                      <span class="title">Sign Out</span>
                  </a>
              </li>
              <!-- ADMIN AND CLIENT USERS ONLY -->
              <?php if($this->session->userdata('user_type') == USER_TYPE_ADMIN
                  || $this->session->userdata('user_type') == USER_TYPE_CLIENT ) { ?>
                  <li class="heading">
                      <h3 class="uppercase">Reports</h3>
                  </li>
                  <?php if($this->session->userdata['activeMenu'] == 'm20') : ?>
                    <li class='nav-item active' id='m20'>
                  <?php else : ?>
                    <li class='nav-item' id='m20'>
                  <?php endif ; ?>
                      <a href="" class="nav-link nav-toggle " onclick='activeMenu("m20");'>
                        <i class="fa fa-list-alt"></i>
                          <span class="title">User Reports</span>
                          <span class="arrow"></span>
                      </a>
                      <ul class="sub-menu">
                        <li class="nav-item ">
                            <a href="<?php echo base_url('Reports/user_progress_summary');?>" class="nav-link ">
                                <!--<i class="fa fa-user"></i>-->
                                <span class="title">User Progress Summary </span>
                            </a>
                        </li>
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Reports/user_progress');?>" class="nav-link ">
                                  <!--<i class="fa fa-user"></i>-->
                                  <span class="title">User Progress Detail </span>
                              </a>
                          </li>
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Reports/system_usage');?>" class="nav-link ">
                                  <!--<i class="fa fa-users"></i>-->
                                  <span class="title">System Usage Report</span>
                              </a>
                          </li>
                        </ul>
                      </il>
                  <?php } ?>

              <!-- ADMIN USERS ONLY -->
              <?php if($this->session->userdata['user_type'] == USER_TYPE_ADMIN ) { ?>
                  <li class="heading">
                      <h3 class="uppercase">Admin</h3>
                  </li>
                  <?php if($this->session->userdata['activeMenu'] == 'm10') : ?>
                    <li class='nav-item active' id='m10'>
                  <?php else : ?>
                    <li class='nav-item' id='m10'>
                  <?php endif ; ?>
                      <a href="" class="nav-link nav-toggle " onclick='activeMenu("m10");'>
                        <i class="icon-user"></i>
                          <span class="title">User Accounts</span>
                          <span class="arrow"></span>
                      </a>
                      <ul class="sub-menu">
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Admin/user_accounts');?>" class="nav-link ">
                                  <i class="fa fa-user"></i>
                                  <span class="title">User Accounts</span>
                              </a>
                          </li>
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Admin/user_groups');?>" class="nav-link ">
                                  <i class="fa fa-users"></i>
                                  <span class="title">User Groups</span>
                              </a>
                          </li>
                        </ul>
                      </li>
                  <!-- -->
                  <?php if($this->session->userdata['activeMenu'] == 'm11') : ?>
                    <li class='nav-item active' id='m11'>
                  <?php else : ?>
                    <li class='nav-item' id='m11'>
                  <?php endif; ?>
                    <a href="" class="nav-link nav-toggle " onclick='activeMenu("m11");'>
                      <i class="fa fa-table"></i>
                      <span class="title">Reference Tables</span>
                      <span class="arrow"></span>
                    </a>
                      <ul class="sub-menu ">
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Admin/adm_address_types');?>" class="nav-link ">
                                  <!--<i class="icon-check"></i>-->
                                  <span class="title">Address Types</span>
                              </a>
                          </li>
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Admin/adm_states');?>" class="nav-link ">
                                  <!--<i class="icon-check"></i>-->
                                  <span class="title">States</span>
                              </a>
                          </li>
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Admin/adm_time_zones');?>" class="nav-link ">
                                  <!--<i class="icon-check"></i>-->
                                  <span class="title">Time Zones</span>
                              </a>
                          </li>
                      </ul>
                  </li>
                  <?php if($this->session->userdata['activeMenu'] == 'm12') : ?>
                    <li class='nav-item active' id='m12'>
                  <?php else : ?>
                    <li class='nav-item' id='m12'>
                  <?php endif; ?>
                      <a href="" class="nav-link nav-toggle " onclick='activeMenu("m12");'>
                          <i class="icon-settings"></i>
                          <span class="title"> Admin Options </span>
                          <span class="arrow"></span>
                      </a>
                      <ul class="sub-menu ">
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Admin/sys_smtp');?>" class="nav-link ">
                                  <i class="icon-envelope"></i>
                                  <span class="title">SMTP Settings</span>
                              </a>
                          </li>
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Admin/sys_email_template');?>" class="nav-link ">
                                  <i class="fa fa-envelope"></i>
                                  <span class="title">Email Templates</span>
                              </a>
                          </li>
                          <li class="nav-item">
                            <a href="#" title="Email Notices">
                              <i class="fa fa-envelope"></i>
                              <span class="title"> Email Notices </span>
                              <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                              <li class="nav-item">
                                <a href="<?php echo base_url('Admin/email_reminder');?>" class="nav-linnk" title="Reminder Emails">
                                  <i class="fa fa-fw fa-caret-right"></i> Reminder Emails
                                  </a>
                              </li>
                              <li class="nav-item">
                                <a href="<?php echo base_url('Admin/email_noshow');?>" class="nav-link" title="No Show Emails">
                                  <i class="fa fa-fw fa-caret-right"></i> No Show Emails
                                  </a>
                              </li>
                          </ul>
                        </li>
                      </ul>
                  </li>
                  <!-- LESSONS & MODULES -->
                  <?php if($this->session->userdata['activeMenu'] == 'm13') : ?>
                    <li class='nav-item active' id='m13'>
                  <?php else : ?>
                    <li class='nav-item' id='m13'>
                  <?php endif; ?>
                      <a href="" class="nav-link nav-toggle " onclick='activeMenu("m13");'>
                          <i class="fa fa-book"></i>
                          <span class="title"> Lessons and Modules </span>
                          <span class="arrow"></span>
                      </a>
                      <ul class="sub-menu ">
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Admin/learning_modules');?>" class="nav-link ">
                                  <!--<i class="icon-envelope"></i> -->
                                  <span class="title"> Learning Modules </span>
                              </a>
                          </li>
                          <li class="nav-item ">
                              <a href="<?php echo base_url('Admin/lessons');?>" class="nav-link ">
                                  <!--<i class="fa fa-envelope"></i> -->
                                  <span class="title"> Lessons </span>
                              </a>
                          </li>
                          <!--
                          <li class="nav-item">
                            <a href="#" title="Email Notices">
                              <i class="fa fa-envelope"></i>
                              <span class="title"> Email Notices </span>
                              <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                              <li class="nav-item">
                                <a href="<?php echo base_url('Admin/email_reminder');?>" class="nav-linnk" title="Reminder Emails">
                                  <i class="fa fa-fw fa-caret-right"></i> Reminder Emails
                                  </a>
                              </li>
                              <li class="nav-item">
                                <a href="<?php echo base_url('Admin/email_noshow');?>" class="nav-link" title="No Show Emails">
                                  <i class="fa fa-fw fa-caret-right"></i> No Show Emails
                                  </a>
                              </li>
                          </ul>
                        </li>
                      -->
                      </ul>
                  </li>



            <?php } ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<script>
function activeMenu(menuItem) {
  var mItem = menuItem;
  var refItem = '#'+mItem;
  $(".nav-link").click(function(){
    $("li").removeClass("active");
    $(refItem).addClass("active");
  });

  $.ajax({
   type: "POST",//or POST
   url: 'common/utils/put-activeMenu.php',
   data: {mid: mItem},
   success: function(data){
     //$('#caseTable').html(data);
     console.log(data);//it will show the error log if any [optional]
   }
 });

}
</script>
