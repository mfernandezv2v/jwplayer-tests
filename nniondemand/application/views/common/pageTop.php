<!--<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">-->
      <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.html">
                            <!--<img src="../assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" />--> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="<?php echo base_url('assets/img/user_blue_icon.png');?>" />
                                    <span class="username username-hide-on-mobile">
                                     <?php if($this->session->userdata['display_name'] == '') {
                                       echo $this->session->userdata['first_name']." ".$this->session->userdata['last_name'];
                                     } else {
                                       echo $this->session->userdata['display_name'];
                                     }
                                     ?>
                                    </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                  <li>
                                      <a href="<?php echo base_url('Home/my_account');?>">
                                          <i class="icon-user"></i> My Account </a>
                                  </li>
                                  <li class="divider"> </li>
                                  <li>
                                      <a href="<?php echo base_url('Home/logoff');?>">
                                          <i class="fa fa-sign-out"></i> Sign Out </a>
                                  </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
