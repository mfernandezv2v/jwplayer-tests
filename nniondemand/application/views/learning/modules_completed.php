  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3> Learning Center <i class="fa fa-book"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
      <div class="row">
        <div class="col-md-12">
            <div class="portlet box">
              <div class="portlet-title bg-custom-blue-mid">
                <div class="caption">
                  <i class="fa fa-check font-white"></i>
                  <span class="caption-subject font-white"> Completed Learning Modules </span><br />
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body">
                <div class="table-responsive">
                  <table class='table table-bordered table-striped table-hover table-condensed'>
                  <thead><tr><th>Module</th><th width='40%'>Description</th><th>Lessons</th><th>Total Time</th><th>Completed</th><th>Date</th></tr></thead>
                    <tbody>
                      <?php if(count($moduleList) > 0) {
                        foreach($moduleList as $idx => $mList) {
                          // show only modules assigned to the user
                          if(strpos($userData['assignments'],$mList['module_id']) !== false) {
                            // CALC TOTAL TIME
                            $module_length = $this->Learning_model->calc_module_length($mList['module_id']);
                            // END TOTAL TIME
                            $completed = $this->Learning_model->is_module_completed($userData['user_id'],$mList['module_id']);
                            $is_completed = count($completed);
                            echo "<tr>";
                            echo "<td><span class='font-custom-blue-heavy bold'>".$mList['module_name']."</span></td>";
                            echo "<td>".$mList['module_description']."</td>";
                            echo "<td>".$mList['total_lessons']."</td>";
                            echo "<td>".date('i:s',strtotime($module_length))."</td>";
                            if($is_completed) {
                              echo "<td><span class='label label-success'>&nbsp;Yes&nbsp;</span></td>";
                              echo "<td>".date('m/d/Y',strtotime($completed['completed_date']))."</td>";
                            } else {
                              echo "<td><span class='label label-danger'>&nbsp;No&nbsp;</span></td>";
                              echo "<td></td>";
                            }
                          echo "</tr>";
                          }
                        }
                      } else {
                        echo "<tr><td>colspan='4'>There are no learning modules defined...</td></tr>";
                      } ?>
                    </tbody></table>
                    </div>
                  </div>
                  <!-- -->
                  <div class="row"><div class="col-md-12">&nbsp;</div></div>
                </div>
              </div>
            </div>
      <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
    </div>
    </div>
    <!--
    ** show last operation status
    -->
    <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
    <?php
    if($this->session->flashdata('success')) {
      echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
    } else if($this->session->flashdata('success')) {
      echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
    } ?>

    <!-- END CONTENT -->
    <script>
      function finalCheck() {
    	var err = false;
    	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
    	if($('#time_zone_code').val() == "") {err=true;errMsg=errMsg+"- Please enter a time zone code.\n";}
    	if($('#time_zone_name').val() == "") {err=true;errMsg=errMsg+"- Please enter the time zone full name.\n";}
      if($('#status').val() == "") {err=true;errMsg=errMsg+"- Please indicate the time zone status.\n";}
      if(err == true) {alert(errMsg); return false;}
      return true;
    }
    </script>
