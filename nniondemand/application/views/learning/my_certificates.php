  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3> Learning Center <i class="fa fa-book"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
      <div class="row">
        <div class="col-md-12">
            <div class="portlet box bg-custom-blue-light">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-check font-white"></i>
                  <span class="caption-subject font-white"> Learning Module Completion Status </span><br />
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body">
                <div class="table-responsive">
                  <table class='table table-bordered table-striped table-hover table-condensed'>
                  <thead><tr><th>Module</th><th width='40%'>Description</th><th>Lessons</th><th>Total Time</th><th>Completed</th><th>Date</th></tr></thead>
                    <tbody>
                      <?php if(count($moduleList) > 0) {
                        foreach($moduleList as $idx => $mList) {
                          // show only modules assigned to the user
                          if(strpos($userData['assignments'],$mList['module_id']) !== false) {
                            // CALC TOTAL TIME
                            $module_length = $this->Learning_model->calc_module_length($mList['module_id']);
                            // END TOTAL TIME
                            $completed = $this->Learning_model->is_module_completed($userData['user_id'],$mList['module_id']);
                            $is_completed = count($completed);
                            /*
                            if($prereq_completed !== false) {
                              $prereq_module = $this->Learning_model->get_learning_module($mList['module_id']);
                              echo "<tr><span class='alert alert-warning'>You must complete the prerequisite module: ";
                              echo $prereq_module['module_name'];
                              echo " before launching these lessons </span>";
                            } */
                            echo "<tr>";
                            echo "<td>".$mList['module_name']."</td>";
                            echo "<td>".$mList['module_description']."</td>";
                            echo "<td>".$mList['total_lessons']."</td>";
                            echo "<td>".date('i:s',strtotime($module_length))."</td>";
                            if($is_completed) {
                              echo "<td><span class='label label-success'>&nbsp;Yes&nbsp;</span></td>";
                              echo "<td>".date('m/d/Y',strtotime($completed['completed_date']))."</td>";
                            } else {
                              echo "<td><span class='label label-danger'>&nbsp;No&nbsp;</span></td>";
                              echo "<td></td>";
                            }
                          echo "</tr>";
                        }
                      }
                    } else {
                        echo "<tr><td>colspan='4'>There are no learning modules defined...</td></tr>";
                    } ?>
                    </tbody></table>
                    </div>
                  </div>
                  <!-- -->
                </div>
              </div>
              <!-- SHOW STATUS -->
              <div class="col-md-6">
                <div class="portlet box bg-custom-purple-light">
                  <div class="portlet-title">
                    <div class="caption">
                      <i class="fa fa-graduation-cap font-white"></i>
                      <span class="caption-subject font-white"> Certificate(s) of Completion </span><br />
                    </div>
                  </div> <!-- /caption -->
                  <div class="portlet-body">
                    <?php
                    if($userData['assignments'] != '') {
                      $assignments = explode(",", $userData['assignments']);
                      $assignments_completed = true;
                      foreach($assignments as $assigned) {
                        $module_completed = $this->Learning_model->is_module_completed($userData['user_id'], $assigned);
                        if(count($module_completed) == 0) {
                          $assignments_completed = false;
                        }
                      }
                    } else {
                      $assignments_completed = false;
                    }
                    ?>
                    <?php if ($assignments_completed == false) : ?>
                      <p><?php echo $this->lang->line('certificate_not_available');?></p>
                    <?php else : ?>
                      <p><?php echo $this->lang->line('certificate_info_1');?></p>
                      <p><?php echo $this->lang->line('certificate_info_2');?></p>
                      <p><?php echo $this->lang->line('certificate_info_3');?></p>
                      <p><?php echo $this->lang->line('certificate_info_4');?></p>
                      <p><?php echo $this->lang->line('certificate_info_5');?></p>
                      <p>
                        <!--<a href="<?php echo base_url('assets/docs/2017_NNI_Certification_of_Completion_Diabetes.pdf');?>" target="_blank" class="btn btn-mid bg-custom-purple-light font-white" onclick="logDownLoad();"> Download Certificate </a>-->
                        <a href="<?php echo base_url('assets/docs/2017_NNI_Certification_of_Completion_Diabetes.pdf');?>" target="_blank" class="btn btn-mid bg-custom-purple-light font-white" onclick="javascript:writeActivity()"> Download Certificate </a>
                      </p>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div> <!-- /row -->
      <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
    </div>
    </div>
    <!--
    ** show last operation status
    -->
    <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
    <?php
    if($this->session->flashdata('success')) {
      echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
    } else if($this->session->flashdata('success')) {
      echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
    } ?>

    <!-- END CONTENT -->
    <script>

    function writeActivity(evt_code, evt_tag) {
       var mod_id = 0;
       var less_id = 0;
       var evt_tag = '<?php echo LOG_CERTIFICATE_DOWNLOAD_TAG ?>';
       var evt_code = '<?php echo LOG_CERTIFICATE_DOWNLOAD ?>';
       $.ajax({
           type: "POST",
           url: "<?php echo base_url('Learning/log_lesson_activity');?>",
           data: {event_code: evt_code, event_tag: evt_tag, module_id: mod_id, lesson_id: less_id},
           success: function (data) {
               $('#message_section').html(data);
               console.log(data);//it will show the error log if any [optional]
           }
       });
     }

    </script>
