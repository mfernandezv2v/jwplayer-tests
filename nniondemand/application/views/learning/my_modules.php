  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3> Learning Center <i class="fa fa-book"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
      <div class="row">
        <div class="col-md-10">
            <div class="portlet box bg-custom-blue-mid">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-check font-white"></i>
                  <span class="caption-subject font-white"> Currently Available Learning Modules </span><br />
                  <span class="helper" style="padding-left:18px;"><small>Click a module name to view lessons</small></span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body">
                <div class="tabbable-line">
                  <ul class="nav nav-tabs ">
                    <?php
                     if(count($moduleList) > 0) {
                      foreach($moduleList as $idx => $mList) {
                        if(strpos($userData['assignments'],$mList['module_id']) !== false) {
                          if($mList['module_id'] == $module_id) {echo "<li class='active'>"; $counter=1;} else {echo "<li>";}
                          $module = "#".$mList['module_id'];
                          echo "<a href='".$module."' data-toggle='tab'><h4 class='font-custom-blue-heavy bold'>".$mList['module_name']."</h4></a>";
                          echo "</li>";
                        }
                      }
                    } ?>
                </ul>
                <div class="tab-content">
                      <?php if(count($moduleList) > 0) : ?>
                        <?php foreach($moduleList as $idx => $mList) : ?>
                          <?php if($mList['module_id'] == $module_id) : ?>
                            <div class='tab-pane active' id='<?php echo $mList['module_id'];?>'>
                          <?php else : ?>
                            <div class='tab-pane' id='<?php echo $mList['module_id']; ?>'>
                          <?php endif; ?>
                          <div class="table-responsive">
                          <table class='table table-bordered table-striped table-hover table-condensed'>
                          <thead><tr><th width='40%'>Description</th><th>Lessons</th><th>Total Time</th><th>Completed</th><th>Date</th></tr></thead>
                          <tbody>
                          <?php
                            // CHECK PREREQUISITE
                            if($mList['prerequisite'] == '0') {
                              // if zero, no prereq required
                              $prereq_complete = true;
                            } else {
                              $prereq_complete = $this->Learning_model->is_prerequisite_completed($userData['user_id'], $mList['prerequisite']);
                            }
                            // CALC TOTAL TIME
                            $module_length = $this->Learning_model->calc_module_length($mList['module_id']);
                            $is_completed = $this->Learning_model->is_module_completed($userData['user_id'],$mList['module_id']);
                            if($prereq_complete == false) {
                              $prereq_module = $this->Learning_model->get_learning_module($mList['prerequisite']);
                              echo "<tr><td colspan='5'><div class='alert alert-warning'>You must complete the prerequisite module: <strong>";
                              echo $prereq_module['module_name'];
                              echo " </strong> before launching these lessons </div></td></tr>";
                            }
                          ?>
                          <tr>
                          <td><?php echo $mList['module_description'];?></td>
                          <td><?php echo $mList['total_lessons'];?></td>
                          <td><?php echo date('i:s',strtotime($module_length));?></td>
                          <?php
                            if($is_completed) {
                              echo "<td><span class='label label-success'>&nbsp;Yes&nbsp;</span></td>";
                              echo "<td>".date('m/d/Y',strtotime($is_completed['completed_date']))."</td>";
                            }
                            else {
                              echo "<td><span class='label label-danger'>&nbsp;No&nbsp;</span></td>";
                              echo "<td></td>";
                            }
                            ?>
                          </tr>
                          </tbody></table>
                          </div>
                          <!-- SHOW LESSONS -->
                          <div class="col-md-12 panel panel-info">
                            <div class="panel-heading"><h4 class="font-custom-blue-mid bold"> Lessons </h4>
                            </div>
                          <div class="panel-body">
                          <div class="table-responsive">
                          <table class='table table-bordered table-striped table-hover table-condensed'>
                          <thead><tr>
                            <th class="font-custom-blue-mid">Name</th>
                            <th width='40%' class="font-custom-blue-mid">Description</th>
                            <th class="font-custom-blue-mid">Total Time</th>
                            <th class="font-custom-blue-mid" width="5%">Status</th>
                            <th class="font-custom-blue-mid">Date</th>
                          </tr></thead>
                          <tbody>
                          <?php $lessons = $this->Learning_model->get_lessons_by_module($mList['module_id']); ?>
                          <?php if(count($lessons) > 0) : ?>
                            <?php $next_lesson = ''; ?>
                            <?php foreach($lessons as $less) : ?>
                              <?php
                              $lesson_time = "00:".$less['lesson_length_min'].":".$less['lesson_length_sec'];
                              // if prerequisites have been completed the provide action buttons
                              // otherwise show 'not available'
                              if($prereq_complete) {
                                $lesson_complete = $this->Learning_model->check_lesson_complete($less['lesson_id'], $userData['user_id']);
                                if(count($lesson_complete) > 0) {
                                  $status = "<a href='".base_url('Learning/play_lesson/').$less['lesson_id']."' class='btn btn-block btn-success font-custom-blue-heavy' data-toggle='tooltip' title='This lesson was completed on ".date('m/d/Y', strtotime($lesson_complete['completed_date']))."'>&nbsp;&nbsp;Completed&nbsp;&nbsp;</a>";
                                } else {
                                  // the first eligible lesson gets an action button
                                  // all others are disabled until the currently available one has been completed
                                  if($next_lesson == '') {
                                    $next_lesson = $less['lesson_id'];
                                    $status = "<a href='".base_url('Learning/play_lesson/').$less['lesson_id']."' class='btn btn-block btn-warning font-custom-blue-heavy' data-toggle='tooltip' title='Click to launch this lesson'> Launch Lesson&nbsp;&nbsp;</a>";
                                  } else {
                                    $status = "<a href='javascript:;' class='btn btn-block btn-default font-custom-blue-heavy disabled'>Not Completed&nbsp;&nbsp;</a>";
                                  }
                                }
                              } else {
                                $status = "<a href='javascript:;' class='btn btn-block btn-default font-custom-blue-heavy disabled'>Not Available&nbsp;&nbsp;</a>";
                              }
                              ?>
                              <tr>
                                <td><?php echo $less['lesson_name'];?></td>
                                <td><?php echo $less['lesson_description'];?></td>
                                <td><?php echo date('i:s',strtotime($lesson_time));?></td>
                                <td><?php echo $status;?></td>
                                <td>
                                <?php
                                  if($lesson_complete['completed_date'] !== NULL) {
                                    echo date('m/d/Y', strtotime($lesson_complete['completed_date']));
                                  } ?>
                                </td>
                              </tr>
                            <?php endforeach; ?>
                          <?php else : ?>
                            <tr><td colspan='5'>There are no lessons defined for this module.</td></tr>
                          <?php endif; ?>
                        </tbody></table>
                      </div></div></div></div>
                        <?php endforeach; ?>
                      <?php else : ?>
                        <table class='table table-bordered table-striped table-hover table-condensed'>
                        <thead><tr><th width='40%'>Description</th><th>Lessons</th><th>Total Time</th><th>Completed</th></tr></thead>
                        <tbody><tr><td>colspan='4'>There are no learning modules defined...</td></tr></tbody>
                        </table>
                        </div>
                      <?php endif; ?>
                    <!-- -->
                  <div class="row"><div class="col-md-12">&nbsp;</div></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
    </div>
    <!--
    ** show last operation status
    -->
    <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
    <?php
    if($this->session->flashdata('success')) {
      echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
    } else if($this->session->flashdata('success')) {
      echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
    } ?>

    <!-- END CONTENT -->
    <script>
      function finalCheck() {
    	var err = false;
    	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
    	if($('#time_zone_code').val() == "") {err=true;errMsg=errMsg+"- Please enter a time zone code.\n";}
    	if($('#time_zone_name').val() == "") {err=true;errMsg=errMsg+"- Please enter the time zone full name.\n";}
      if($('#status').val() == "") {err=true;errMsg=errMsg+"- Please indicate the time zone status.\n";}
      if(err == true) {alert(errMsg); return false;}
      return true;
    }
    </script>
