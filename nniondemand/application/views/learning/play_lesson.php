  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <!--
          <div class="page-title">
            <h3> Lesson Library <i class="fa fa-book"></i></h3>
          </div>
        -->
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
      <div class="row">
        <div class="col-md-10">
            <div class="portlet box bg-custom-blue-mid">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-check font-white"></i>
                  <span class="caption-subject font-white">
                    <?php echo $lessonData['lesson_name'];?>
                    <?php if($lessonData['lesson_length_min'] > 0) {
                      $lesson_length = "00:".$lessonData['lesson_length_min'].":".$lessonData['lesson_length_sec'];
                      echo "&nbsp;&nbsp;<small>(Total Lesson Time:&nbsp;".date('i:s',strtotime($lesson_length)).")</small>";
                    } ?>
                   </span>
                  <!--<span class="helper" style="padding-left:18px;"><small>Click a module name to view lessons</small></span>-->
                </div>
              </div>
              <!-- -->
              <div class="portlet-body">
                <div class="row">
                  <div class="col-md-12">
                <center>
                    <div class="embed-responsive embed-responsive-16by9">
                    <div id = 'video' name='video'>
                    </div>  
                    <button id="Back15s" onclick="volta15s()"> Get back 15s </button>
                    <button id="Forward15s" onclick="adianta15s()"> Forward 15s </button>
                    <button id="VolumePlus" onclick="riseVolume()"> Rise Volume </button>
                    <button id="VolumeDown" onclick="downVolume()"> Down Volume </button>
                    <script type="text/javascript">
                      
                      jwplayer('video').setup({
                          file: "videos/5steps.mp4",
                          image: "img/5steps.png",
                          //flashplayer: "jwplayer-7.10.4/jwplayer.flash.swf",
                        height: "75%",
                        width: "75%",
                        tracks: [{ 
                              file: "assets/captions/english.vtt", 
                              label: "English",
                              kind: "captions",
                              "default": true} ],
                        logo: {
                          file: 'img/V2V_logo_transp_75.png',
                          link: 'http://www.vision2voice.com'
                        },
                        abouttext: "Video Available at Vision2Voice.com",             
                            
                        aspectratio: "16:9",
                        stretching: "fill"
                        
                      });     
                      function volta15s() {
                        sharingPlugin.open();
                        if (jwplayer('video').getPosition() > 15)
                          jwplayer('video').seek (jwplayer('video').getPosition()-15);
                        else 
                          jwplayer('video').seek(0);
                      }
                      function adianta15s() {
                          jwplayer('video').seek (jwplayer('video').getPosition()+15);
                      }     
                      function riseVolume() {
                        jwplayer('video').setVolume(jwplayer('video').getVolume() + 10);
                      }
                      function downVolume () {
                        jwplayer('video').setVolume(jwplayer('video').getVolume() - 10);
                      }
                      </script>
                    <!-- <script src="//content.jwplatform.com/players/gC8hhnUH-MH5L5mzx.js"></script>-->
                      <!-- <video id="myVideo" class="embed-responsive-item" preload="auto" poster="<?php echo base_url('assets/media/').$lessonData['lesson_image'];?>" -->

                    </div>
                    </center>
                  </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                    <div class="control">
                        <a href="javascript:playVideo();" class="btnPlay btn btn-sm btn-success" data-toggle='tooltip' title='Begin playback'><i class='fa fa-play'>&nbsp;</i>&nbsp;Play&nbsp;</a>
                        <a href="javascript:pauseVideo();" class="btnPlay btn btn-sm btn-danger" data-toggle='tooltip' title='Pause playback'><i class='fa fa-pause'>&nbsp;</i>&nbsp;Pause&nbsp;</a>
                        <a href="javascript:restartVideo();" class="btn btn-sm btn-primary" data-toggle='tooltip' title='Restart playback from the beginning'><i class='fa fa-rotate-left'>&nbsp;</i>&nbsp;Restart&nbsp;</a>
                        <a href="javascript:rewindVideo(-15);" class="btn btn-sm btn-default" data-toggle='tooltip' title='Back up 15 seconds'><i class='fa fa-backward'>&nbsp;</i>&nbsp;Backup 15s&nbsp;</a>
                        <a href="javascript:changeVolume(0.2);" class="btn btn-sm btn-default" data-toggle='tooltip' title='Increase volume'><i class='fa fa-arrow-up'>&nbsp;</i>&nbsp;Volume&nbsp;</a>
                        <a href="javascript:changeVolume(-0.2);" class="btn btn-sm btn-default" data-toggle='tooltip' title='Decrease volume'><i class='fa fa-arrow-down'>&nbsp;</i>&nbsp;Volume&nbsp;</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label id="playback_meter" class='label label-info'></label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo base_url('Learning/modules');?>/<?php echo $lessonData['module_id'];?>" id="finished" class="btn btn-lg bg-custom-green-bold font-white pull-right" style="display:none;"><i class='fa fa-check'></i> Lesson Completed!<br /><small>Click to Exit</small></a> 
                    </div>
                    </div>
                  </div>
                  <div class="row">
                    <div id="message_section" class="col-md-6"><!-- ajax messages here -->&nbsp;</div>
                    <div class="col-md-6">&nbsp;</div>
                </div>
                <!-- -->
              </div>
            </div>
          </div>
        </div>
      <!-- END PAGE BASE CONTENT -->
    </div>
  <!-- END CONTENT BODY -->
  </div>

  <script>
  function writeActivity(evt_code, evt_tag) {
     var mod_id = '<?php echo $lessonData['module_id'];?>';
     var less_id = '<?php echo $lessonData['lesson_id'];?>';
     $.ajax({
         type: "POST",
         url: "<?php echo base_url('Learning/log_lesson_activity');?>",
         data: {event_code: evt_code, event_tag: evt_tag, module_id: mod_id, lesson_id: less_id},
         success: function (data) {
             $('#message_section').html(data);
             console.log(data);//it will show the error log if any [optional]
         }
     });
   }

   function writeLog() {
      var mod_id = '<?php echo $lessonData['module_id'];?>';
      var less_id = '<?php echo $lessonData['lesson_id'];?>';
      $.ajax({
          type: "POST",
          url: "<?php echo base_url('Learning/log_lesson_complete');?>",
          data: {module_id: mod_id, lesson_id: less_id},
          success: function (data) {
              $('#message_section').html(data);
              console.log(data);//it will show the error log if any [optional]
          }
      });
    }

 function secondsToHms(d) {
     d = Number(d);
     var h = Math.floor(d / 3600);
     var m = Math.floor(d % 3600 / 60);
     var s = Math.floor(d % 3600 % 60);

     var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
     var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
     var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
     return hDisplay + mDisplay + sDisplay;
   }

  function pauseVideo() {
    var video = $('#myVideo');
    video[0].pause();
    //return false;
  }

  function playVideo() {
    var video = $('#myVideo');
    video[0].play();
    //return true;
  }

  //Rewind control
  function restartVideo() {
      var video = $('#myVideo');
      video[0].pause();
      video[0].load();
      video[0].play();
      var event_tag = '<?php echo LOG_PLAYBACK_RESTART_TAG ?>';
      var event_code = '<?php echo LOG_PLAYBACK_RESTART ?>';
      writeActivity(event_code, event_tag);
      //return false;
  }

  function rewindVideo(param) {
    var video = $('#myVideo');
    video[0].currentTime += param;
    var event_tag = '<?php echo LOG_PLAYBACK_REWIND_TAG ?>';
    var event_code = '<?php echo LOG_PLAYBACK_REWIND ?>';
    writeActvity(event_code, event_tag);
    //return false;
  }

  function changeVolume(param) {
    var video = $('#myVideo');
    video[0].volume += param;
    //return false;
  }

  var vid = document.getElementById("myVideo");

  vid.ontimeupdate = function() {myFunction()};
  function myFunction() {
    // Display the current position of the video in a <p> element with id="demo"
    var current_position = secondsToHms(vid.currentTime);
    document.getElementById("playback_meter").innerHTML = current_position;
  }

  vid.onplay = function() {
    vid.removeAttribute("controls");
    dt = new Date();
    //alert("The video has started to play - "+dt);
    var event_tag = '<?php echo LOG_PLAYBACK_START_TAG ?>';
    var event_code = '<?php echo LOG_PLAYBACK_START ?>';
    writeActivity(event_code, event_tag);
  };

  document.getElementById('myVideo').addEventListener('ended',myHandler,false);
    function myHandler(e) {
    dt = new Date();
    //alert("video ended - "+dt);
    var event_tag = '<?php echo LOG_PLAYBACK_END_TAG ?>';
    var event_code = '<?php echo LOG_PLAYBACK_END ?>';
    writeActivity(event_code, event_tag);
    $('#finished').show();
    setTimeout(writeLog(), 500);
  }

  document.getElementById('myVideo').addEventListener('pause',myPausedHandler,false);
    function myPausedHandler(e) {
    dt = new Date();
    //alert("video paused - "+dt);
    //writeLog('video paused');
    var event_tag = '<?php echo LOG_PLAYBACK_STOP_TAG ?>';
    var event_code = '<?php echo LOG_PLAYBACK_STOP ?>';
    writeActivity(event_code, event_tag);
  }

  </script>
