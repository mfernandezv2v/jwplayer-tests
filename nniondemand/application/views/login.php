<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url('assets/theme/pages/css/login.min.css');?>" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
          <!--
            <a href="<?php echo base_url('Home/index');?>">
                <img src="<?php echo base_url('assets/img/signin_logo.png');?>" alt="" /> </a>
              -->
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php echo base_url('home/login_validate');?>" method="post">
              <img src="<?php echo base_url('assets/img/signin_logo.png');?>" class="img-responsive" alt="" /> </a>
                <h3 class="form-title font-green">Sign In</h3>

                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="user_name" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="user_password" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase">Login</button>
                    <!--
                    <label class="rememberme check mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" />Remember
                        <span></span>
                    </label>
                  -->
                    <a href="<?php echo base_url('Home/resetpswd');?>" id="forget-password" class="forget-password">Forgot Password?</a>
                </div>
                <!--
                <div class="login-options">
                    <h4>Or login with</h4>
                    <ul class="social-icons">
                        <li>
                            <a class="social-icon-color facebook" data-original-title="facebook" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color twitter" data-original-title="Twitter" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color linkedin" data-original-title="Linkedin" href="javascript:;"></a>
                        </li>
                    </ul>
                </div>
              -->
                <div class="create-account">
                    <p>
                        <!--<a href="javascript:;" id="register-btn" class="uppercase">Create an account</a>-->
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright">
          <span class="font-white"><?php echo date('Y');?> <?php echo $this->lang->line('footer_copyright');?> <br /></span>
          <span class="font-white"><?php echo $this->lang->line('footer_line1');?></span>
        </div>

        <!--
        ** show login error if it was set
        -->
        <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
        <?php
        if($this->session->flashdata('login_error')) {
          echo "<script>javascript: growlError('".$this->session->flashdata('login_error')."');</script>";
        } else if($this->session->flashdata('success')) {
          echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
        }

        ?>
