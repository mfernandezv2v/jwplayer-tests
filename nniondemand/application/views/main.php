
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
          <!-- BEGIN PAGE HEADER-->
          <!-- PAGE CONTENT -->
          <!--
          <div class="row">
            <div class="col-md-6">
              <center><h3><?php echo $this->lang->line('app_client_name')." - ".$this->lang->line('app_title');?></h3></center>
            </div>
          </div>
        -->
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue-hoki" href="#">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="12,5"><?php echo $number_modules;?></span></div>
                        <div class="desc"> Learning Modules Available </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue-soft" href="#">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="12,5"><?php echo $modules_completed;?></span></div>
                        <div class="desc"> Learning Modules Completed </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green-soft" href="#">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="12,5"><?php echo $lessons_completed;?></span></div>
                        <div class="desc"> Lessons Completed </div>
                    </div>
                </a>
            </div>
          </div>
          <!-- -->
          <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12">
              <div class="panel panel-info">
                <div class="panel-heading"> Welcome! </div>
                <div class="panel-body">
                  <p><h3><?php echo $this->lang->line('main_info_title');?></h3></p>
                  <p><?php echo $this->lang->line('main_info_content');?></p>
                  <p class="pull-right"><img src="<?php echo base_url('assets/img/page_top_logo.png');?>" class="img-responsive" border="0" /></p>
                </div>
              </div>
            </div>
            <!--
            <div class="col-lg-6 col-md-6 col-xs-12">
              <div class="panel panel-warning">
                <div class="panel-heading"> Alerts</div>
                <div class="panel-body">
                  In per cetero aeterno. In feugiat nostrum adipisci eam, ea ancillae pertinax vis. Vel te affert reprehendunt, nec doming electram molestiae et, ut adhuc tempor pro. Solum iracundia interesset no nam, nec ea aeque quaestio intellegat. No probo inani est, qui persius praesent te.
                </div>
              </div>
            </div>
          -->
          </div>
        </div>
      </div>
