
<!--**********************************************************************
 *	Name:
 *		rpt_system_usage_excel.php
 *	Description:
 *		excel report showing user system usage
 *	Copyright:
 *		Copyright (C) 2017 - Vision2Voice, Inc.  All rights reserved.
 *	History:
 *		bj20170301 - Initial code
 ************************************************************************-->
<?php
	require_once('PHPExcel/Classes/PHPExcel.php');
	$objPHPExcel = new PHPExcel;
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	$dollarFormat = '$#,#0.00;[Red]-$#,#0.00';
	$decimalFormat = '#,#0.00;[Red]-#,#0.00';
	//  $objPHPExcel->getActiveSheet()->setCellValue('A7', 'Total:')->setCellValue('B7', '=SUM(B2:B6)');
	// writer already created the first sheet for us, let's get it
	//$objPHPExcel->createSheet();
	$styleDefault10 = array(
		'font'  => array(
				'bold'  => false,
				'color' => array('rgb' => '000000'),
				'size'  => 10,
				'name'  => 'Calibri'
		));
	$styleBold10 = array(
		'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '000000'),
				'size'  => 10,
				'name'  => 'Calibri'
		));
	$styleFontGreen = array(
		'font'  => array(
				'bold'  => false,
				'color' => array('rgb' => '008000'),
				'size'  => 10,
				'name'  => 'Calibri'
		));
	$styleFontRed = array(
		'font'  => array(
				'bold'  => false,
				'color' => array('rgb' => 'FF0000'),
				'size'  => 10,
				'name'  => 'Calibri'
		));
	$styleBoldUnderline10 = array(
		'font'  => array(
				'bold'  => true,
				'underline' => true,
				'color' => array('rgb' => '000000'),
				'size'  => 10,
				'name'  => 'Calibri'
		));
		$styleBold12 = array(
			'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => '000000'),
					'size'  => 12,
					'name'  => 'Calibri'
			));
		$styleGridlines = array(
	  'borders' => array(
	    'allborders' => array(
	      'style' => PHPExcel_Style_Border::BORDER_THIN
	    )
	  )
	);
	$styleHeadingRed = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FF0000'),
        'size'  => 10,
        'name'  => 'Calibri'
    ));
	$styleHeadingDefault = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 10,
        'name'  => 'Calibri'
    ));
	$currentSheet = 0;
	$objPHPExcel->setActiveSheetIndex($currentSheet);
	$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
	$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
	$objSheet = $objPHPExcel->getActiveSheet();
	$objSheet->setTitle('System Usage Report' );
	$objPHPExcel->getActiveSheet()->getTabColor()->setARGB('92D050');
	//first heading line
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
	$objSheet->getCell('A1')->setValue("System Usage Report for: ".$userData['last_name'].", ".$userData['first_name']." - ".date('m/d/Y H:i:s'));
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('EEEEEE');
	$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($styleBold12);
	$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
	//
	$objSheet->getCell('A2')->setValue('Module');
	$objSheet->getCell('B2')->setValue('Lesson');
	$objSheet->getCell('C2')->setValue('Code');
	$objSheet->getCell('D2')->setValue('Activity');
	$objSheet->getCell('E2')->setValue('Date');
	$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('EEEEEE');
	$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->applyFromArray($styleBold12);
	//
	$row = 3;
	$objSheet->getCell('A3')->setValue('User Name');
	foreach($activity as $act) {
			$module_name = $lesson_name = '';
			foreach($modules as $idx => $mod) {
			if($act['module_id'] == $mod['module_id']) {
				$module_name = $mod['module_name'];
				break;
				}
			}
			foreach($lessons as $idx => $les) {
			if($act['lesson_id'] == $les['lesson_id']) {
				$lesson_name = $les['lesson_name'];
				break;
				}
			}
		$objSheet->getCell('A'.$row)->setValue($module_name);
		$objSheet->getCell('B'.$row)->setValue($lesson_name);
		$objSheet->getCell('C'.$row)->setValue($act['event_code']);
		$objSheet->getCell('D'.$row)->setValue($act['event_tag']);
		$objSheet->getCell('E'.$row)->setValue(date('m/d/Y H:i:s',strtotime($act['log_date'])));
		$row++;
	} // end foreach

	// WRAP UP - WRITE OUTPUT
 	$filename = "system_usage_report_".date('Ymd').".xlsx";
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	//
	header('Content-Disposition: attachment; filename="'.$filename.'"');
	$objWriter->save('php://output');

?>
