  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3> Reports <i class="fa fa-list-alt"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-6">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> Users </span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body form">
                <form class="form" role="form" action="" method="">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="control-label"> Select a User Name </label>
                      <div class="input-group">
                        <select name="user_id" id="user_id" class="form-control">
                          <option value="">-- select -- </option>
                          <?php foreach($userList as $uList) : ?>
                            <option value='<?php echo $uList['user_id'];?>'><?php echo $uList['last_name'].", ".$uList['first_name'];?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="form-group">
                      <a href="javascript:showProgress();" class="btn btn-md btn-info"> Run Report </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

      <div class="row">
        <div class="col-md-12">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <!--<i class="fa fa-edit font-white"></i>-->
                  <span class="caption-subject font-white"> User Progress Report </span><br />
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body">
                  <div id="report_section" >
                  <!-- selected lesson list goes here -->

                  </div>
                  <!-- -->
                <div class="row"><div class="col-md-12">&nbsp;</div></div>
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
  <script>
    function showProgress() {
      var user = $('#user_id').val();
      $.ajax({
          type: "GET",//or POST
          url: '<?php echo base_url("Reports/show_user_progress");?>',
          data: {user_id: user},
          success: function (data) {
              $('#report_section').html(data);
              console.log(data);//it will show the error log if any [optional]
          }
      });
    }
  </script>
