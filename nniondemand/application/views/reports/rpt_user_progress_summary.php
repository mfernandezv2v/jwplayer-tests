  <div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h3> Reports <i class="fa fa-list-alt"></i></h3>
          </div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-12">
            <div class="portlet box grey-cascade">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit font-white"></i>
                  <span class="caption-subject font-white"> User Progress (Summary) Report </span>
                </div>
              </div> <!-- /caption -->
              <div class="portlet-body form">
                <form class="form" role="form" action="" method="">
                  <div class="form-body">
                    <div class="form-group">
                      <a href="<?php echo base_url('Reports/user_progress_summary_excel');?>" class="btn btn-md btn-info"> Export to Excel </a>
                    </div>
                  </div>
                </form>
                <div class="table-responsive">
                  <table class="table table-striped table-hover table-condensed">
                    <caption>(Columns showing a date indicate completed modules.  N/A = not assigned)</caption>
                    <thead>
                      <tr>
                        <td> Name </td>
                        <?php foreach($moduleList as $mList) {
                          echo "<td>".$mList['module_name']."</td>";
                        } ?>
                        </tr>
                        </thead>
                        <tbody>
                          <tr>
                        <?php foreach($userList as $uList) {
                            echo "<td>".$uList['last_name'].", ".$uList['first_name']."</td>";
                            foreach($moduleList as $mList) {
                              if(strpos($uList['assignments'], $mList['module_id']) !==false) {
                                $is_completed = $this->Learning_model->is_module_completed($uList['user_id'],$mList['module_id']);
                                if($is_completed) {
                                  echo "<td><span class='font-green'>".date('m/d/Y',strtotime($is_completed['completed_date']))."</span></td>";
                                } else {
                                  echo "<td><span class='font-red'>&nbsp;No&nbsp;</span></td>";
                                }
                              } else {
                                echo "<td>N/A</td>";
                              }
                          }
                          echo "</tr>";
                        } ?>
                      </tbody>
                  </table>

                </div>

              </div>
            </div>
          </div>
        </div>
      <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
    </div>
  <script>
    function showProgress() {
      var user = $('#user_id').val();
      $.ajax({
          type: "GET",//or POST
          url: '<?php echo base_url("Reports/show_user_progress");?>',
          data: {user_id: user},
          success: function (data) {
              $('#report_section').html(data);
              console.log(data);//it will show the error log if any [optional]
          }
      });
    }
  </script>
