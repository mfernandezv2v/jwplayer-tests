
<!--**********************************************************************
 *	Name:
 *		user_progress_summary_excel.php
 *	Description:
 *		excel report showing list of users and modules completed
 *	Copyright:
 *		Copyright (C) 2017 - Vision2Voice, Inc.  All rights reserved.
 *	History:
 *		bj20170227 - Initial code
 ************************************************************************-->
<?php
	require_once('PHPExcel/Classes/PHPExcel.php');
	$objPHPExcel = new PHPExcel;
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	$dollarFormat = '$#,#0.00;[Red]-$#,#0.00';
	$decimalFormat = '#,#0.00;[Red]-#,#0.00';
	//  $objPHPExcel->getActiveSheet()->setCellValue('A7', 'Total:')->setCellValue('B7', '=SUM(B2:B6)');
	// writer already created the first sheet for us, let's get it
	//$objPHPExcel->createSheet();
	$styleDefault10 = array(
		'font'  => array(
				'bold'  => false,
				'color' => array('rgb' => '000000'),
				'size'  => 10,
				'name'  => 'Calibri'
		));
	$styleBold10 = array(
		'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '000000'),
				'size'  => 10,
				'name'  => 'Calibri'
		));
	$styleFontGreen = array(
		'font'  => array(
				'bold'  => false,
				'color' => array('rgb' => '008000'),
				'size'  => 10,
				'name'  => 'Calibri'
		));
	$styleFontRed = array(
		'font'  => array(
				'bold'  => false,
				'color' => array('rgb' => 'FF0000'),
				'size'  => 10,
				'name'  => 'Calibri'
		));
	$styleBoldUnderline10 = array(
		'font'  => array(
				'bold'  => true,
				'underline' => true,
				'color' => array('rgb' => '000000'),
				'size'  => 10,
				'name'  => 'Calibri'
		));
		$styleBold14 = array(
			'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => '000000'),
					'size'  => 14,
					'name'  => 'Calibri'
			));
		$styleGridlines = array(
	  'borders' => array(
	    'allborders' => array(
	      'style' => PHPExcel_Style_Border::BORDER_THIN
	    )
	  )
	);
	$styleHeadingRed = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FF0000'),
        'size'  => 10,
        'name'  => 'Calibri'
    ));
	$styleHeadingDefault = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 10,
        'name'  => 'Calibri'
    ));
	$currentSheet = 0;
	$objPHPExcel->setActiveSheetIndex($currentSheet);
	$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
	$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
	$objSheet = $objPHPExcel->getActiveSheet();
	$objSheet->setTitle('User Progress Summary Report' );
	$objPHPExcel->getActiveSheet()->getTabColor()->setARGB('92D050');
	//first heading line
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	$objSheet->getCell('A1')->setValue("User Progress Summary Report: ".date('m/d/Y H:i:s'));
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('EEEEEE');
	$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($styleBold14);
	$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
	//
	$objSheet->getCell('A2')->setValue('(Columns showing a date indicate completed modules.  N/A = not assigned)');
	$objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('EEEEEE');
	$objPHPExcel->getActiveSheet()->getStyle('A2:D2')->applyFromArray($styleBold14);
	$objPHPExcel->getActiveSheet()->mergeCells('A2:D2');
	//
	$objSheet->getCell('A3')->setValue('User Name');
	foreach($moduleList as $idx => $mList) {
		switch ($idx)
		{
    	case 0: $col = 'B3'; break;
    	case 1: $col = 'C3'; break;
			case 2: $col = 'D3'; break;
			case 3: $col = 'E3'; break;
			case 4: $col = 'F3'; break;
			case 5: $col = 'G3'; break;
			case 6: $col = 'H3'; break;
			case 7: $col = 'I3'; break;
			case 8: $col = 'I3'; break;
			case 9: $col = 'J3'; break;
			default: $col = 'B3'; break;
		}
		$objSheet->getCell($col)->setValue($mList['module_name']);
	} // end foreach
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getStyle('A3:J3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('EEEEEE');
	$objPHPExcel->getActiveSheet()->getStyle('A3:J3')->applyFromArray($styleHeadingDefault);
	//
	$row = 4;
	foreach($userList as $uList) {
		$objSheet->getCell('A'.$row)->setValue($uList['last_name'].", ".$uList['first_name']);
			foreach($moduleList as $idx => $mList) {
				switch ($idx)
				{
					case 0: $col = 'B'.$row; break;
					case 1: $col = 'C'.$row; break;
					case 2: $col = 'D'.$row; break;
					case 3: $col = 'E'.$row; break;
					case 4: $col = 'F'.$row; break;
					case 5: $col = 'G'.$row; break;
					case 6: $col = 'H'.$row; break;
					case 7: $col = 'I'.$row; break;
					case 8: $col = 'J'.$row; break;
					case 9: $col = 'K'.$row; break;
					default: $col = 'B'.$row; break;
					break;
				}
				if(strpos($uList['assignments'], $mList['module_id']) !==false) {
					$is_completed = $this->Learning_model->is_module_completed($uList['user_id'],$mList['module_id']);
					if($is_completed) {
						$objSheet->getCell($col)->setValue(date('m/d/Y',strtotime($is_completed['completed_date'])));
						$objPHPExcel->getActiveSheet()->getStyle($col)->getFont()->setBold(false)
		                                ->setName('Calibri')
		                                ->setSize(10)
		                                ->getColor()->setRGB('228B22');
					} else {
						$objSheet->getCell($col)->setValue('No');
						$objPHPExcel->getActiveSheet()->getStyle($col)->getFont()->setBold(false)
																		->setName('Calibri')
																		->setSize(10)
																		->getColor()->setRGB('FF0000');
					}
				} else {
					$objSheet->getCell($col)->setValue('N/A');
				}
		}
		$row++;
	} // endforeach

	// WRAP UP - WRITE OUTPUT
 	$filename = "user_progress_summary_report_".date('Ymd').".xlsx";
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	//
	header('Content-Disposition: attachment; filename="'.$filename.'"');
	$objWriter->save('php://output');

?>
