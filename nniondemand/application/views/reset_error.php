    <link href="<?php echo base_url('assets/theme/assets/pages/css/error.min.css');?>" rel="stylesheet" type="text/css" />
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top" style="background-color:#CCC;">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                  <a href="<?php echo base_url('Home/index');?>">
                    <img src="<?php echo base_url('assets/img/edu_exch_logo_small.png');?>" alt="logo" class="logo-default" /> </a>
                </div>
                <!-- END LOGO -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1><?php echo $this->session->user_data['errorCode'];?></h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12 page-401">
                            <div class="details">
                                <h4>Oops! Looks like we could not find an account that matches the information you've provided.</h4>
                                <p> Please check your information and try again.
                                <br/><br />
                                <a href="<?php echo base_url('Home/resetpswd');?>" class="btn btn-info btn-sm"> Try Again </a></p>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
