<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top" style="background-color:#CCC;">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="<?php echo base_url('Home/index');?>">
                    <img src="<?php echo base_url('assets/img/edu_exch_logo_small.png');?>" alt="logo" class="logo-default" /> </a>
            </div>
            <!-- END LOGO -->
            </div>
            <!-- END PAGE TOP -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                          <?php if($reset_complete == true) {
                              echo "<h3>Password Reset Successful!</h3>";
                            } else {
                              echo "<h3>Password Reset Request Acknowledged!</h3>";
                            } ?>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12 page-401">
                            <div class="details">
                              <?php if($reset_complete == true) {
                                echo "<h4>Your password reset request has been completed successsfully.</h4>";
                                echo "<p>Click the button below to proceed to the login page.</p>";
                              } else {
                                echo "<h4>An Email containing password reset instructions has been sent to the Email address provided.</h4>";
                                echo "<p>Click the button below to return to the login page.</p>";
                              } ?>
                              <a href="<?php echo base_url('Home/index');?>" class="btn btn-info btn-sm"> Continue </a>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
