<link href="<?php echo base_url('assets/theme/pages/css/login.min.css');?>" rel="stylesheet" type="text/css" />
<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo">
      <!--
        <a href="<?php echo base_url('Home/index');?>">
            <img src="<?php echo base_url('assets/img/signin_logo.png');?>" alt="" /> </a>
          -->
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action='<?php echo base_url('Home/reset_password_post');?>' method="post" onsubmit="return finalCheck();">
                <img src="<?php echo base_url('assets/img/signin_logo.png');?>" class="img-responsive" alt="" /> </a>
                <h4 class="form-title">Password Reset</h4>
                <?php if($userInitial) : ?>
                  <p>You are required to change your password after the initial sign in.  Passwords should be at least 8 character long and contain at least 1 number or capital letter.<br /><br />Please enter and validate your new passsword.</p>
                <?php else : ?>
                  <p><small>Please enter and validate your new passsword.</small></p>
                <?php endif; ?>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input type="hidden" name="user_id" value="<?php echo $userData['user_id'];?>">
                        <input type="hidden" name="email_address" value="<?php echo $userData['email_address'];?>">
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="New Password" name="password" id="password" /> </div>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Validate</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Validate Password" name="password2" id="password2" /> </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green pull-right"> Submit </button>
                </div>
            </form>
            <!-- END LOGIN FORM -->
        </div>
    <!--
    ** show login error if it was set
    -->
    <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
    <?php
    if($this->session->flashdata('error')) {
      echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
    } ?>

  <script>
    function finalCheck() {
  	var err = false;
  	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
  	if($('#password').val() == "") {err=true;errMsg=errMsg+"- Please enter a valid password.\n";}
    if($('#password2').val() == "") {err=true;errMsg=errMsg+"- Please validate your password.\n";}
    var pass1 = $('#password').val();
    var pass2 = $('#password2').val();
    if(pass1 != pass2) {err=true;errMsg=errMsg+"- Passwords do not match!.\n";}
    if($('#password').val().length < 8) {err=true;errMsg=errMsg+'- Password must be at least 8 characters long.\n';}
    if((!$('#password').val().match(/[A-Z]/)) || !$('#password').val().match(/([0-9])/)) { err=true;errMsg=errMsg+'- Passwords must contain at least one uppercase letter or a number.\n';}
    if(err == true) {alert(errMsg); return false;}
    return true;
  }
  </script>
