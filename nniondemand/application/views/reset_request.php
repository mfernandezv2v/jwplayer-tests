<link href="<?php echo base_url('assets/theme/pages/css/login.min.css');?>" rel="stylesheet" type="text/css" />
    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo"></div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php echo base_url('Home/reset_validate');?>" method="post">
                <img src="<?php echo base_url('assets/img/signin_logo.png');?>" class="img-responsive" alt="" /> </a>
                <h4 class="form-title">Password Reset</h4>
                <p><small>Please enter your login email address.<br />You will receive an email with instructions and a link to reset your password.</small></p>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email Address</label>
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email Address" name="email_address" /> </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn green pull-right" value=" Submit ">
                </div>
            </form>
            <!-- END LOGIN FORM -->
        </div>
        <!--
        ** show login error if it was set
        -->
        <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
        <?php
        if($this->session->flashdata('reset_error')) {
          echo "<script>javascript: growlError('".$this->session->flashdata('reset_error')."');</script>";
        }
        ?>
