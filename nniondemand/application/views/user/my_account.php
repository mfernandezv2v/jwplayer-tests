<?php $user_password = $this->User_model->encrypt_decrypt('decrypt', $userData['password']); ?>
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
      <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title"><h3> My Account </h3></div>
          <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
          <div class="col-md-6 col-xs-12">
          <div class="portlet box blue-soft">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-edit"></i>Manage Account Settings </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                </div>
            </div>
            <div class="portlet-body">
              <div class="tabbable-line">
                <ul class="nav nav-tabs ">
                  <li class="active">
                    <a href="#account_info" data-toggle="tab"> Account </a>
                  </li>
                  <li>
                    <a href="#contact_info" data-toggle="tab"> Contact Info </a>
                  </li>
                </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="account_info">
                  <p> You may update the following fields of your account profile.<br />All hightlighted fields are required.</p>
                    <form class="form" role="form" action='<?php echo base_url('Home/my_account_post');?>' method="POST" onsubmit='return finalCheck();'>
                        <div class="form-body">
                          <div class="form-group">
                            <label class="control-label">Email Address
                            </label>
                            <input type="hidden" name="user_id" id="user_id" value='<?php echo $user_id;?>'/>
                            <input type="email" class="form-control" name="email_address" id="email_address" value='<?php echo $userData['email_address'];?>' disabled='yes'/ data-toggle="tooltip" title="Email address cannot be changed">
                          </div>
                          <!-- -->
                          <div class="form-group">
                              <label class="control-label">Password
                                  <span class="required"> * </span>
                              <input type="password" class="form-control required" name="password" id="password" size="20" maxlength="20" value='<?php echo $user_password;?>'/>
                              <span class="help-block"><small>(min. 8 characters and 1 digit)</small></span>
                                </label>
                          </div>
                          <!-- -->
                          <div class="form-group">
                            <label class="control-label">Confirm Password
                              <span class="required"> * </span>
                            <input type="password" class="form-control required" name="rpassword" id="rpassword" size="20" maxlength="20" value='<?php echo $user_password;?>'/>
                            <span class="help-block"> Confirm your password </span>
                            </label>
                          </div>
                          <!-- -->
                          <div class="form-group">
                            <label class="control-label">Display Name
                            <input type="text" class="form-control" name="display_name" id="display_name" value="<?php echo $userData['display_name'];?>" size="25" maxlength="50"/>
                            <span class="help-block"> (Optional) </span>
                            </label>
                          </div>
                          <!-- -->
                          <div class="form-group">
                            <label class="control-label">Title
                            <select name="title" id="title" class="form-control" />
                            <option value=''>none</option>
                            <option value='Dr.'>Dr.</option>
                            <option value='Mr.'>Mr.</option>
                            <option value='Mrs.'>Mrs.</option>
                            <option value='Ms.'>Ms.</option>
                            <option value='Prof.'>Prof.</option>
                            </select>
                            <span class="help-block"> Select a title </span>
                            </label>
                          </div>
                          <!-- -->
                          <div class="form-group">
                            <label class="control-label">First Name
                                <span class="required"> * </span>
                            <input type="text" class="form-control required" name="first_name" id="first_name" value="<?php echo $userData['first_name'];?>" size="50" maxlength="50"/>
                            </label>
                          </div>
                          <!-- -->
                          <div class="form-group">
                            <label class="control-label">Middle Name
                            <input type="text" class="form-control" name="mid_name" id="mid_name" value="<?php echo $userData['mid_name'];?>" size ="50" maxlength="50"/>
                            </label>
                          </div>
                          <!-- -->
                          <div class="form-group">
                            <label class="control-label">Last Name
                                <span class="required"> * </span>
                            <input type="text" class="form-control required" name="last_name" id="last_name" value="<?php echo $userData['last_name'];?>" size="50" maxlength="50"/>
                            </label>
                          </div>
                          <!-- -->
                          <div class="form-group">
                              <label class="control-label">Degree(s)
                                <input type="text" class="form-control" name="degree" id="degree" value="<?php echo $userData['degree'];?>" size="20" maxlength="50"/>
                                <span class="help-block"></span>
                            </label>
                          </div>
                          <!-- -->
                          <div class="form-actions">
                            <input type="submit" class="btn btn-success button-submit" value=" Submit Changes">&nbsp;&nbsp;&nbsp;
                            <a href='<?php echo base_url('Home/main');?>' class='btn btn-md btn-default'> Cancel </a>
                          </div>
                        </div>
                      </form>
                    </div>
                    <!-- END TAB -->
                    <div class="tab-pane" id="contact_info">
                    <p> The following contact data fields are available for update. </p>
                    <form class="form" role="form" action='<?php echo base_url('Home/my_account_contact_post');?>' method="POST" onsubmit='return finalCheck();'>
                      <div class="form-body">
                        <div class="form-group">
                            <label class="control-label"> Address Type
                            </label>
                                <input type="hidden" name="user_id" id="user_id" value='<?php echo $user_id;?>'/>
                                <select name="address_type" class="form-control">
                                  <option value="">-- select --</option>
                                  <?php foreach($addressTypes as $aType) {
                                    echo "<option value='".$aType['address_type_id']."' ";
                                    if($userData['address_type'] == $aType['address_type_id']) echo " selected";
                                    echo ">".$aType['address_type_name']."</option>";
                                  }
                                  ?>
                                </select>
                        </div>
                        <!-- -->
                        <div class="form-group">
                            <label class="control-label"> Address </label>
                            <input type="text" class="form-control" name="address1" id="address1" size="25" maxlength="50" value='<?php echo $userData['address1'];?>'/>
                        </div>
                        <!-- -->
                        <div class="form-group">
                            <label class="control-label"> <small>(line 2)</small></label>
                            <input type="text" class="form-control" name="address2" id="address2" size="25" maxlength="50" value='<?php echo $userData['address2'];?>'/>
                        </div>
                        <!-- -->
                        <div class="form-group">
                            <label class="control-label"> City </label>
                            <input type="text" class="form-control" name="city" id="city" size="25" maxlength="50" value='<?php echo $userData['city'];?>'/>
                        </div>
                        <!-- -->
                        <div class="form-group">
                          <label class="control-label"> State </label>
                          <select name="state" id="state" class="form-control">
                            <option value="">-- select --</option>
                            <?php foreach($stateCodes as $sCode) {
                              echo "<option value='".$sCode['state_code']."' ";
                              if($userData['state'] == $sCode['state_code']) echo " selected";
                              echo ">".$sCode['state_code']." - ".$sCode['state_name']."</option>";
                            }
                            ?>
                          </select>
                        </div>
                        <!-- -->
                        <div class="form-group">
                          <label class="control-label"> Postal Code </label>
                          <input type="text" class="form-control" name="postal_code" id="postal_code" value="<?php echo $userData['postal_code'];?>" maxlength="20"/>
                        </div>
                        <!-- -->
                        <div class="form-group">
                          <label class="control-label"> Phone </label>
                          <input type="text" class="form-control" name="phone" id="phone" value="<?php echo $userData['phone'];?>" maxlength="20"/ onkeyup="formatAsPhone(this);">
                        </div>
                        <!-- -->
                        <div class="form-group">
                          <label class="control-label"> Phone Ext </label>
                          <input type="text" class="form-control" name="phone_ext" id="phone_ext" valu="<?php echo $userData['phone_ext'];?>" maxlength="20"/>
                        </div>
                        <!-- -->
                        <div class="form-group">
                          <label class="control-label"> Time Zone </label>
                          <select name="time_zone" id="time_zone" class="form-control">
                            <option value="">-- select --</option>
                            <?php foreach($timeZones as $tzCode) {
                              echo "<option value='".$tzCode['time_zone_code']."' ";
                              if($userData['time_zone'] == $tzCode['time_zone_code']) echo " selected";
                              echo ">".$tzCode['time_zone_code']." - ".$tzCode['time_zone_name']."</option>";
                            }
                            ?>
                          </select>
                        </div>
                        <!-- -->
                        <div class="form-actions">
                          <input type="submit" class="btn btn-success button-submit" value=" Submit Changes ">&nbsp;&nbsp;&nbsp;
                          <a href='<?php echo base_url('Admin/user_accounts');?>' class='btn btn-md btn-default'> Cancel </a>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- END TAB -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END CONTENT -->

  <!--
  ** show last operation status
  -->
  <script src="<?php echo base_url('assets/custom/scripts/myapp.js');?>" type="text/javascript"></script>
  <?php
  if($this->session->flashdata('success')) {
    echo "<script>javascript: growlSuccess('".$this->session->flashdata('success')."');</script>";
  } else if($this->session->flashdata('success')) {
    echo "<script>javascript: growlError('".$this->session->flashdata('error')."');</script>";
  } ?>
  <script>
    function finalCheckAccount() {
  	var err = false;
  	var errMsg = 'NOTE: Some required information has not been provided:\n\n';
  	if($('#email_address').val() == "") {err=true;errMsg=errMsg+"- Please enter a valid email address.\n";}
    if($('#password').val() == "") {err=true;errMsg=errMsg+"- Please enter a password.\n";}
    if($('#password').val().length < 8) {err=true;errMsg=errMsg+"- Passwords should be at least 8 characers in length.\n";}
    var pswd1 = $('#password').val();
    var pswd2 = $('#rpassword').val();
    if(pswd1 != pswd2) {err=true;errMsg=errMsg+"- Passwords do not match.\n";}
    if($('#user_type').val() == "") {err=true;errMsg=errMsg+"- Please select an account type.\n";}
    if($('#first_name').val() == "") {err=true;errMsg=errMsg+"- Please enter the user's first name.\n";}
    if($('#last_name').val() == "") {err=true;errMsg=errMsg+"- Please enter the user's last name.\n";}
    if($('#active').val() == "") {err=true;errMsg=errMsg+"- Please indicate the user's status.\n";}
    if(err == true) {alert(errMsg); return false;}
    return true;
  }
    function finalCheckContact() {
    var err = false;
    var errMsg = 'NOTE: Some required information has not been provided:\n\n';
    if($('#address_type').val() == "") {err=true;errMsg=errMsg+"- Please enter a valid email address.\n";}
    if(err == true) {alert(errMsg); return false;}
    return true;
  }
  //
  function formatAsPhone(fld) {
    var numbers = fld.value.replace(/\D/g, ''),
        char = { 0: '', 3: '-', 6: '-' };
            fld.value = '';
            for (var i = 0; i < numbers.length; i++) {
                fld.value += (char[i] || '') + numbers[i];
            }
    }
  </script>
