/* custom growl error message handler */

var growlError = function (msg) {
  setTimeout(function(){
    $.bootstrapGrowl(msg, {
      ele: 'body', // which element to append to
      type: 'danger', // (null, 'info', 'danger', 'success')
      offset: {from: 'top', amount: 30}, // 'top', or 'bottom'
      align: 'center', // ('left', 'right', or 'center')
      width: 250, // (integer, or 'auto')
      delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
      allow_dismiss: true, // If true then will display a cross to close the popup.
      stackup_spacing: 10 // spacing between consecutively stacked growls.
    });
  }, 800);
};

/* custom growl info message handler */
var growlInfo = function (msg) {
  setTimeout(function(){
    $.bootstrapGrowl(msg, {
      ele: 'body', // which element to append to
      type: 'info', // (null, 'info', 'danger', 'success')
      offset: {from: 'top', amount: 30}, // 'top', or 'bottom'
      align: 'center', // ('left', 'right', or 'center')
      width: 250, // (integer, or 'auto')
      delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
      allow_dismiss: true, // If true then will display a cross to close the popup.
      stackup_spacing: 10 // spacing between consecutively stacked growls.
    });
  }, 800);
};

/* custom growl info message handler */
var growlSuccess = function (msg) {
  setTimeout(function(){
    $.bootstrapGrowl(msg, {
      ele: 'body', // which element to append to
      type: 'success', // (null, 'info', 'danger', 'success')
      offset: {from: 'top', amount: 30}, // 'top', or 'bottom'
      align: 'center', // ('left', 'right', or 'center')
      width: 250, // (integer, or 'auto')
      delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
      allow_dismiss: true, // If true then will display a cross to close the popup.
      stackup_spacing: 10 // spacing between consecutively stacked growls.
    });
  }, 800);
};
